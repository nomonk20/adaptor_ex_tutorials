Wir haben 4 Gruppen vom Tester/innen
- Gruppe A: helfen uns Tutorial schreiben
- Gruppe B: Installationssetup testen
- Gruppe C: Alle die wissen, was sie mit Adaptor anfangen wollen
- Gruppe D: Alle die nicht wissen, was sie mit Adaptor anfangen wollen

1. **Fragerunde**
Zuerst werden alle (außer A&B) angeschrieben mit der Bitte, den folgenden Fragebogen zu beantworten:

Hast du eine Idee, was du mit Adaptor machen möchtest?

Nein: Macht nix, damit wissen wir schon alles, was wir brauchen.

Ja: Cool, dann beantworte bitte noch die folgenden Fragen:

- Wer bist du? Was ist deine Rolle / Aufgabe?
- Was ist dein Ziel? Was hast du vor?
- Was erhoffst du dir von Adaptor bei der Lösung deines Problems?
- Hast du schon Erfahrungen mit Adaptor?
- Oder Erfahrungen mit einem ähnlichen Tool?
- Wie würdest du das Problem ohne Adaptor lösen?
- Wie würde die perfekte Lösung / der perfekte Weg zum Ziel aussehen? (muss nicht mit Adaptor sein, kann auch mit Magie sein)


2. **Tutorial Runde**
Danach bekommt Gruppe A die konkreten Wünsche der Tester/innen aus Gruppe C, um selber zu probieren und ggf. Tutorials daraus zu schreiben.

3. **Basic Test Runde**
Zwischenzeitlich können Probanden aus Gruppe D ein Einstiegstutorial unter Beobachtung durchführen. 

4. **Advanced Test Runde**
Dann bekommen die Probanden aus Gruppe C die Tutorials von Gruppe A, um sie (nicht unter Aufsicht?) auszuführen.

5. **Auswertung**
Die Ergebnisse aus den Testreihen fließen anschließend in die Tutorials bzw. in die Überarbeitung des Interface.



