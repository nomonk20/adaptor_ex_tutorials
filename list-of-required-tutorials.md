# List of required tutorials

- Setup (own Server)
    - Basics: local machine mode (e.g. laptop in local network running adaptor:ex) vs. remote mode (adaptor running on a server)
- Create Telegram Client
- Connect physical devices via network (local machine mode)
- Connect 3rd party software via network (local machine mode and remote mode)
- Control Stage Lights with DMX USB Pro Device (local machine mode)
- Send Group Message

## Existing tutorials that require other tutorials 

### Telegram Basics

- Setup (own Server)
- Create Telegram Client (remote mode)
