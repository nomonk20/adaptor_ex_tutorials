Getting Started
=========================

![adaptor:ex](./assets/header.png)

adaptor:ex helps you to create interactive performances, Theatre-Games, installations, chatbot adventures or escape rooms. The graphical programming interface is optimized to bring dramaturgy and game mechanics together and to arrange them clearly.

You can make quick adjustments during rehearsals and monitor and control the live flow during testing and performance.

The adaptor:ex editor is accessed through the browser, so you can edit and control the same game with multiple people.

![Screenshot: Der adaptor:ex editor](./assets/editor_example.png)

Here you can find more about how to use adaptor:ex to wire and control your storyline, game mechanics, software and hardware.

Get and install adaptor:ex on your local machine or a server. There are different ways to [Install](#Install).

Start with a [Tutorial](docs/tutorials/arduino-serial/index.md) if you want to learn about one of the different ways you can use adaptor:ex.

The basic building Blocks, Actions and Plugins are described in more detail in [Glossary](editor.md).

Install
------------

To install adaptor:ex you currently have to use the command line of your operating system. On macOS, look for the "Terminal" program. On Linux you can find the terminal under the key combination [Ctrl]+[Alt]+[T]. On Windows search for the application "Input Prompt" or "Command line".

There are three installation options to choose from: [NPM](#install-with-npm), [SOURCE](#install-from-source-with-npm) and [DOCKER](#install-with-docker).

If you want to use the ports on your computer, e.g. the serial port via USB or a sound card, install adaptor:ex with [NPM](#install-with-npm) or via [SOURCE](#install-from-source-with -npm).

Especially for a server installation we recommend installing in a [Docker container](#install-with-docker).

adaptor:ex has just started. We'll be fixing bugs, improving things (hopefully) and adding new features frequently over the coming months. Occasionally update via [NPM](#updates-mit-npm) or [Docker](updates-mit-docker) to stay current.

Contact us if you're having trouble installing adaptor:ex. Write to us at [tech@machinaex.de](mailto:tech@machinaex.de) or check out our [Discord](https://discord.gg/quHbQAMvF6).


### Install-with-NPM

Before you can continue to install adaptor:ex, you need the latest version of NodeJS. You can find the necessary installation files for your operating system [here](https://nodejs.org).

Once you have installed NodeJS, open your console or terminal and run the installation with npm in the command line

On macOS and Linux
```console
sudo npm install -g adaptorex --unsafe-perm
```

On Windows
```console
npm install -g adaptorex --unsafe-perm
```

If the installation was successful, start adaptor:ex with the command
```console
adaptorex
```

You can find options for configuring adaptor:ex in the [adaptor:ex server readme](https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server#install)

To exit the adaptor:ex server use the key combination [Ctrl] + [C] (windows/linux) or [Cmd] + [C] (MacOS), or `quit` enter in the console.

### Update with NPM

Download the latest version of adaptor:ex by repeating the installation above.

With `npm outdated -g` you can check whether a new version is available.

### Installing Source with NPM

Before you can continue installing adaptor:ex, you need the latest version of NodeJS. You can find the necessary installation files for your operating system [here](https://nodejs.org/) .

Get adaptor:ex server and client:

**Server**
Clone or download the latest adaptor:ex server GitLab repository here: [https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server](https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server)

**Client**
Clone or download the latest adaptor:ex client GitLab repository here: [https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_client](https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_client)


Now you can install the server and client locally using [NPM](https://www.npmjs.com/).

In the terminal (the console) change to the directory in which you loaded or cloned the server or client.
```console
cd adaptor_ex_server/
```

or
```console
cd adaptor_ex_client/
```

When you are in the correct directory run the following command
```console
npm install
```

If the installation was successful, start the **adaptor:ex server** in the appropriate directory with the command
```console
node index.js
```

Then, in a separate command line, start the **adaptor:ex client** in the appropriate directory with the command
```console
npm run serve
```

### Update with GIT

Download the latest version of adaptor:ex server and client by updating the Git repository in the respective directory with the following command updates
```console
git pull
```

Then restart the adapter server and client.

### Install Telegram extantion

In order to be able to use the Telegram plugin in adaptor:ex you have to additionally install [python](https://python.org/) and some python libraries. Follow the instructions of the adaptor:ex [Telegram Plugin Readme](https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server/-/blob/main/plugins/telegram/README.md) .

### Install with Docker

Download Docker for your operating system and follow the installation instructions at [https://docs.docker.com/get-docker/](https://docs.docker.com/get-docker/)

Create a new folder where you want to install adaptor:ex.

Open your command line, change to `cd` the adaptor:ex directory with the command and download the [adaptor:ex docker-compose](https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server/-/blob/main/docker-compose.yml) file:

```console
curl -o docker-compose.yml https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server/-/raw/main/docker-compose.yml
```

Run the Docker Compose file:

```console
docker-compose up -d
```

adaptor:ex server, client, database and plugin extensions will be downloaded and installed.

You can read more about configuring adaptor:ex in the [adaptor:ex server readme .](https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server#install)

### Update with Docker

To update the adaptor:ex Docker installation change to the adaptor:ex directory. Execute the following commands there:

```console
docker-compose pull
```

and subsequently

```console
docker-compose up -d
```

First Steps
---------------

Enter the URL, i.e. the web address, of your adaptor:ex setup in the web browser.

> We recommend using the latest version of Google Chrome or Firefox.

If you have installed adaptor:ex on your computer or laptop with the default settings, you can also click on one of these links: [http://localhost:8081](http://localhost:8081/) (installation via NPM) or [http://localhost:8080](http://localhost:8080/) (source installation).

If you have installed adaptor:ex on a server or another computer, you must replace the URL with the IP or domain name of your server.

### Create a Game

![Die adaptor:ex Startseite](./assets/no_games.png)

Start by creating a new game project. Click `New Game` and give your first game a name.

![Ein neues Game erstellen](./assets/new_game.png)

### Add levels

If you click on the newly created game project, you will end up in the overview that shows all existing levels. Each game can consist of several levels that are linked in a variety of ways.

Create your first level with `Add new`.

> Games and levels get their own web address through which they can be reached from the web browser. Therefore the name must not contain spaces or special characters.

Name the level and click `Add new Level`.

![Ein Level hinzufügen](./assets/new_level.png)

### Edit level

Then open the level by clicking on the name. You are now in the level editor.

The start ( _START_ ) and end point ( _QUIT_ ) of your level already exist.

![Der Level Editor](./assets/empty_level.png)

From the left sidebar, [Actions](editor.md#action), locate the control action [Log Message](log.md)  and drag and drop it to the middle of the page.

A new [State](editor.md#state) Element is created and contains a [Log Message](log.md) action ( _log_1_ ).

![Eine Action hinzufügen](./assets/log_action.png)

Click on the new action to edit it. Write a short text in the `message` input field and select _info_ `level` from the dropdown .

![Eine Action bearbeiten](./assets/edit_log_action.png)

In order to make your level more understandable, it is good to give all states understandable names.

> State names must also not contain spaces, as they can be used in [Variables](variables.md)

Click in the title bar of your Log Message State and rename it.

![States umbenennen](./assets/rename_state.png)
Also edit the [Next](next.md) action _next_1_ in the _START_ state by clicking on it. Then select the new state, here _HelloWorld_ , from the dropdown to `next state` connect it to the _START_ state.

![2 States verbinden](./assets/connect_log_action.png)

### Start Level

Switch from editing to [Live Mode](live.md) by clicking the play button in the top right corner. 

![Live Modus Toggle](./assets/live_toggle.png)

The left sidebar with the actions closes and a menu opens on the right sidebar.

Click on the right sidebar `Create Session` and then click to `Start Session` create a [Session](live.md#session) from your level.

![Eine Session starten](./assets/start_session.png)

The newly started session is selected in the right sidebar.

![Eine session auswählen](./assets/select_session.png)

The _HelloWorld_ State is already highlighted.

When we create a new session from a level, the _START_ state is triggered automatically. The _next_1_ action immediately continued to our state _HelloWorld_ with the log message.

Open the log console by clicking on the cursor icon.

![Log Konsole anzeigen](./assets/console_toggle.png)

The latest log message should say 'Hello World!'.

By clicking on the _HelloWorld_ status you can manually reactivate it. Observe the log console to view the message.

### automate levels

Switch back to edit mode by clicking the Live Play button again.

Find the [TimeOut](timeout.md) action in the right sidebar and drag it into the _START_ State.

If you drag the action over it, the state will be highlighted, indicating that the action will be added to that state instead of creating a new state.

> If we add an action to a state that is waiting for something, like a timer to expire, it automatically replaces any [Next](next.md) action.

Edit the timeout. Enter a small number of seconds in the `timeout` field and choose the _HelloWorld_ State as `next state`.

![Einen timeout erstellen](./assets/timeout.png)

Also add a timeout action to the _HelloWorld state._ Edit the timeout and this time select _Quit_ as `next state`.

If you now switch to live mode and start a new session, the states will be triggered one after the other at fixed intervals.

![Mehrere Sessions gleichzeitig](./assets/automated_session.png)

As soon as the _QUIT_ state is reached, the session is terminated and disappears from the right sidebar.

Your first session will not end automatically. To end it you can close it (x) in the right sidebar or trigger the _QUIT_ state when you have selected the session.

> Each session is always based on the latest version of the level. However, if the state a session is in changes, it must be triggered again.

Next Steps
----------------

In the **reference** you can find out more about the different basic building blocks of adaptor:ex.

**Tutorials** guide you step by step through projects that you can implement with adaptor:ex.

For example, learn how to use the Telegram plugin to develop a messenger adventure: [Storytelling with Telegram and adapter eX](docs/tutorials/telegram-basics/index.md)

Or start driving an Arduino with adaptor:ex: [Arduino Serial](docs/tutorials/arduino-serial/index.md)

## Authors and Supporters

This is a [machina commons](https://www.machinacommons.world/) project by [machina eX](https://machinaex.com/) .

sponsored by

<img src="https://machinacommons.world/img/Logo_Senat_Berlin.png" alt="Logo Senatsverwaltung für Kultur und Europa Berlin" width="50%"/>

Senate Department for Culture and Europe Berlin