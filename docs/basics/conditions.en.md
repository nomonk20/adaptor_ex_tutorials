# Query conditions with conditions
===================================

An essential part of every game is the query of conditions.

In adaptor:ex, for example, the logic actions [Switch](../basics/actions/logic/switch.md), [onEvent](../basics/actions/logic/onEvent.md) and [onChange](../basics/actions/logic/onChange.md) allow conditions to be queried. In the [Telegram](./plugins/telegram.md) and in the [Twilio](./plugins/twilio.md) plugin we can also use conditions to shape the course of the game.

## If ... then ... (if)
---------

In the `if` option we formulate one or more queries which, if they apply, trigger different states.

![Switch Condition](./assets/conditions_switch_example.png)

Here we use the [Switch](../basics/actions/logic/switch.md) action to query the status variable of the current player and decide which state to proceed with. We check whether the value behind the [Variables](./variables.md) `Player.status` equals the text 'beginner' ( _equals_ ). If this is the case, it continues directly in the 'Welcome' state. Since we haven't specified anything else, it would be the end at this point if `Player.status` not 'beginner' but had a different value.

However `Add condition`, with we can add other conditions that result in a different or the same `next state` result.

> Conditions are always queried from top to bottom. If several conditions apply at once, the system prioritizes the first applicable condition read from above.


## Otherwise ... (else)
------------

Some conditions also allow us to specify what should happen if none of our conditions match.

Add `Settings` the option using the button below `else`.

![Else Option](./assets/conditions_switch_example_add_else.png)
If none of the above conditions apply, the state `else` specified in is now triggered.

>In the case of `switch` action, you can use `else` instead of [Next](../basics/actions/control/next.md) action to specify which state should come next if none of the conditions are met.

## Action variable 'match'
----------------------

All conditions store the value that met the condition in the action variable `match`. You can access it in the following state. Conditions in messengers sometimes even allow you to already use the match variable in the same action.

You can `[[state.<state_name>.<condition_action_name>.match]]` access it with .

[Switch](../basics/actions/logic/switch.md) action example above, you would address the value like this: `[[state.SelectStatus.switch_1.match]]`.

> In the explanation of the operators you may find more detailed information about which value is saved.

## Query Types (Operators)
------------------------

In addition to the query `equals` that checks whether a variable has a specific value, there are other ways of formulating queries.

You can use a different form of query for each `condition`.

![Abfrage Auswahl](./assets/conditions_switch_example_operators.png)

Please note: not all query forms are available in all actions that use conditions.

### Equals

The condition is true if the values in `value` and in `equals` have the same value. Similar to the `==` operator.

It also checks spaces, paragraphs, and punctuation marks. That means: `Hello, how are you?` is not equal to `Hello,how are you?`.

By default, uppercase and lowercase letters are not taken into account. That means: `Hello, how are you?` is equal to `hello, how are YOU?`. Set the `case sensitive` option to _active_ if you want to be case sensitive.

`equals` It is also possible to specify multiple values. The condition is true if `value`equals any of the values.

If the condition is true, the `match` action variable contains the `equals` value contained in `value`.

Variables that cannot be resolved always result in **undefined** . You can set `equals` **undefined** to query this case. The condition is true if the variable in `value` refers to a non-existent value or item.

### Contains

The condition is true if the `contains` value is contained in `value`.

`contains` applies only to text not to numeric values.

`fine` is contained in `I'm fine.` and the condition is true.

Also  `I'm f`  is included in `I'm fine.` and satisfies the condition.

As with the `equal` condition, uppercase and lowercase letters are not taken into account by default. Select `case sensitive` option to turn it on.

It is possible to specify multiple `contains` values. The condition is true if one or more of the values are contained in `value`.

If the condition is met, the action variable `match` contains the `contains` value contained in `value`.

### Less Than

The condition is true if `value` and `less_than` are numeric values and `value` is `less_than`. Similar to the `<` operator.

`1` is smaller than `2`

`2` is NOT less than `2`

`-5` is smaller than `-2`

Note that floating point numbers must use a period (`.`) Instead of a comma (`,`).

`1.54` is smaller than `2.1`

It is possible to specify multiple `less_than` values. The condition is true if one or more of the values are less than `value`.

If the condition is met, the `match` action variable contains the `less_than` value contained in `value`.

### Greater Than

The condition is true if `value` and `greater_than` are numeric values and `value` is `greater_than`. Similar to the `>` operator.

 `2` is bigger than `1`

`2` is not  bigger than `2`

`-2` is bigger than `-5`

Note that floating point numbers must use a period (`.`) Instead of a comma (`,`).

`2.1` is bigger than `1.54`

It is possible to specify multiple `greater_than` values. The condition is true if one or more of the values are less than `value`.

If the condition is met, the `match` action variable contains the `greater_than` value contained in `value`.

### Regular Expression

The condition is true if the regular expression has at least one match.

With regular expressions it is possible to check texts in many different ways. Regular expressions are a popular tool - accordingly, there are many resources on the web that can help you formulate a regex for your query. The page [https://regexr.com/](https://regexr.com/) , for example, offers detailed instructions and test and help tools for creating regular expressions.

Regex in adaptor:ex conditions are always case insensitive (i) and not global (the regex is only executed up to the first match).

If there is a match, the action variable `match` contains the `match` value of the regular expression.

#### example

![Regular Expression Beispiel](./assets/conditions_switch_example_regex.png)

This query determines whether the **secret_code** variable conforms to a specific format. The regex formula `\d{3}-[a-zA-Z]{4}-\d{3}` returns a match if the text to be checked begins with 3 digits a hyphen 4 letters (az or AZ) another hyphen and 3 numbers.

A **secret_code** '392-hgZb-520' would lead to the next State **ValidCode** . The formatted part should also be surrounded by other text. ???

### Javascript Functions

The condition is true when the javascript function returns a [truthy](https://developer.mozilla.org/en-US/docs/Glossary/Truthy) value `return`.

If the `return` value of the function is [Falsy](https://developer.mozilla.org/de/docs/Glossary/Falsy) , such as `false`, `undefined` or `0`, the condition is not true.

The only transfer parameter of the function is `value`.

The `function` form field only defines the function body and not the function declaration.

`match` If the condition is true, the action variable contains the value `return` of the Javascript function.

#### example

Create a [[switch]] action and select `Javascript function` as query type.

Check whether the property `inventory` in the level argument `Player` contains 5 or more entries and switch to `next state` _TooHeavy_ state if necessary.

The function also returns `return` all entries `inventory` after the 5th entry as a value.

Note that the query assumes that `inventory` is an _array_ variable.


![Eine Abfrage mit Javascript erstellen](./assets/conditions_function_example.png)

In the following states, we can use the _array_ that was `return` returned by the action variable `match`.

![Den Funktion return Wert in der match action variable aufrufen](./assets/conditions_function_example_log.png)

If the `if` query does not apply, `return true` is ignored. A javascript function that does not specify `return` returns `undefined`. The condition is therefore not satisfied.

The above function is identical to:

``` js
if ( value.length > 5)  {
    return value.slice(5)
} else {
    return undefined
}
```

### Database Query

The condition is true when the **database query** finds 1 or more matching entries.

Use `Collection`  to specify which system items or collection ("level" and "session") the **query** refers to.

In **query**, formulate a database search in the style of a [MongoDB find query](https://docs.mongodb.com/manual/reference/method/db.collection.find/) .

**Database Query** has no `value`option. Instead, use variables within `query`to access variable values.

If the condition is true, the `match` action variable contains the number of entries found.

#### example

For example, to check whether a player item with a certain `name` variable exists, set `Collection` to "player" and formulate the following **query** :`{name:'Ada Lovelace'}`

![Database Query Beispiel](./assets/query_example.png)

The `next state` "LetsGo" is triggered when a player item with `name` variable "Ada Lovelace" exists or when the [onChange](./actions/logic/onChange.md) listener detects that a player item with `name` variable "Ada Lovelace" has been created.

To search for an entry with a variable `name` property, **query** could look like this:

`{name:'[[Player.best_friend]]'}`

Note that you also have to put quotation marks here, i.e. in the case of a _string_ variable.

> The curly brackets in **query** are optional.
