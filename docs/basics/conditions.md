Bedingungen abfragen mit conditions
===================================

Wesentlicher Bestandteil jedes Games ist die Abfrage von Bedingungen.

In adaptor:ex erlauben es zum Beispiel die logic actions [Switch](../basics/actions/logic/switch.md), [On Event](../basics/actions/logic/onEvent.md) und [On Change](../basics/actions/logic/onChange.md), Bedingungen abzufragen. Auch im [telegram](./plugins/telegram.md) und im [twilio](./plugins/twilio.md) plugin können wir mit conditions den Verlauf des Spiels gestalten.

Wenn ... dann ... (if)
---------

In der `if` option formulieren wir eine oder mehrere Abfragen, die, wenn sie zutreffen, unterschiedliche states auslösen.

![Switch Condition](./assets/conditions_switch_example.png)

Hier nutzen wir die [Switch](../basics/actions/logic/switch.md) action, um die status variable des aktuellen Player abzufragen und zu entscheiden, mit welchem state es weitergehen soll. Wir überprüfen, ob der Wert hinter der [Variable](./variables.md) `Player.status` dem text 'beginner' gleicht (*equals*). Wenn dies der Fall ist, geht es direkt weiter im state 'Welcome'. Da wir weiter nichts festgelegt haben, wäre an dieser Stelle Schluss, wenn `Player.status` nicht 'beginner', sondern einen anderen Wert hat.

Mit `Add condition` können wir aber weitere Bedingungen hinzufügen, die einen anderen oder denselben `next state` zur Folge haben.

> Bedingungen werden immer von oben nach unten abgefragt. Sollten mehrere Bedingungen auf einmal zutreffen, priorisiert das System also die von oben gelesen erste zutreffende Bedingung.

Sonst ... (else)
------------

Manche conditions erlauben es außerdem, dass wir angeben, was passieren soll, wenn keine unserer Bedingungen passt.

Füge über den unteren `Settings` button die `else` Option hinzu.

![Else Option](./assets/conditions_switch_example_add_else.png)

Trifft keine der oberen Bedingungen zu, wird nun der state ausgelöst, der in `else` angegeben ist.

> Im Falle der `switch` action kannst du statt `else` auch die control action [next](../basics/actions/control/next.md) nutzen, um anzugeben, welcher state als nächstes kommen soll, wenn keine der Bedingungen zutrifft.

Action variable 'match'
----------------------

Alle conditions speichern den Wert, der die Bedingung erfüllt hat, in der action variable `match`. Du kannst im folgenden state darauf zugreifen. Conditions in Messengern erlauben es dir mitunter sogar, die match variable in derselben action bereits zu verwenden.

Du kannst mit `[[state.<state_name>.<condition_action_name>.match]]` darauf zugreifen. 

Im obigen Beispiel mit der [Switch](../basics/actions/logic/switch.md) action würdest du den Wert also so addressieren: `[[state.SelectStatus.switch_1.match]]`.

In den Erläuterung zu den Operatoren findest du ggf. genauere Angaben, welcher Wert gespeichert wird.

Abfragetypen (Operatoren)
------------------------

Neben der Abfrage `equals`, die überprüft, ob eine Variable einen bestimmten Wert hat, gibt es noch weitere Möglichkeiten, Abfragen zu formulieren.

Du kannst für jede `condition` eine andere Form der Abfrage verwenden.

![Abfrage Auswahl](./assets/conditions_switch_example_operators.png)

Bitte beachte: Nicht in allen actions, die conditions verwenden, stehen alle Abfrageformen zur verfügung.

### Equals

Die Bedingung ist erfüllt, wenn die Werte in `value` und `equals` den gleichen Wert haben. Ähnlich dem `==` Operator.

Dabei werden auch Leerzeichen, Absätze und Satzzeichen überprüft. Das bedeutet: `Hallo, wie geht's?` ist nicht gleich `Hallo,wie gehts?`.

Groß- und Kleinschreibung wird per default nicht berücksichtigt. Das bedeutet: `Hallo, wie gehts?` ist gleich `hallo, wie Gehts?`. Setze die `case sensitive` Option auf *aktiv*, wenn Groß- und Kleinschreibung berücksichtigt werden soll.

Es ist außerdem möglich, mehrere `equals` Werte anzugeben. Die Bedingung trifft dann zu, wenn `value` irgendeinem der Werte gleicht.

Bei erfüllter Bedingung enthält die `match` action variable den `equals` Wert, der `value` gleicht.

Variablen, die nicht aufgelöst werden können, ergeben stets **undefined**. Du kannst `equals` **undefined** setzen, um diesen Fall abzufragen. Die Bedingung ist dann erfüllt, wenn die variable in `value` auf einen nicht existierenden Wert oder ein nicht existierendes item verweist.

### Contains

Die Bedingung ist erfüllt, wenn der `contains` Wert in `value` enthalten ist.

`contains` bezieht sich nur auf Texte, nicht auf numerische Werte.

`gut` ist in `Mir geht's gut.` enthalten und die Bedingung ist erfüllt.

Auch `ht's g` ist in `Mir geht's gut.` enthalten und erfüllt die Bedingung.

Groß- und Kleinschreibung wird wie bei `equals` per default nicht berücksichtigt. Setze die `case sensitive` Option auf *aktiv*, wenn Groß- und Kleinschreibung berücksichtigt werden soll.

Es ist möglich, mehrere `contains` Werte anzugeben. Die Bedingung trifft dann zu, wenn einer oder mehrere der Werte in `value` enthalten sind.

Bei erfüllter Bedingung enthält die `match` action variable den `contains` Wert, der in `value` enthalten ist.

### Less Than

Die Bedingung ist erfüllt, wenn `value` und `less_than` numerische Werte sind und der Wert in `value` kleiner als der in `less_than` ist. Ähnlich dem `<` Operator.

 `1` ist kleiner als `2`

`2` ist NICHT kleiner als `2`

`-5` ist kleiner als `-2`

Beachte, dass bei Fließkommazahlen statt des Kommas (`,`) ein Punkt verwendet werden muss (`.`).

`1.54` ist kleiner als `2.1`

Es ist möglich, mehrere `less_than` Werte anzugeben. Die Bedingung trifft dann zu, wenn einer oder mehrere der Werte kleiner sind als `value`.

Bei erfüllter Bedingung enthält die `match` action variable den `less_than` Wert, der kleiner ist als `value`.

### Greater Than

Die Bedingung ist erfüllt, wenn `value` und `greater_than` numerische Werte sind und der Wert in `value` größer als der in `greater_than` ist.

Ähnlich dem `>` Operator.

 `2` ist größer als `1`

`2` ist NICHT größer als `2`

`-2` ist größer als `-5`

Beachte, dass bei Fließkommazahlen statt des Kommas (`,`) ein Punkt verwendet werden muss (`.`).

`2.1` ist größer als `1.54`

Es ist möglich, mehrere `greater_than` Werte anzugeben. Die Bedingung trifft dann zu, wenn einer oder mehrere der Werte größer sind als `value`.

Bei erfüllter Bedingung enthält die `match` action variable den `greater_than` Wert, der größer ist als `value`.

### Regular Expression

Die Bedingung ist erfüllt, wenn der reguläre Ausdruck (regular expression) mindestens eine Übereinstimmung hat.

Mit Regular Expressions ist es möglich, Texte auf verschiedenste Arten zu überprüfen. Regular Expressions sind ein beliebtes Tool - entsprechend gibt es viele Ressourcen im Netz, die dir helfen können, eine Regex für deine Abfrage zu formulieren. Die Seite [https://regexr.com/](https://regexr.com/) etwa bietet eine ausführliche Anleitung und Test- und Hilfetools zur Erstellung von Regular Expressions.

Regex in adaptor:ex-conditions sind immer case insensitiv (i) und nicht global (die Regex wird nur bis zum ersten Match ausgeführt).

Gibt es eine Übereinstimmung, enthält die `match` action variable den `match` Wert der regular expression.

#### Beispiel

![Regular Expression Beispiel](./assets/conditions_switch_example_regex.png)

Diese Abfrage stellt fest, ob die variable **secret_code** ein bestimmtes Format erfüllt. Die Regex-Formel **\d{3}-[a-zA-Z]{4}-\d{3}** liefert dann eine Übereinstimmung, wenn der zu überprüfende Text mit 3 Zahlen (digits) beginnt, dann, nach einem Bindestrich, 4 Buchstaben aus dem Alphabet (a-z oder A-Z) folgen und sich, nach einem weiteren Bindestrich, wieder 3 Zahlen anschließen.

Ein **secret_code** '392-hgZb-520' etwa würde zum nächsten State **ValidCode** führen. Der formatierte Teil dürfte dabei auch von anderem Text umschlossen sein.

### Javascript Function

Die Bedingung ist erfüllt, wenn die Javascript Funktion einen [Truthy](https://developer.mozilla.org/en-US/docs/Glossary/Truthy) Wert mit `return` zurückgibt.

Ist der `return` Wert der Funktion [Falsy](https://developer.mozilla.org/de/docs/Glossary/Falsy), also etwa `false`, `undefined` oder `0`, ist die Bedingung nicht erfüllt.

Einziger Übergabe Parameter der Funktion ist `value`.

Das `function` Formularfeld definiert nur den Function Body und nicht die Funktionsdeklaration.

Die `match` action variable enthält, wenn die Bedingung erfüllt ist, den `return` Wert der Javascript Funktion.

#### Beispiel

Erstelle eine [Switch](./actions/logic/switch.md) action und wähle `Javascript function` als Abfragetyp aus.

Überprüfe ob die Eigenschaft `inventory` im Level Argument `Player` 5 oder mehr Einträge enthält und wechsle ggf. zum `next state` *TooHeavy*.

Die Funktion gibt zudem als `return` Wert alle Einträge in `inventory` hinter dem 5. Eintrag zurück.

Beachte, dass die Abfrage davon ausgeht, das `inventory` eine *array* Variable ist.

![Eine Abfrage mit Javascript erstellen](./assets/conditions_function_example.png)

In den folgenden States können wir das *array* das mit `return` zurückgegeben wurde über die `match` action Variable nutzen.

![Den Funktion return Wert in der match action variable aufrufen](./assets/conditions_function_example_log.png)

Wenn die `if` Abfrage nicht zutrifft, wird `return true` übersprungen. Eine Javascript Funktion, die kein `return` Spezifiziert gibt `undefined` zurück. Die Bedingung ist dann also nicht erfüllt.

Die obige Funktion ist identisch mit:

``` js
if ( value.length > 5)  {
    return value.slice(5)
} else {
    return undefined
}
```
### Database Query

Die Bedingung ist erfüllt, wenn die **Database Query** 1 oder mehrere passende Einträge findet.

Gib mit `Collection` an auf welche Item oder System Collection ("level" und "session") sich die **query** bezieht.

Formuliere in **query** eine Datenbank Suche im Stile einer [MongoDB find query](https://docs.mongodb.com/manual/reference/method/db.collection.find/).

**Database Query** hat keine `value` Option. Verwende stattdessen Variablen innerhalb der `query` um auf variable Werte zuzugreifen.

Wenn die Bedingung erfüllt ist,  enthält die `match` action variable die Anzahl der gefundenen Einträge.

#### Beispiel

Um etwa zu überprüfen ob ein player Item mit einer bestimmten `name` Variable existiert, setze `Collection` auf "player" und formuliere folgende **query**: `{name:'Ada Lovelace'}`

![Database Query Beispiel](./assets/query_example.png)

Der `next state` "LetsGo" wird ausgelöst, wenn ein player Item mit `name` Variable "Ada Lovelace" existiert oder wenn der [On Change](./actions/logic/onChange.md) listener feststellt, dass ein player Item mit `name` Variable "Ada Lovelace" erstellt wurde.

Um nach einem Eintrag mit variabler `name` Eigenschaft zu suchen könnte **query** folgendermaßen aussehen:

`{name:'[[Player.best_friend]]'}`

Beachte, dass du auch hier, also im falle einer *string* variable, Anführungszeichen setzen musst.

> Die äußeren geschweiften Klammern in **query** sind optional.
