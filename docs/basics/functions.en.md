Javascript Functions
=====================

Javascript functions extend the possibilities of an adaptor:ex game by the complete range of the script programming language Javascript. Some functions are already included in the adaptor:ex server installation. It is also possible to create and integrate your own functions.

## add features

Like [files and media files](files.md), functions are stored in the adaptor:ex data folder  `games` in the respective game folder.

Create one or more javascript  files ( `.js` ) in your game's folder `functions`  containing your functions.

```
data
├── games
|   ├── Playground
│   |   ├── files
│   |   ├── functions
│   |   |   ├── myfunctions.js
├── log
├── nedb
├── config.json
```

The files are loaded into your game as [nodejs modules .](https://www.w3schools.com/nodejs/nodejs_modules.asp) So you need to make your functions available to adaptor:ex using `module.exports`

``` js
async function playerCount(args, {session, game}) {
    all_player = await game.db.player.find({})

    return all_player.length
}

function reverse(args, {session, game}) {
    let reversed = args[0].split("").reverse().join("")
    return reversed
}

module.exports = {
  playerCount:playerCount,
  reverse:reverse
}
```

If you copy these lines into a new file with the `.js` extension and move them to the functions folder in your game, you can access two new functions "reverse" and "playerCount" in your game.

You can add as many features as you want in one file or split the features across multiple files. The file names are arbitrary and play no role.

Perform Functions
--------------------

You can use functions both in the independent [Function](./actions/logic/function.md) action and in all form fields that allow variables as input.

### Functions-as-Variables

Specify the function name enclosed in two square brackets ( `[[` and `]]` ) in an action setting that accepts variable input.

The function name is supplemented by an open and a closed bracket `()`. Arguments can also be specified here, separated by a comma ( `,` ), to pass them to the function.

Arguments cannot have spaces between them, and they cannot contain spaces. In addition, the individual arguments are always passed as a _string within an_ _array_ .

Example of a function as a variable:

`[[test(hello,world)]]`

Within the function, "hello" and "world" are passed as an _array_ :

``` js
function test(args, {session, game}) {
  session.log("1. " + args[0]) // 1. hallo
  session.log("2. " args[1]) // 2. welt
}
```

The return value of the function is inserted at the point at which the function is inserted.

You can also find all the functions of your game in the VARIABLES TOOLBAR and from there you can drag them into the respective action.

#### example

Use the "playerCount" function created above in a [Switch](./actions/logic/switch.md) action.

![Funktion als Variable in der switch action](./assets/function_as_variable_example.png)

If the number of items in the player collection is greater than 10, the `next state`"Party" continues. Otherwise it continues with "NetflixAndChill".

### The function action

You can find out more about the action here: [Function](./actions/logic/function.md)

## Write functions

> This chapter is far from complete. Contact us if you have any questions at [tech@machinaex.de](mailto:tech@machinaex.de) or on the [machina commons discord server](https://discord.gg/quHbQAMvF6)

You can use all features of the [Nodejs](https://nodejs.org/en/docs/guides/) Javascript Runtime.

To include external libraries that are not included in adaptor:ex, we recommend installing them globally with npm.

All the parameters passed by the level session are included in the first argument.

The second argument always contains variables and functions from the adaptor:ex game and session context.

``` js
async function playerCount(args, context) {
    all_player = await context.game.db.player.find({})

    return all_player.length
}

function reverse(args, {session, game}) {
    let reversed = args[0].split("").reverse().join("")
    return reversed
}

module.exports = {
  playerCount:playerCount,
  reverse:reverse
}
```

`args` contains the transfer parameters specified in the level. A number of parameters must be passed as an _array_ .

> Note that "args" is always an array when the [function is called as variable](#functions-as-variables). Therefore it is recommended to always treat "args" as an array. This way you can use the function in any context.

`context` is an Object that contains `session` and `game`.

`context.session` contains functions and variables related to the session that called the function.

`context.game` contains functions and variables related to the game in which the session that called the function is running.

You also have access to the global variable `adaptor` that contains functions and variables related to the adaptor:ex server hosting the game with the corresponding session.

We will try to explain the various functions and variables here as soon as possible.
