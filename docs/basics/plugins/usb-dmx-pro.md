USB-DMX-PRO
===========

Steuere DMX Lichtsignale über eine dmx-usb-pro box.

Um die dmx-usb-pro box anschließen zu können musst du adaptor:ex auf einem Rechner mit USB Serial anschluss installiert haben.

Tutorials
---------

Im Tutorial [Licht kontrollieren mit adaptor eX und der ENTTEC DMXUSB Pro Box](../../tutorials/lightcontrol-dmx-usb-pro/index.md) erfährst du, wie du das **USB-DMX-PRO** Plugin einsetzen kannst.

Actions
-------

Steuere die Kanäle deiner dmx-box mit [Set Lights](../actions/usb-dmx-pro/setLights.md) an.