Telegram
=========

Versende Nachrichten über den Instant Messenger Telegram und reagiere auf eingehende Nachrichten in Einzel- und Gruppenchats.

Um Telegram in adaptor:ex nutzen zu können, muss adaptor:ex auf das Internet zugreifen können.

Tutorials
---------

In [Telegram Basics: Settings / Setup / Add Client](../../tutorials/telegram-settings/index.md) erfährst du, wie du den Telegram Messenger in adaptor:ex einrichtest und deine Telefonnummern einrichtest.

[Storytelling mit Telegram und adaptor eX](../../tutorials/telegram-basics/index.md) ist der Einstieg in das Versenden und empfangen von Textnachrichten. 

Details zum versenden von Mediendateien und Standortdaten findest du in [adaptor:ex telegram plugin media files und andere Spezialfunktionen](../../tutorials/telegram-send-media/index.md) 

Actions
-------

Versende Textnachrichten mit [Send Message](../actions/telegram/sendMessage.md).

Reagiere auf eingehende Nachrichten mit [Dialog](../actions/telegram/dialog.md).

Versende Dateien, Bilder, Sprach- und Videonachrichten mit [Send File](../actions/telegram/sendFile.md).

Versende Standortdaten in der telegram Kartenansicht mit [Send Geo](../actions/telegram/sendGeo.md).

Erstelle und versende eine Umfrage mit [Send Poll](../actions/telegram/sendPoll.md).

Erstelle und versende eine Umfrage mit richtiger Antwort mit [Send Quiz](../actions/telegram/sendQuiz.md).

Erstelle Gruppenchats mit [Create Chat](../actions/telegram/createChat.md).

Ändere die Einstellungen von Gruppenchats und lade Mitglieder in eine Gruppe ein mit [Edit Chat](../actions/telegram/editChat.md).