Twilio
======

Mache Anrufe und Versende SMS Nachrichten und reagiere auf eingehende Anrufe und SMS Nachrichten über die [Twilio](https://www.twilio.com/) Voice und SMS API.

Um dieses Plugin zu nutzen brauchst du einen Twilio account und musst dir dort eine Telefonnummer bestellen. Das bestellen und nutzen von Telefonnummern auf Twilio ist ein kostenpflichtiger Service.

Damit adaptor:ex auf eingehende SMS reagieren und Anrufe verwalten kann, muss der adaptor:ex Server mit dem Internet verbunden und aus dem Internet erreichbar sein.

adaptor:ex aus dem Internet erreichbar machen
----------------------------------------------

Starte deinen adaptor:ex server mit der option `--host` um eine Webadresse als hostname zu übergeben unter der, der Rechner oder Server auf dem adaptor:ex läuft, aus dem Internet zu erreichen ist. Z.B.:

`adaptorex --host https://myveryownwebdomain.org`

Du kannst natürlich auch die statische IP angeben

`adaptorex --host 123.456.7.890`

Wenn du adaptor:ex mit Twilio auf einem lokalen Rechner benutzen willst musst du einen IP Weiterleitungs Service nutzen.

Als Besitzer einer Fritzbox kannst du dir z.B. eine [MyFritz](https://myfritz.net) Adresse einrichten und auf deiner Fritzbox Port Weitergaben zum adaptor:ex Server auf deinem Rechner einrichten.

Eine andere Möglichkeit ist es einen Service wie [ngrok](https://ngrok.com/) zu nutzen der die Weiterleitung für dich übernimmt und eine Domain bereitstellt.

Eine Telefonnummer einrichten
------------------------------

In adaptor:ex, öffne `Game > settings` und füge das Twilio Plugin zu deinem Game hinzu

![Das Twilio Plugin dem Game hinzufügen](./assets/twilio_add_plugin.png)

Um Twilio und adaptor:ex zu verbinden brauchst du deine Twilio **SID**, deinen Twilio **Token** und eine Twilio **Telefonnummer**.

### SID und Token

Registriere dich auf der Twilio Webseite: [www.twilio.com/](https://www.twilio.com/)

Öffne dein Twilio Nutzerkonto um auf das Dashboard zu gelangen. Auf der Startseite findest du deine **SID** und den **Token**. 

![Das twilio Dashboard mit SID und Token](./assets/twilio_id_and_token.png)

Kopiere die beiden Werte und trage sie in adaptor:ex in den Twilio Plugin Settings im jeweiligen Feld ein.

![SID und Token in den Twilio Plugin Settings angeben](./assets/twilio_settings.png)

### Telefonnummer

Um eine telefonnummer hinzuzufügen wähle in deinem Twilio Account Dashboard `Phone Numbers > Manage > Buy A Number` aus. Stelle sicher, dass du eine Nummer auswählst die sowohl "Voice" als auch "SMS" Fähigkeit hat.

![Eine Twilio Telefonnummer kaufen](./assets/twilio_buy_number.png)

> Die nationalen und Twilio internen Regelungen zum Erwerb einer Telefonnummer ändern sich immer wieder, deshalb können wir an dieser Stelle leider keine genaue Anleitung bereitstellen. Twilio wird entsprechende Nachweise von dir verlangen, bevor du endgültig eine Nummer bestellen kannst.

Wenn du eine Telefonnummer erstanden hast, erstelle im Twilio Plugin ein neues *PHONE* Item.

![Screenshot, ein neues phone item mit dem plus button erstellen](./assets/twilio_add_phone.png)

Wähle den `info` Reiter aus um deinem Twilio phone einen Namen zu geben. Das Twilio Telefon im Beispiel hat den Namen "Kuhlmann". Eine Figur aus unserem Messenger Adventure *Lockdown*.

Gib unter `settings` die Twilio Telefonnummer (`telnumber`) an.

![Screenshot, die Einstellungen für das twilio phone](./assets/twilio_save_phone.png)

> Achte darauf, dass die `telnumber` keine Leerzeichen enthält und mit `+` beginnt.

klicke anschließend auf `SAVE` um das Item zu erstellen.

Deine Telefonnummer kann jetzt mit [Send SMS](../actions/twilio/smsout) über die Twilio API SMS versenden. 

### Webhooks einrichten

Um SMS zu empfangen und Anrufe tätigen zu können musst du Twilio noch auf deinen adaptor:ex Server verweisen.

Öffne die Twilio Plugin Settings in adaptor:ex und wähle das Phone Item aus, dass du im Twilio Account anbinden willst.

Kopiere die Angaben unter `webhooks`

![Screenshot, webhooks in den phone item settings](./assets/twilio_phone_webhooks.png)

und füge sie in deinem twilio Account in den Einstellungen der Telefonnummer (`Phone Numbers > Manage > Active Numbers`) ein:

![Screenshot, call webhooks in Twilio angeben](./assets/twilio_call_webhook.png)

![Screenshot, sms webhooks in Twilio angeben](./assets/twilio_sms_webhook.png)

### Default Level angeben

Das `default level` kannst du in den Settings des Twilio phone angeben. Wähle es aus den Levels, die du in deinem Game erstellt hast, aus.

![Screenshot game twilio plugin settings phone item default level](./assets/twilio_default_level.png)

Wird dein Twilio phone per SMS oder Anruf Kontaktiert und ist nicht bereits in einer [Incoming SMS](../actions/twilio/smsin.md) oder [Incoming Call](../actions/twilio/callin.md) Action mit dieser Telefonnummer, wird eine Session des `default level` gestartet.

Auch wenn das entsprechende `default level` bereits aktiv ist wird, auch ohne aktive SMS oder Call action, keine weitere `default level` Session gestartet.

Das kontaktierende *player* [Data Item](../variables.md#item-container-und-level-arguments) ist innerhalb des Levels als "Player" argument adressierbar.

Gibt es kein *player* Data Item mit der Kontaktierenden Telefonnummer, wird ein neues Data Item in der *player* Collection erstellt.

### Telefonnummern von verschiedenen Accounts

Du kannst für jedes Phone eigene Accountdaten festlegen indem du in den `Settings` des Phone `sid` und `token` angibst.

Nur Phone Items, die selbst keine sid und keinen token angeben werden über `sid` und `token` des Twilio Plugins verbunden.

Actions
-------

Verschicke eine SMS von einer Twilio Telefonnummer mit [Send SMS](../actions/twilio/smsout.md).

Reagiere auf eingehende SMS mit [Incoming SMS](../actions/twilio/smsin.md).

Starte einen Anruf mit [Outgoing Call](../actions/twilio/call.md).

Reagiere auf eingehende Anrufe mit [Incoming Call](../actions/twilio/callin.md)

Starte einen Studio Flow den du in Twilio Studio erstellt hast mit [Twilio Studio Flow](../actions/twilio/flow.md)