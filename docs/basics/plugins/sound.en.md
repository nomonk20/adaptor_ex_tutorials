Sound Player
============

Play audio files on your computer.

In order to be able to use the sound plugin, adaptor:ex must be installed on a device with sound output.

To use the **sound** plugin on a computer with a Windows operating system, we recommend downloading the free [mplayer](http://www.mplayerhq.hu/) .

Download a version of [mplayer](http://www.mplayerhq.hu/design7/dload.html#binaries) and add the mplayer files folder to your path environment variables.

## actions

With [Play](../actions/sound/play.md) you can play sound files.
