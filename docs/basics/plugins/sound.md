Sound Player
============

Spiele Audiofiles über deinen Rechner ab.

Um das Sound Plugin nutzen zu können muss adaptor:ex auf einem Endgerät mit Soundausgabe installiert sein.

Um das **Sound** Plugin auf einem Rechner mit Windows Betriebssystem zu nutzen empfehlen wir dir, den kostenlosen [mplayer](http://www.mplayerhq.hu) herunterzuladen.

Lade eine Version des [mplayer](http://www.mplayerhq.hu/design7/dload.html#binaries) herunter und füge den mplayer Dateiordner zu deinen Pfad Umgebungsvariablen hinzu.

Actions
-------

Mit [Play](../actions/sound/play.md) kannst du Soundfiles abspielen.
