Files and media files
========================

To use files in actions, save them in the folder  `files`  of the game that you find in the adaptor:ex data directory.

Where you find the data directory depends on your adaptor:ex setup.

If you installed adaptor:ex with the default settings, a new folder `data` was created where you run the application. To use files in your game, copy or move them to `data/games/<game name>/files`.

For example, to add an audio file "ambient_starlight.wav" to your game "Playground", save it in the appropriate `files` folder.

```
data
├── games
|   ├── Playground
│   |   ├── files
│   |   |   ├── ambient_starlight.wav
│   |   ├── functions
├── log
├── nedb
├── config.json
```

You can also add more subfolders to sort the files in the `files` folder.

To use a file in an action, select it in the corresponding form field with `Select File`from the dropdown.

![Eine Datei auswählen](./assets/files_select_file.png)

### Media

You can use `Drop File` to copy them from the TOOLBAR.

All files are listed in the adaptor:ex editor in the TOOLBAR . Drag the file you want to use in an action into the appropriate form field.

![Dateien finden und verwenden](./assets/files_drop_file.png)

As you can see, only the file path is given and the action searches for the file accordingly. You can also address the files manually.

The starting point for the relative file path is the `files` folder of the respective game. However, you can also use any absolute path that points to a file in the file system on which the adaptor:ex server is running.

Some actions can also load files from the Internet with hyperlinks, i.e. with a URL. Such file references must begin with `http` or `https`. So for example:`https://machinacommons.world/img/logo-quer-700px.png`

Copy the URL into the appropriate form field of the action.