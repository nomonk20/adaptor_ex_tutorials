Set Lights
==========

**Plugin**: [usb-dmx-pro](../../plugins/usb-dmx-pro.md) | **Mode**: [Run](../../editor.md#run-actions)

Ändere die DMX Werte deiner DMX-USB-PRO Box

Du kannst jedem Kanal einen neuen Wert zuordnen.

Im Tutorial [Licht kontrollieren mit adaptor eX und der ENTTEC DMXUSB Pro Box](../../../tutorials/lightcontrol-dmx-usb-pro/index.md) findest du mehr über die **Set Lights** action heraus.