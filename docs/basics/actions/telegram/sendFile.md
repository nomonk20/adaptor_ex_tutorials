
Send File
=========

**Plugin**: [Telegram](../../plugins/telegram.md) | **Mode**: [Run](../../editor.md#run-actions)

Mit der Telegram action **Send File** können Dateien in verschiedenen Formaten vom Client an die Spieler:in versendet werden. 

 <img src="../../../tutorials/telegram-send-media/assets/3-1.png" width="300px">

[Hier](../../../tutorials/telegram-send-media/index.md){target=_blank} findest du ein Tutorial zu dieser action. 

## Grundeinstellungen

**Telegram Client**: Der Telegram-Account, der die Datei versenden soll. <br>
**To**: Die Spieler:in oder Spieler:innen, an die die Datei geschickt werden soll. <br>
**File**: Der Dateiname der zu versendenden Datei. <br>
Dateien können entweder via Drag and Drop aus der linken Toolbar "Media" direkt in das Feld gezogen werden (Dropdown-Auswahl: *Drop File*), oder via Dropdown ausgewählt werden (Dropdown-Auswahl: *Select File*). 

Im Toolbar Tab "Media" befinden sich die Dateien, die auf dem adaptor:ex-Server unter 'files/' für dieses Game abgelegt wurden.

<img src="../../../tutorials/telegram-send-media/assets/4.png" width=40%>

[Hier](../../files.md) findest du mehr Infos zu Dateien in adaptor:ex.

Weitere Einstellungen (hinzufügbar über "Settings"):<br>
**Pin**: Setzt die Nachricht als gepinnte Nachricht in den Chat<br>
**Caption**: Fügt der Datei eine Nachricht/Beschreibung/Bildunterschrift hinzu<br>

## Bilddateien

Wenn wir in **file** eine Bilddatei (.png, .jpeg) einsetzen, wird der Telegram-Client ein Foto an die Spieler:innen versenden.

<img src="../../../tutorials/telegram-send-media/assets/7.png" width="350px">
Photo by <a href="https://unsplash.com/@charlesdeluvio?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Charles Deluvio</a> on <a href="https://unsplash.com/s/photos/balloon-dog?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>

Auch eine URL zu einem Bild im Internet führt dazu, dass der Client dieses Bild versendet. Einen Bild-Link erkennst du daran, dass sich am Ende der URL eine Dateiendung, z.B. .png oder .jpeg befindet.<br>
Bitte achte darauf, dass die Nutzungsrechte dieser Datei deine Verwendung erlauben.<br>

**format**: für ein Bild sollte hier `auto` eingestellt sein. Das Bild wird, solange es unter 10 MB groß ist, dann auch sofort eingeblendet, vorausgesetzt die empfangende Person erlaubt dies in ihrer Telegram App. <br>
Bilder über 10MB werden den Empfangenden standardmäßig als Download angezeigt.<br>

## Audio Files und Voice Messages

Audio-Dateien haben Dateiendungen wie .mp3, .wav und .ogg.

.mp3 und .wav werden den Spieler:innen immer als Audio-Downloadlink angezeigt.

.ogg Dateien können auch als Audio-Downloadlink verschickt werden. <br>
Wenn jedoch **format** auf `voice` eingestellt wird, die .ogg Datei korrekt encoded ist (OPUS codec) und wenn sie nicht größer als 1MB ist, wird Telegram die Datei als Voice Message anzeigen, so als hätte der Telegram Client sie selbst eingesprochen.

<img src="../../../tutorials/telegram-send-media/assets/16.png" width=51%><br>
So sollten die Einstellungen aussehen, damit eine Audiodatei als Voice Message versendet wird.

<img src="../../../tutorials/telegram-send-media/assets/17.png" width=51%><br>
So sieht eine Voice Message aus.

## Videos

Mit **format** auf `video` können auch Videodateien direkt als Video im Messenger angezeigt werden.

## Andere Files

Andere Dateitypen werden als Downloadlink verschickt. 

Die Darstellung der Dateien hängt nicht nur von Dateiformat und der Größe der Datei, sondern auch von den Einstellungen in der Telegram App der empfangenden Person ab.
