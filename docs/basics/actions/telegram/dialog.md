
Dialog
========

**Plugin**: [Telegram](../../plugins/telegram.md) | **Mode**: [Listen](../../editor.md#listen-actions)

Reagiere auf vielfältige Weise auf eingehende Telegram Nachrichten.
## Grundlagen

Ein Dialog ist ein Gesprächsbaustein, der uns ermöglicht, auf verschiedene Antworten der Spieler:innen unterschiedlich zu reagieren.

Default Einstellungen: <br>
**Telegram Client:** Der Telegram Client, welcher den Dialog führt. <br>
**chat:** Die Spieler:in, bzw. der Gruppenchat, die am Dialog teilnehmen. <br>

Zusätzliche Einstellungen: <br>
**from:** Wenn der Dialog auf eine Nachricht von einer bestimmten Spielerin oder Client hören soll. Nur sinnvoll in Gruppenchats. Hier kann der Spielername als Variable aus der linken Toolbar "Variablen" hineingezogen werden.<br>
**priority:** Bestimmt die Priörität des Dialogs, falls es mehrere Dialoge gleichzeitig gibt (auf anderen Pfaden bzw. Erzählsträngen des Games).<br>
**if:** Dient in Kombination mit "else" der Abfrage von Bedingungen. (s. Erläuterung unten)<br>
**else:** Tritt ein, wenn keine der if-Bedingungen erfüllt wurde. (s. Erläuterung unten)<br>
**timeout:** Sendet nach abgelaufener Zeit eine Nachricht (oder mehrere) an den Chat und geht zum nächsten State über.<br>


## Abfrage von Bedingungen (Conditions)

Mit **if** kann eine oder mehrere Bedingungen abgefragt werden. Dabei wird der Inhalt der Nachricht(en) überprüft, die eingehen, während der State (mit dieser dialog action) aktiv ist. <br>
Dazu auf den Button "add condition" klicken. <br>
Im Dropdown neben **condition 1** besteht nun die Wahl zwischen verschiedenen Abfragearten:<br>
**equals:** Der Inhalt der Nachricht entspricht exakt dem angegebenen Text. <br>
**contains:** Die Nachricht beinhaltet den angegebenen Text. <br>
**contains media:** Die Nachricht beinhaltet eine Datei. Anschließend kann der Dateityp sowie Dateipfad zum Herunterladen angegeben werden. <br>
**less than / greater than:** Die Nachricht ist eine Zahl dessen Wert Größer oder Kleiner als der hier angegebene Wert ist.<br>
**regular expression:** Die Nachricht matched die hier angegebene RegEx.<br>
**javascript function:** Die javascript Funktion mit der Nachricht als Übergabe Parameter gibt einen [Truthy](https://developer.mozilla.org/en-US/docs/Glossary/Truthy) Wert zurück.<br>
**mongo query:** Die mongo query findet ein Item in einer deiner collections.<br>

Im Glossar Kapitel [Bedingungen Abfragen mit Conditions](../../conditions.md#abfragetypen-operatoren) findest du mehr darüber heraus, wie du die verschiedenen Abfragearten verwendest.

Sollte eine der Bedingungen eintreten, kann unter **respond** eine (oder mehrere) Antwort-Nachricht(en) des Client festgelegt werden, sowie der darauf folgende State (unter **next state**).

Wenn du mehrere Antwortnachrichten unter **respond** angibst, wird mit jedem Mal, dass die Bedingung zutrifft, die jeweils nächste Antwort in der Liste gesendet.

Mit der letzten Antwort der Liste wird in den **next state** gewechselt.

Wenn keine weitere Antwort zur Verfügung steht und kein **next state** angegeben ist, die Bedingung aber erneut eintrifft, passiert nichts.

In **else** wird angegeben was passiert, falls bei einer eingehenden Nachricht keine der if-Bedingungen eingetreten sein sollte. 

Du kannst auch hier eine oder mehrere Nachrichten angeben, die mit jedem mal, dass keine der **if** Bedingungen eintritt, nacheinander gesendet werden. Mit der letzten Nachricht, oder sofort, falls du keine **respond** Nachricht angegeben hast, wird in **next state** gewechselt. 

Im Dropdown besteht die Wahl zwischen "respond" (normale Nachricht) und "reply" (Antwort auf die Nachricht).

