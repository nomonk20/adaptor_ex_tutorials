Send Poll
=========

**Plugin**: [Telegram](../../plugins/telegram.md) | **Mode**: [Run](../../editor.md#run-actions)

Erstelle und versende eine Umfrage in einem Gruppenchat.

Umfragen können anschließend mithilfe der [Umfragedaten](#umfragedaten) ausgewertet werden.

Settings
--------

### Telegram Client

Der telegram client account, der die Umfrage versendet. Der Client muss Mitglied im `to` Gruppenchat sein.

### to

Der Gruppenchat an den die Umfrage gesendet wird.

### Name

Der name unter dem die Umfrage im Chat Data Item gespeichert wird. Mit `name` kannst du die Umfrage adressieren und Auswerten.

Umfragen werden im Chat Dokument in der *chats* Data Collection unter "poll" gespeichert. Rufe die Umfragedaten z.B. innerhalb einer Variable auf: `[[chats.Chatname.poll.Pollname]]`

### Question

Der Fragetext, der in der Umfrage gestellt wird.

### Answers

Eine Liste von Antwortmöglichkeiten. Füge beliebig viele Antworten mit `Add Answer` hinzu.

### Anonymous Vote

Gib an, ob eingesehen werden wie die anderen Gruppenmitglieder abgestimmt haben.

Wähle die Option an, um das Abstimmungsverhalten verdeckt zu lassen.

### Multiple Choice

Gib an, ob mehrere Antworten ausgewählt werden können.

Umfragedaten
------------------

Die Daten der Umfrage werden im entsprechenden Gruppenchat Item in der "poll" variable abgespeichert. Um verschiedene Umfragen unterscheiden zu können ist jede Umfrage unter der `Name` Einstellung abgelegt.

> Beachte, dass, wenn du eine Umfrage mit der selben `name` Eigenschaft im selben Chat erneut erstellst, die Umfragedaten der vorherigen Umfrage überschrieben werden.

Zum einen kannst du so auf die Ausgangsdaten wie `question` und `answers` zugreifen. Zum anderen werden die Daten mit jeder Stimmabgabe eines Gruppenmitglieds aktualisiert und geben z.B. die Anzahl der Mitglieder an, die bereits abgestimmt haben (`voters_count`).

### Ausgangsdaten

* **`name`** - der Name der Umfrage. 

  *Beispiel*: `[[Groupchat.poll.my_poll.name]]` ergibt "my_poll"

* **`question`** - Der Fragetext. 

  *Beispiel*: `[[Groupchat.poll.my_poll.question]]`

* **`answers`** - Liste der Antwortmöglichkeiten beginnend mit `0`

  *Beispiel*: `[[Groupchat.poll.my_poll.answers.2]]` gibt den Antworttext der 3. Antwort aus.

* **`private`** - Boolean ("true" oder "false") der angibt, ob die Umfrage offen oder geheim stattfindet.

  *Beispiel*: `[[Groupchat.poll.my_poll.anonymous]]` ergibt "true"

* **`multiple_choice`** - Anonymous Vote boolean ("true" oder "false") der angibt, ob mehrere Antworten ausgewählt werden können sind.

* **`correct_answer`** - die richtige Antwort (Nur Quiz!)

  Beispiel: `[[Groupchat.poll.my_poll.correct_answer]]` ergibt "Ja", wenn "Ja" die korrekte Antwortoption ist.


### Aktualisierte Daten

* **`voters_count`** - Anzahl der Gruppenmitglieder die bisher an der Umfrage teilgenommen haben.

  *Beispiel*: `[[Groupchat.poll.my_poll.voters_count]]` ergibt 1 wenn ein Gruppenmitglied abgestimmt hat

* **`voters`** - Liste der Telegram ids der Mitglieder die bereits an der Umfrage teilgenommen haben.

  *Beispiel*: `[[Groupchat.poll.my_poll.voters.0]]` ergibt 1234567 wenn das erste Gruppenmitglied, dass an der Umfrage teilgenommen hat die Telegram id 1234567 hat

* **`ambiguous`** - Boolean ("true" oder "false") der angibt ob es genau eine oder mehrere meist gewählte Antwortoptionen gibt.

  `ambiguous` ist "true" wenn mehrere Antwortoption die gleiche Stimmzahl bekommen haben, aber nur, wenn es sich um die höchste Stimmzahl handelt.

  *Beispiel*: `[[Groupchat.poll.my_poll.ambiguous]]` ergibt "false" wenn es ein Eindeutiges Ergebnis für die erste Wahl gab.

* **`results`** - Liste der aktuellen Umfrageergebnisse in der Reihenfolge der `Answers`. Jeder Eintrag in `results` hat die folgenden Eigenschaften:

  * `votes` - Anzahl der abgegebenen Stimmen für diese Antwort
  * `answer` - Text der Antwortoption
  * `rank` - Position im Ranking der meisten abgegebenen Stimmen für die Antwortoption (0 für die meisten Stimmen, 1 für die zweitmeisten etc.)
  * `correct` - Boolean ("true", "false") ob dies die richtige Antwort ist (nur Quiz!)

  Nutze den entsprechenden PLatz in der Liste der Antworten (beginnend bei `0`) um das entsprechende Antwortergebniss aufzurufen.

  *Beispiel*: `[[Groupchat.poll.my_poll.results.2.votes`]] ergibt 5, wenn für die 3. Antwortmöglichkeit 5 Stimmen abgegeben wurden

* **`ranking`** - Liste der aktuellen Umfrageergebnisse in der Reihenfolge der meisten abgegebenen Stimmen für die Antwortoption. Jeder Eintrag in `ranking` hat die folgenden Eigenschaften:

  * `votes` - Anzahl der abgegebenen Stimmen für diese Antwort
  * `answer` - Text der Antwortoption
  * `index` - Position in der reihenfolge in der die Antwortoptionsn ursprünglich angeordnet sind (0 für die erste Antwort, 1 für die zweite etc.)
  * `correct` - Boolean ("true", "false") ob dies die richtige Antwort ist (nur Quiz!)

  *Beispiel*: `[[Groupchat.poll.my_poll.ranking.0.answer]]` ergibt "Ja", wenn für die die Antwortoption "Ja" die meisten Stimmen abgegeben wurden.

  Beachte, dass `ranking` bei gleicher Anzahl Stimmen die ursprünglich obere Antwort bevorzugt!

Beispiel
---------

Erstelle einen neuen chat mit [Create Chat](./createChat.md) und gib als `reference` "Groupchat" an oder verwende einen existierenden Chat, den du als "Groupchat" in das Level geladen hast.

Ziehe eine **Send Poll** action auf die STAGE und gib dem neuen State den Namen "LeakItOrNot". Gib den `Telegram Client` an und unter `to` den Chat "Groupchat". Gib einen kurzen Namen unter `name` an, hier "leak" und formuliere die Frage: "Was soll ich mit der CD tun?".

Klicke unter `Answer Options` auf `Add Answer` und füge zwei Antwortmöglichkeiten hinzu.

![Die send poll action](./assets/poll_send_example.png)

Nun erstellen wir zunächst eine Abfrage ob die Umfrage durchgeführt wurde.

Ziehe eine [On Change](../logic/onChange.md) action in den "LeakItOrNot" State. Füge mit `Add Condition` eine neue If Bedingung hinzu in der du abfragst, ob alle Gruppenmitglieder an der Umfrage teilgenommen haben.

Verwende die "equals" Abfrage und gib für **value** `[[Groupchat.poll.leak.voters_count]]` an und für **equals** `[[Groupchat.member_count]]`. Gib als `next state` "CheckPoll" an.

![Abfragen ob alle Gruppenmitglieder gewählt haben](./assets/poll_all_voted.png)

Um die Umfrage Auswertung nicht bedingungslos davon abhängig zu machen, dass alle Gruppenmitglieder mitmachen wollen, fügen wir noch eine [Timeout](../time/timeout.md) action zu "LeakItOrNot" hinzu.

![5 Minuten bis zur Auswertung](./assets/poll_timeout.png)

Wenn nach 5 Minuten nicht alle Gruppenmitglieder abgestimmt haben wird einfach mit den bestehenden Stimmen ausgewertet.

Ziehe jetzt eine [Switch](../logic/switch.md) action auf die STAGE und benenne den neuen State in "CheckPoll" um.

Hier überprüfen wir welche Antwortoption die meisten Stimmen bekommen hat.

Erstelle drei neue conditions. 

In der 1. Bedingung überprüfen wir ob das Ergebnis eindeutig war. Gib für **value** `[[Groupchat.poll.leak.ambiguous]]` an und für **equals** `true`. Gib für **next state** `Undecided` an.

In der 2. Bedingung überprüfen wir ob die erste Antwortoption die meisten Stimmen erhalten hat. Gib für **value** `[[Groupchat.poll.leak.results.0.rank]]` und für **equals** die Zahl `0` an. Gib für **next state** `Destroy` an.

In der 3. Bedingung überprüfen wir ob die zweite Antwortoption die meisten Stimmen erhalten hat. Gib für **value** `[[Groupchat.poll.leak.results.1.rank]]` und für **equals** die Zahl `0` an. Gib für **next state** `Leak` an.

![Die Umfrage mit der switch action evaluieren](./assets/poll_evaluate.png)

Es gibt also 3 Mögliche Ausgänge der Umfrage, mit denen wir rechnen müssen. Für jede Variante erstellen wir neue States.

![Das Level in der Übersicht](./assets/poll_full_example.png)

Wenn der State "LeakItOrNot" ausgelöst wird, wird eine neue Umfrage in "Groupchat" gesendet und darauf gewartet, dass alle Gruppenmitglieder an der Umfrage teilnehmen.

![Die Umfrage im telegram Messenger](./assets/poll_chat_lol.png)

Sobald die beiden anderen Gruppenmitglieder ihre Stimme abgegeben haben oder sobald der timeout abgelaufen ist, wird die Umfrage ausgewertet und der entsprechende State ausgelöst.

![Die Abgeschlossene Umfrage im telegram Messenger](./assets/poll_chat_lol2.png)