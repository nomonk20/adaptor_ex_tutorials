Create Chat
===========

**Plugin**: [Telegram](../../plugins/telegram.md) | **Mode**: [Run](../../editor.md#run-actions)

Erstelle einen neuen Telegram-Gruppenchat oder -Kanal.

Der gewählte Telegram Client erstellt einen neuen Gruppenchat oder Kanal und lädt andere Telegram-Accounts in den Chat ein.

In adaptor:ex wird gleichzeitig ein [Data Item](../../variables.md#collections-und-data-items) in der `chats` collection angelegt, das den Chat repräsentiert.

Settings
--------

Du kannst bereits beim Erstellen des Chats die meisten Optionen festlegen. Wenn du bestimmte Optionen später ändern willst, nutze die [Edit Chat](./editChat.md) action.

### Telegram Client

Der Client, der den Chat erstellen soll. Dieser Account ist automatisch Admin der neu erstellten Gruppe.

### type

Lege fest, ob du einen Gruppenchat (`chat`) oder Kanal (`channel`) erstellen willst. Hier ist der Unterschied erklärt: [https://telegram.org/faq#f-was-ist-der-unterschied-zwischen-gruppen-und-kanalen](https://telegram.org/faq#f-was-ist-der-unterschied-zwischen-gruppen-und-kanalen)

### name

Lege einen Namen für deine Gruppe oder deinen Kanal fest. Wenn du keinen `title` festlegst erscheint `name` im Titel des Chats.

Wenn `name` keine Sonderzeichen oder Leerzeichen enthält kannst du `name` nutzen um den Chat in einem anderen Level bzw. in einer anderen Session zu adressieren. Etwa: `[[chats.Chatname]]`

Bedenke, dass `name` in diesem Fall für jeden Chat einzigartig sein muss.

Um den Chat im aktuellen Level bzw. der aktuellen Session eindeutig zu adressieren empfehlen wir `reference` verwenden.

### title

Gib einen Titel für den Chat an.
### members

Eine Liste der Mitglieder, die der Gruppe hinzugefügt werden sollen.

Verwende Data Items, player oder telegram clients, die mit einem Telegram-Account verbunden sind.

`Player` - füge den player aus dem level Argument Player hinzu

`player.{'telegram.first_name':'Amanda'}` - füge den Spieler-Eintrag aus der `player` collection hinzu, der in seinem Telegram Account den Vornamen "Amanda" angegeben hat

`telegram.client.Chris` - lade den Telegram Client `Chris` in den Chat ein.

### photo

Lade ein Gruppenbild für den Chat hoch. 

Nutze dafür eine Bilddatei aus deinen [Files](../../files.md).

### about

Füge eine Textbeschreibung für die Gruppe hinzu.

### permissions

Lege fest, welche Interaktionen für Mitglieder des Chats, die nicht Admins sind, erlaubt sind.

Verbiete eine Interaktionsmöglichkeit, indem du die permission abwählst.

![Die verschiedenen Möglichkeiten Interaktionen zu beschränken](./assets/chat_permissions.png)

Gruppenmitglieder ohne Admin-Status können in diesem Beispiel keine Mediendateien, GIFs oder Sticker versenden.

### reference

Gib beim Erstellen des chats einen `reference` Wert an. So kannst du den Chat anschließend im selben Level einfach adressieren. 

Wenn du die Referenz in den Telegram actions als `chat` oder `to` angibst, wird der Telegram Chat adressiert. In anderen actions verweist die Referenz auf das `chat` Data Item und du kannst auf seine Eigenschaften zugreifen.
 
Chatdaten
---------

Das Data Item, das mit dem Chat erstellt wurde, enthält Informationen über den Chat, die beständig aktualisiert werden.

> Beachte: wenn der adaptor:ex Server nicht aktiv ist während Änderungen am Chat passieren, werden diese Änderungen nicht im Data Item angepasst

Über `reference` oder `name` kannst du das Chat Item adressieren und die Chat Daten als variablen in anderen actions verwenden.

Die folgenden Daten können im Chat Item enthalten sein

* **`name`** - der Name des Chats. `name` ist nicht notwendigerweise der Titel des Chats

* **`title`** - Der Anzeigename des Chats

* **`members`** - Liste der telegram ids der Gruppenmitglieder

* **`member_count`** - Anzahl der Gruppenmitglieder. Der Client, der den Chat erstellt hat wird hier nicht mitgezählt!

* **`telegram`** - Telegram Eigenschaften dieses chats wie `id` und `participants_count`

* **`poll`** - Umfragen, die in diesem Chat mit [Send Poll](./sendPoll.md) oder [Send Quiz](./sendQuiz.md) erstellt wurden. `poll` enthält [Send Poll Umfragedaten](./sendPoll.md#umfragedaten)


Beispiel
--------

![Einen neuen Gruppenchat erstellen](./assets/create_chat_example.png)

Erstellt einen neuen Gruppenchat "Better Together" für den Telegram Client "Thekla", in den der Telegram-Account von "Player" und dem player mit der `name` variable "Arvid" eingeladen wird.

![Der neue Gruppenchat in Telegram](./assets/new_telegram_chat_android.png)

Da wir beim Erstellen des chats einen `reference` Wert angegeben haben, können wir den Chat anschließend im selben Level z.B. in der [Send Message](./sendMessage.md) action adressieren.

![Eine Nachricht an einen Gruppenchat senden](./assets/send_message_to_chat.png)

![Die Nachricht im Gruppenchat](./assets/to_telegram_chat_android.png)

Du kannst den Chat auch über die `chats` collection adressieren, z.B. wenn du ihn in einem Level verwenden willst, das nicht über die Referenz verfügt.

Adressiere den Chat über eine Query:

`[[chats.{name:"Better Together"}]]`

Wenn du keine Leerzeichen verwendet hast, kannst du den Namen auch direkt verwenden, z.B.:

`[[chats.BetterTogether]]`

![Einen Gruppenchat per Name adressieren](./assets/send_message_to_chat_name.png)
