Edit Chat
=========

**Plugin**: [Telegram](../../plugins/telegram.md) | **Mode**: [Run](../../editor.md#run-actions)

Ändere die Einstellungen eines Telegram-Gruppenchats.

Passe etwa die Beschreibung, den Namen oder das Profilbild von deinem Gruppenchat oder Kanal an.

Settings
--------

### Telegram Client

Der Client, der den Chat bearbeiten soll. Dieser Account muss Admin der Gruppe sein.

### chat

Der Chat, der bearbeitet werden soll. Nutze den `reference` Namen oder adressiere einen Chat in der `chats` collection.

### add members

Lade Telegram-Accounts in den Chat ein.

Gib hier telegram clients oder player Data Items, die mit einem Telegram-Account verbunden sind, an.

### remove members

Entferne Telegram-Accounts aus dem Chat.

Gib hier telegram clients oder player Data Items an , die mit einem Telegram-Account verbunden sind und sich in diesem Chat befinden.

### photo

Lade ein Gruppen-Profilbild hoch, oder ändere das aktuelle Bild.

Nutze dafür eine Bilddatei aus deinen [Files](../../files.md).

### about

Ändere die Gruppenbeschreibung.

### permissions

Lege fest, welche Interaktionen für Mitglieder des Chats, die nicht Admins sind, erlaubt sind.

Verbiete eine Interaktionsmöglichkeit, indem du die permission abwählst.

![Die verschiedenen Möglichkeiten Interaktionen zu beschränken](./assets/chat_permissions.png)

Gruppenmitglieder ohne Admin-Status können in diesem Beispiel keine Mediendateien, GIFs oder Sticker versenden.