
Send Geo Location
=================

**Plugin**: [Telegram](../../plugins/telegram.md) | **Mode**: [Run](../../editor.md#run-actions)

Mit *Send Geo* kann eine digitale Straßenkarte mit Markierung versendet werden. 

## Sende Kartenausschnitt mit Ort

**Telegram Client**: Der Telegram-Account, der die Geolocation versenden soll.<br>
**To**: Die Spieler:in oder Spieler:innen, an die die Geolocation geschickt werden soll. <br>
**Latitude**: Längengrad<br>
**Longitude**: Breitengrad<br>

ACHTUNG: Die Werte müssen englisch notiert werden, also mit Punkt anstatt eines Kommas.

Latitude und longitude von einem Ort sind z.B. über Open Street Maps oder Google Maps zu finden. <br>
Hier sind Beispiel-Koordinaten:<br>
Latitude: 52.438015 <br>
Longitude: 13.231191<br>

<img src="../../../tutorials/telegram-send-media/assets/13-1.png" width=45%>

<img src="../../../tutorials/telegram-send-media/assets/15.png" width=44%>
