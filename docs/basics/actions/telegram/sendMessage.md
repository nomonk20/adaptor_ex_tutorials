Send Message
============

**Plugin**: [Telegram](../../plugins/telegram.md) | **Mode**: [Run](../../editor.md#run-actions)

Versende eine Text Nachricht von einem deiner Telegram Clients an einen Telegram account.

**Telegram Client**: Der Telegram-Account, der die Nachricht versenden soll. <br>
**To**: Die Spieler:in oder Spieler:innen, an die die Nachricht geschickt werden soll. <br>
**text**: Die Nachricht, die versendet werden soll. <br>
**Pin**: Setzt die Nachricht als gepinnte Nachricht in den Chat. <br>
