Send Message
=============

**Plugin**: [Devices](../../plugins/devices.md) | **Mode**: [Run](../../editor.md#run-actions)

Send a message to a previously configured device.

`to` determines the device to `message` send to.

### JS objects

If is `message` a `string` and starts with `{` and ends with `}` , the message is `Object`interpreted as JS. Example:

`{colors:['green','blue','purple'],count:3}`

String `message`s can contain variables even if they `Object` are interpreted as JS.

### OSC-addresses
#### From the OSC device

Messages with OSC addresses received by adaptor:ex are converted to JSON. The message address path becomes JSON fields.

The message `/some/osc/message 123` will appear in adaptor:ex as

```
{
	"some":{
		"osc":{
			"message":123
		}   
	} 
}
```


interpreted.

So the message can be used in the [On Event](../logic/onEvent.md) action and the device `incoming` variable.

#### To the OSC device

If a JS is `Object` sent to an OSC device, all object fields are converted into addresses.

The message `{color:"pink"}` would be sent as an OSC address `/color` and value `pink`.

This `message` one with nested JS `Object`...

```
{
  "hello":"world",
  "one":{
    "two":{
      "three":[4, 5, 6, 7]
    },
    "deux":"trois",
    "zwei":3
  }
}

```

... would be converted into four OSC messages:
```
1.
Adresse:/hello, 
Wert:"world"
2.
Adresse:/one/two/three, 
Wert:[4, 5, 6, 7]
3.
Adresse:/one/deux, 
Wert:"trois"
4.
Adresse:/one/zwei, 
Wert:3

```

### Path

`path` is only available for HTTP and OSC devices. The specified path is used for addressing.

If send is used `path` for an _OSC_ device, the path is prefixed to any paths present in the message (see [OSC-Adresses](#OSC-addresses))

When send is sent `path` to an _HTTP_ device, the path is appended to the device's base URL path.

### methods

`method` is only available for HTTP devices and determines the HTTP request method of the message.

If specified `method` in **Send Message**, the Request method specified in Device Settings is overridden for this message.
