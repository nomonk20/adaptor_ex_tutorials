Send Message
=============

**Plugin**: [Devices](../../plugins/devices.md) | **Mode**: [Run](../../editor.md#run-actions)

Versende eine Nachricht an ein zuvor eingerichtetes Device.

`to` bestimmt das Device, an das `message` gesendet wird.
 
### JS Objekte

 Ist `message` ein `string` und beginnt mit `{` und endet mit `}`, wird die Nachricht als JS `Object` interpretiert. Beispiel:

 `{colors:['green','blue','purple'],count:3}`

 String `message`s können Variablen beinhalten, auch wenn sie als JS `Object` interpretiert werden.

### OSC-Adressen

#### Vom OSC-Device
Bei adaptor:ex eingehende Nachrichten mit OSC-Adressen werden in JSON umgewandelt. Der Adresspfad der Nachricht wird zu JSON-Feldern.

Die Nachricht `/some/osc/message 123` wird in adaptor:ex als

```json
{
  "some":{
    "osc":{
      "message":123
    }
  }
}
```

interpretiert. 

So kann die Nachricht in der [On Event](../logic/onEvent.md) action und der device `incoming` variable verwendet werden.

#### Zum OSC-Device
Wird ein JS `Object` an ein OSC-Device gesendet, werden alle Objektfelder in Adressen gewandelt.

Die Nachricht `{color:"pink"}` würde als OSC-Adresse `/color` und Wert `pink` versendet.

Diese `message` mit verschachteltem JS `Object` ...
``` json
{
  "hello":"world",
  "one":{
    "two":{
      "three":[4, 5, 6, 7]
    },
    "deux":"trois",
    "zwei":3
  }
}
```

... würde in vier OSC-Nachrichten gewandelt:

```
1.
Adresse:/hello, 
Wert:"world"
2.
Adresse:/one/two/three, 
Wert:[4, 5, 6, 7]
3.
Adresse:/one/deux, 
Wert:"trois"
4.
Adresse:/one/zwei, 
Wert:3
```

### Path

 `path` ist nur für HTTP- und OSC-Devices verfügbar. Der angegebene Pfad wird zur Adressierung verwendet.

Wird send mit `path` für ein *OSC*-Device verwendet, wird der Pfad ggf. in der Message vorhandenen Pfaden vorangestellt (siehe [OSC-Adressen](#osc-adressen))

Wird send mit `path` an ein *HTTP*-Device gesendet, wird der Pfad an den Basis-URL-Pfad des Device angehängt.

### Methoden

`method` ist nur für HTTP-Devices verfügbar und bestimmt die HTTP-Request-Methode der Nachricht.

Wird `method` in **Send Message** angegeben, wird die Request-Methode, die in den Device Settings angegeben ist, für diese Nachricht überschrieben.
