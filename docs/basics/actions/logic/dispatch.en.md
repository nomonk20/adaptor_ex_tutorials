Dispatch Event
==============

**Plugin**: *Logic* | **Mode**: [Run](../../editor.md#run-actions)

Trigger an event that can be responded to elsewhere with [On Event](./onEvent.md).

## Settings

### Name

The name of the event. This name can be used to respond to this event elsewhere.

### Range

Choose "game" if you want to be able to receive the event in every session of every level in your game.

Select "session" if you only want to be able to receive the event within the session of this level in which it is triggered.

### Message

Attach more information to your event.

You can use JS Object Notation to attach a JS Object Message to the event.
