On Event
========

**Plugin**: *Logic* | **Mode**: [Listen](../../editor.md#listen-actions)

Wartet auf das Eintreffen eines bestimmten Ereignisses (Event) und wechselt in einen nächsten State, wenn das angegebene Event eintrifft. 

Zusätzlich ist es möglich den Inhalt (Payload) eines Event auf eine Bedingung abzufragen und nur bei zutreffender Bedingung in einen nächsten State zu wechseln.

Reagiere etwa auf Ereignisse, die mit [Dispatch Event](./dispatch.md) ausgelöst wurden.

Auch [Devices](../../plugins/devices.md) lösen Events aus, die den selben Namen tragen wie das Device, wenn das Device eine Nachricht an adaptor:ex schickt.

**On Event** wartet solange auf Events, bis eines der Events, und ggf. der Bedingungen eintrifft oder aufgrund einer anderen action oder einer manuellen Operation der State gewechselt wird.

Wie alle listener actions wird **On Event** nur bei einem State wechsel innerhalb des selben Pfades beendet. Unter [Pfade](../../paths.md) findest du mehr über das Verhalten von Pfaden und ihre Auswirkungen auf listener actions wie **On Event** heraus.

Unter [Bedingungen (Conditions)](../../conditions.md) erfährst du, wie du variablen in der **On Event** und anderen actions abfragst oder vergleichst.

Settings
--------

### event

Der Name des Ereignisses auf das gewartet wird.

### then

Wähle aus, ob du bei Eintreten des Events immer in den nächsten State wechselst (*Goto Next State*) oder nur dann, wenn der Payload des Event eine [Bedingung](../../conditions.md) erfüllt (*Switch Event Message*).