Switch
======

**Plugin**: *Logic* | **Mode**: [Run](../../editor.md#run-actions)

Frage den Status einer Variable ab und wechsle in einen nächsten State wenn die abgefragte Bedingung zutrifft.

Die **Switch** Bedingung wird, anders als [On Event](./onEvent.md) und [On Change](./onChange.md) Bedingungen, nur einmal zu Beginn des States abgefragt.

Trifft eine Bedingung zu, wird der State sofort gewechselt und folgende actions, die unter der **Switch** action einsortiert sind, werden nicht ausgeführt.

![Eine switch und eine next action](./assets/switch_and_next.png)

Trifft die Bedingung, die in der **Switch** action angegeben ist zu, geht es weiter im "BE" State. Die folgenden actions werden nicht mehr ausgeführt.

Trifft die Bedingung nicht zu, wird die switch action übersprungen und die folgende next action führt in den "NOT_TO_BE" State.

**Switch** actions ermöglichen die selben Abfragen wie andere actions die Bedingungen enthalten.

Unter [Bedingungen (Conditions)](../../conditions.md) erfährst du, wie du variablen in der **Switch** und anderen actions abfragst oder vergleichst.