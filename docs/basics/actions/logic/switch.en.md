Switch
======

**Plugin**: *Logic* | **Mode**: [Run](../../editor.md#run-actions)

Query the status of a variable and switch to a next state if the queried condition applies.

Unlike [On Event](./onEvent.md)  and [On Change](./onChange.md) conditions, the **Switch** condition is only queried once at the beginning of the state.

If a condition applies, the state is changed immediately and the following actions, which are sorted under the **switch** action, are not executed.

![Eine switch und eine next action](./assets/switch_and_next.png)

If the condition specified in the switch action is met, it continues in the "BE" state. The following actions are no longer executed.

If the condition does not apply, the switch action is skipped and the following next action leads to the "NOT_TO_BE" state.

Switch actions allow the same queries as other actions containing conditions.

Under [Conditions](../../conditions.md) you will learn how to query or compare variables in the **Switch and other actions**.
