On Event
========

**Plugin**: *Logic* | **Mode**: [Listen](../../editor.md#listen-actions)

Waits for a certain event to occur and switches to a next state when the specified event occurs.

In addition, it is possible to query the content (payload) of an event for a condition and only switch to the next state if the condition applies.

For example, react to events triggered with [Dispatch Event](./dispatch.md).

[Devices](../../plugins/devices.md) also trigger events that have the same name as the device when the device sends a message to adaptor:ex.

**On Event** waits for events until one of the events and possibly the conditions occurs or the state is changed due to another action or a manual operation.

Like all listener actions, **On Event** is only terminated if there is a state change within the same path. See [Paths](../../paths.md) to learn more about path behavior and how it affects listener actions such as **On Event** .

See [Conditions](../../conditions.md) to learn how to query or compare variables in the On **Event and other actions.**

## Settings

### Event

The name of the event to wait for.

### Then

Choose whether you always switch to the next state when the event occurs ( _Goto Next State_ ) or only if the payload of the event meets a [condition](../../conditions.md) ( _Switch Event Message_ ).
