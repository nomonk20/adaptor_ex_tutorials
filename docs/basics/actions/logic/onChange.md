On Change
=========

**Plugin**: *Logic* | **Mode**: [Listen](../../editor.md#listen-actions)

Wartet darauf, dass eine Variable einen wert annimmt, bzw. eine Bedingung erfüllt.

Mit **On Change** ist es möglich jede Veränderung einer Variable zu beobachten und in einen nächsten State zu wechseln, wenn sie eine Bedingung erfüllt.

Sobald in den State gewechselt wird in dem sich die **On Change** action befindet, überprüft **On Change** alle angegebenen Bedingungen. Wenn eine Variable bereits die angegebene Bedingung bei wechsel in den State erfüllt, wird direkt in den angeschlossenen nächsten State gewechselt.

Wie in der [Switch](./switch.md) action geht es unter Umständen also direkt in den nächsten State. **On Change** überprüft die angegebenen Bedingungen jedoch anschließend solange weiter, wie der State aktiv ist.

Wie alle listener actions wird **On Change** nur bei einem State wechsel innerhalb des selben Pfades beendet. Unter [Pfade](../../paths.md) findest du mehr über das Verhalten von Pfaden und ihre Auswirkungen auf listener actions wie **On Change** heraus.

Unter [Bedingungen (Conditions)](../../conditions.md) erfährst du, wie du variablen in der **On Change** und anderen actions abfragst oder vergleichst.