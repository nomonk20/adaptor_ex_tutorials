Dispatch Event
==============

**Plugin**: *Logic* | **Mode**: [Run](../../editor.md#run-actions)

Löse ein Event aus, auf das an anderer Stelle mit [On Event](./onEvent.md) reagiert werden kann.

Settings
--------

### name

Der Name des Event. Über diesen Namen, kann auf dieses Event anderswo reagiert werden.
### range

Wähle "game", wenn du das Event in jeder Session jedes Level in deinem Game empfangen können willst.

Wähle "session", wenn du das Event nur innerhalb der Session dieses Levels in der es ausgelöst wird empfangen können willst.

### message

Hänge weitere Informationen an dein Event an.

Du kannst JS Object Notation verwenden um eine JS Object Message an das Event anzuhängen.