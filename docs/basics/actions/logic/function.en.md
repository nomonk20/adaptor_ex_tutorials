Function
========

**Plugin**: *Logic* | **Mode**: [Run](../../editor.md#run-actions)

Execute a javascript function.

In the [Javascript Function](../../functions.md) chapter you will find out how to create functions and add them to your game.

## Settings

### Name

The name of the function being executed. Choose the name from the available functions.

### Arguments

The argument or arguments passed as parameters to the function.

Choose `string` if you want to pass a text, a variable or a JS object.

Use square brackets (`[[` and `]]`) to assign the value to another variable.

Use curly brackets (`{` and `}`) to assign a JS object.

Choose _array_ if you want to pass multiple arguments in a list.

In the function, **arguments** are passed as the first parameter:

``` js
function test(arg, {session, game}) {
    session.log(arg) // Write arguments to the log console
}
```
## example

Execute the "cancelSessions" function and end all running sessions based on a certain level with the query `{level_name:"Tutorial"}`:

![Beispiel für die function action](./assets/functions_example.png)

"cancelSessions" is one of the embedded functions and uses the "game" context to terminate sessions based on a find query. She looks like this:

``` js
async function cancelSessions(args, {session, game}) {
  await game.deleteSession(args)
}
```
