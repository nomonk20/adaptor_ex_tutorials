Function
========

**Plugin**: *Logic* | **Mode**: [Run](../../editor.md#run-actions)

Führe eine Javascript Funktion aus.

Im Kapitel [Javascript Funktionen](../../functions.md) findest du heraus wie du Funktionen erstellst und zu deinem Game hinzufügst.

Settings
--------

### name

Der Name der Funktion die ausgeführt wird. Wähle den Namen aus den verfügbaren Funktionen aus.

### arguments

Das Argument oder die Argumente die der Funktion als Parameter übergeben werden.

Wähle `string` wenn du einen Text, eine Variable oder ein JS Objekt übergeben willst.

Nutze eckige Klammern (`[[` und `]]`) um den Wert einer anderen Variable zuzuweisen.

Nutze geschwungene Klammern (`{` und `}`) um ein JS Object zuzuweisen.

Wähle *array* wenn du mehrere Argumente in einer Liste übergeben willst.

In der Funktion werden **arguments** als erster Parameter übergeben:

``` js
function test(arg, {session, game}) {
    session.log(arg) // Schreibe arguments in die Log Konsole
}
```

Beispiel
--------

Führe die Funktion "cancelSessions" aus und beende alle laufenden sessions die auf einem bestimmten Level basieren mit der query `{level_name:"Tutorial"}`:

![Beispiel für die function action](./assets/functions_example.png)

"cancelSessions" ist eine der eingebetteten Funktionen und nutzt den "game" Kontext um sessions auf Basis einer find query zu beenden. Sie sieht folgendermaßen aus:

``` js
async function cancelSessions(args, {session, game}) {
  await game.deleteSession(args)
}
```