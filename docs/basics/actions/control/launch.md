Launch Session
==============

**Plugin**: *Control* | **Mode**: [Run](../../editor.md#run-actions)

Startet eine neue Level Session.

Settings
--------

### level

Das Level, dass der neuen Session zugrunde liegt. Du kannst den Level Namen angeben, oder eine Query formulieren um etwa ein Level mit einem Bestimmten Attribut auszuwählen.

`Chapter5` startet das Level mit dem Namen "Chapter5"

`{difficulty:5, area:'sonnenallee'}` startet ein Level dessen Attribut `difficulty` den Wert `5` und dessen Attribut `area` den Wert `sonnenallee` hat.

Du kannst in der query auch variblen verwenden. SO kannst du das obige Beispiel auch abhängig vom aktuellen Player Item formulieren:

`{difficulty:[[Player.skill]], area:[[Player.area]]}`

### arguments

Überge der neuen Session Level Argumente.
### name

Du kannst einen spezifischen Namen für die neue Session festelgen. Beachte, dass es keine 2 Sessions mit dem selben Namen geben kann.

Wenn du `name` nicht angibst wird ein automatischer Name generiert.

### reference

Gib einen Namen an, unter dem du die Session in diesem Level als Item referenzieren kannst.
