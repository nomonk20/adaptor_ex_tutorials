Log Message
===========

**Plugin**: *Control* | **Mode**: [Run](../../editor.md#run-actions)

Writes a text to the log console.

**Log Message** can be used to mark important points in your level or to check the contents of variables at a certain point in time.

![Screenshot die log action und die log konsole](./assets/log_live.png)

Here Log Message is used to check what value the variable `count` has when the "ChooseColor" state is triggered (repeatedly).

Settings
---------

### level

Specify the "log level" at which the log message is output.

The level indicates how important the message that is issued is.

The order of the log levels is:

1.  error (very important)
2.  warn
3.  info
4.  debug
5.  trace (unimportant)

Log messages with the "error" level are always visible in the console. Log messages with the "trace" level are only visible if "trace" is selected as the level in the console.

### message

The text that is displayed in the console when the **Log Message** action is triggered.
