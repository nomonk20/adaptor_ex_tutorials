On Cue
======

**Plugin**: *Control* | **Mode**: [Listen](../../editor.md#listen-actions)

Warte auf einen per Hand ausgelösten Wechsel in einen nächsten State.

**On Cue** hat keine technische Funktion, sondern erlaubt es Stellen im Level zu markieren in denen manuelle Eingaben benötigt sind.

Ein **cue** kann zum Beispiel ein Bestimmter Satz in einem Stücktext sein, den ein:e Schauspieler:in sagt und auf den etwa ein Licht oder Tonwechsel folgt.

Settings
--------

### Cue

Ein Text, der Beschreibt, zu welchem Zeitpunkt oder unter welchen Umständen der `next state` manuell ausgelöst werden soll.

Etwa: "Performer Amanda sagt zu Performer Hans Hövel: 'Es gibt keinen Code'"

Oder: "Falls der Arduino wieder einen Wackelkontakt hat: Auslösen sobald Spieler:innen das Rätsel gelöst haben"

### next state

Der State, der manuell ausgelöst werden soll, wenn der beschriebene `Cue` eintrifft.