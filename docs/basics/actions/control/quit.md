Quit
====

**Plugin**: *Control* | **Mode**: [Run](../../editor.md#run-actions)

Beende die laufende Session dieses Levels.

Sobald die **Quit** action ausgelöst wird, wird die laufende Session des Levels beendet und es können keine weiteren States oder Actions ausgeführt werden und alle aktiven Listener Actions in allen Pfaden werden geschlossen.

Actions die auf die **Quit** action folgen werden nicht mehr ausgeführt.