On Cue
======

**Plugin**: *Control* | **Mode**: [Listen](../../editor.md#listen-actions)

Waiting for a manually triggered transition to a next state.

**On Cue** has no technical function, but allows marking points in the level where manual input is required.

A **cue** can be, for example, a specific sentence in a piece of text that an actor says and is followed by a change of light or sound.

Settings
--------

### Cue

A text that describes when or under what circumstances the `next state` should be triggered manually.

For example: "Performer Amanda says to performer Hans Hövel: 'There is no code'"

Or: "If the Arduino has a loose contact again: trigger as soon as players have solved the puzzle"

### next state

The state that should be triggered manually when the one described `Cue` arrives.
