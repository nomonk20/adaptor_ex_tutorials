Launch Session
==============

**Plugin**: *Control* | **Mode**: [Run](../../editor.md#run-actions)

Starts a new level session.

Settings
--------

### level

The level underlying the new session. You can specify the level name, or formulate a query to select a level with a specific attribute.

`Chapter5` starts the level named "Chapter5"

`{difficulty:5, area:'sonnenallee'}` starts a level whose attribute has `difficulty` value `5`and whose attribute has `area`value `sonnenallee`.

You can also use variables in the query. THIS is how you can formulate the above example depending on the current player item:

`{difficulty:[[Player.skill]], area:[[Player.area]]}`

### arguments

Pass the new session level arguments.

### name

You can set a specific name for the new session. Note that there cannot be 2 sessions with the same name.

If you `name` do not specify, an automatic name will be generated.

### reference

Enter a name by which you can refer to the session in this level as an item.
