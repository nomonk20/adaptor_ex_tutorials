Cancel Session
==============

**Plugin**: *Control* | **Mode**: [Run](../../editor.md#run-actions)

Complete another running [session](../../live.md#session) of this or another level.

**Cancel Session** interrupts all running states and actions in all paths that are active in the session to be ended.

To end your current session, use the [Quit](./quit.md) action.

Settings
--------

### session

Specify the session to end.

You can 1. Use a reference to a session that you previously created with [Launch Session](./launch.md) or [Load Item](../data/load.md). Eg:

`[[mySession]]`

You can 2. address a session with "sessions" by name or database query:

`[[sessions.MySessionName]]`

or

`[[sessions.{'level_name':'SomeLevel'}]]`

Note: A database query that addresses multiple sessions will terminate all of those sessions.
