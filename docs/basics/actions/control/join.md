Join Path
=========

**Plugin**: *Control* | **Mode**: [Run](../../editor.md#run-actions)

Führe Pfade zusammen, sodass dieser und anschließende States nicht  parallel laufen können.

Settings
--------

### path

Der Name eines zuvor erstellten Pfades der in den aktuellen Pfad geführt werden soll.

Du kannst mehrere Pfade zusammenführen indem du mehrere Pfad Namen in `path` angibst.

Beachte: Um Pfade zusammenzuführen, die nicht (nur) die selbe [Split Path](./split.md) action als Ursprung haben, musst du die gesamte getrennte Pfad Historie des zu schließenden Pfad angeben.

Ergänze die `path` Liste um alle Pfadnamen, die seit der Trennung von diesem Pfad genutzt wurden.
