Cancel Session
==============

**Plugin**: *Control* | **Mode**: [Run](../../editor.md#run-actions)

Beende eine andere laufende [Session](../../live.md#session) dieses oder eines anderen Levels.

**Cancel Session** unterbricht alle laufenden States und Actions in allen Pfaden, die in der zu beendenden Session aktiv sind.

Um die eigene, aktuelle Session zu beenden benutze die [Quit](./quit.md) action.

Settings
--------

### session

Gib die session an, die beendet werden soll.

Du kannst 1. eine Referenz auf eine Session verwenden, die du zuvor mit [Launch Session](./launch.md) oder [Load Item](../data/load.md) erstellt hast. Z.B.:

`[[mySession]]`

Du kannst 2. eine session mit "sessions" per Name oder Datenbank Query adressieren:

`[[sessions.MySessionName]]`

oder

`[[sessions.{'level_name':'SomeLevel'}]]`

Beachte: Eine Datenbank Query, die mehrere Sessions adressiert, wird alle diese Session beenden.
