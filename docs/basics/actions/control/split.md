Split Path
==========

**Plugin**: *Control* | **Mode**: [Run](../../editor.md#run-actions)

Zweige neue Pfade ab um sie parallel verlaufen zu lassen.

Du kannst im selben State keine listener Actions oder actions einsetzen, die einen `next state` auslösen.

Settings
--------

**Split Path** ist eine Liste von neuen Pfaden für die du jeweils einen Namen und einen `next state` angeben musst.

### name

Der Name für den neu erstellten Pfad. Mit `name` kannst du den Pfad an anderer Stelle in der [Join Path](./join.md) action wieder zusammenführen.

Verwende für jeden neuen Pfad einen neuen ungenutzten Namen.

Verwende nicht "start" als Namen für neue Pfade, da mit dem Beginn des Levels im START State bereits der "start" Pfad verwendet wird.

### next state

Der erste State im neu erstellten Pfad. `next state` wird sofort ausgelöst, sobald alle vorangehenden run actions ausgeführt wurden.