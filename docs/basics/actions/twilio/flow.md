Twilio Studio Flow
==================

**Plugin**: [Twilio](../../plugins/twilio.md) | **Mode**: [Run](../../editor.md#run-actions)

Starte einen [Twilio Studio Flow](https://www.twilio.com/de/docs/studio) den du zuvor in Twilio erstellt hast.

Ggf. musst du das Twilio Plugin in `Game > settings` neu laden, wenn du einen neuen Studio Flow erstellt hast.

![Screenshot den reload button im plugin drücken](./assets/flow_plugin_reload.png)

Settings
--------

### Twilio Phone

Das Twilio Plugin phone item, dessen Telefonnummer im Twilio Studio Flow verwendet werden soll.

Im Studio Flow ist die Telefonnummer als `{{flow.channel.address}}` zugänglich und kann so in allen in Twilio Studio zur Verfügung stehenden Widgets verwendet werden.

### to

Item, dessen Telefonnumer im flow als Adressat übergeben werden soll.

Im Studio Flow ist die Telefonnummer als `{{contact.channel.address}}` zugänglich.

### flow

Der Studio Flow, der gestartet werden soll.

### parameters

*optional*

Zusätzliche variablen, die dem Flow beim Start übergeben werden sollen.

Die Parameter sind im flow unter `{{flow.data}}` zugänglich.

Wähle **parameters** unter `Settings` an, wenn du sie verwenden willst.

Füge Parameter anschließend über `Settings` in `parameters` hinzu.

![Screenshot Auf Settings klicken, dann Add](./assets/flow_parameter.png)

Der Parameter "myParamter" kann im Twilio Studio Flow als `{{flow.data.myParameter}` verwendet werden.

Beispiel
--------

Ziehe eine neue **Twilio Studio Flow** action aus den Twilio actions auf die STAGE.

Wähle den Studio Flow, den du starten willst aus und gib ihn in der action als `flow` an

![Screenshot studio flows im Twilio dashboard](./assets/flow_dashboard.png)

![Screenshot den flow aus der liste auswählen](./assets/flow_example.png)

Wenn der *Evaluation* State ausgelöst wird, startet adaptor:ex den Twilio Studio Flow "Veronica_Registration" mit der Veronica Twilio Telefonnummer als `{{flow.channel.address}}` und der telnumber Eigenschaft von Player als `{{contact.channel.address}}`.

Studio Flows die von adaptor:ex gestartet werden beginnen mit dem `REST API` trigger

![Screenshot Twilio Studio Flow Beispiel mit trigge rund call](./assets/flow_trigger.png)

In diesem Studio Flow Beispiel wird also zunächst ein Anruf an die telnumber von Player abgesetzt.