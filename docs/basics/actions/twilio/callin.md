Incoming Call
=============

**Plugin**: [Twilio](../../plugins/twilio.md) | **Mode**: [Listen](../../editor.md#listen-actions)

Warte und reagiere auf einen eingehenden Anruf bei einem deiner Twilio phone Items.

Reagiere auf eine von drei Arten auf den eingehenden Anruf:

1. Say Text and Play Audiofiles

Nutze die Twilio Sprachgenerierung um Texte vorlesen zu lassen (`Say Text`) oder spiele eigene Audiofiles ab (`Play Audio`).

2. Forward to Twilio Studio Flow

Starte einen zuvor in Twilio erstellten [Studio Flow](https://www.twilio.com/de/docs/studio) und leite die Anrufdaten in den Studio Flow weiter.

3. Reject Call

Weise den Anruf ab oder zeige an, dass die Leitung besetzt ist.

Settings
--------

### Twilio Phone

Das Twilio Plugin phone Item, das auf den Anruf reagiert.

### From

Data Item mit `telnumber` variable, auf dessen Anruf gewartet und reagiert wird.

### priority

Lege fest, ob diese **Incoming Call** action vor- oder nachrangig behandelt wird, wenn mehrere **Incoming Call**s mit gleichen Teilnehmern (selbe Items in `Twilio Phone` und `From`) auf einen Anruf warten.

Dies kann z.B. wichtig sein, wenn **Incoming Call** in getrennten [Pfaden](../../paths.md) oder [Sessions](../../live.md#session) verwendet wird.

Warten mehrere **Incoming Call** actions, mit gleichen Teilnehmern auf einen Anruf, wird die Reaktion des **Incoming Call** listener mit höherer `priority` verwendet.

Warten mehrere **Incoming Call** actions, mit gleichen Teilnehmern und gleicher `priority` auf einen Anruf, wird die Reaktion des zuletzt ausgelösten **Incoming Call** listener verwendet.

### next state

*optional*

Wechsel in den `next state` wenn die Reaktion auf den Anruf beendet ist und eine `Say Text and Play Audiofiles` nicht unterbrochen wurde.
### SayPlay

*Option: Say Text and Play Audiofiles*

Eine Reihe von Text to Speech ausgaben oder Audiofiles die innerhalb des Anrufs abgespielt werden.

Füge Audiofiles, die du hier verwenden willst zu deinem `files` Ordner deines Games hinzu. Schau dir dazu den Glossar Eintrag [Files](../../files.md) an.

Text to Speech Texte, die in `say` angegeben werden, werden von der Stimme und in der Sprache interpretiert, die in den Settings des Twilio phone Item angegeben sind.

Du kannst die Stimme und Sprache aber auch innerhalb der **Incoming Call** Action anpassen.

Wähle über die `Settings` innerhalb des `sayplay` eine alternative Sprache oder Stimme aus.

![Screenshot eine alternative Stimme und Sprache auswählen](./assets/call_voice.png)

### failed

*Option: Say Text and Play Audiofiles*

Wähle einen `next state` aus, in den gewechselt wird, sollte der Anruf vorzeitig unterbrochen werden.

### flow

*Option: Forward to Twilio Studio Flow*

Wähle den [Twilio Studio Flow](https://www.twilio.com/de/docs/studio) aus, der mit dem eingehenden Anruf gestartet werden soll.

Die Anrufdaten, Telefonnummer des Phone Item und Data Item, werden an den Flow übergeben.

Ein weitergegebener Flow wird in Twilio Studio als "Incoming Call" behandelt.

Ist der Anruf im Studio Flow beendet oder unterbrochen, wird, wenn angegeben, der `next state` ausgelöst.

### reject

*Option: Reject Call*

Gib an, auf welche Art der Anruf abgewiesen werden soll.

**reject** entspricht dem Abweisen des Telefonats durch Auflegen.

**busy** entspricht dem Abweisen des Telefonats durch eine besetzte Leitung.

Beispiel
--------

Ziehe eine neue **Incoming Call** action aus der TOOLBAR auf die STAGE.

Wähle die Option *Say Text and Play Audiofiles* aus.

![Sreenshot im obersten Dropdown die erste Option auswählen](./assets/callin_select.png)

Wähle das `Twilio Phone` aus, dass auf Anrufe des Data Item unter `from` reagieren soll.

Wähle in `SayPlay` ein Audiofile aus deinen [Files](../../files.md) aus.

Füge eine weitere `SayPlay` Aktion mit `Add sayplay` hinzu.

Wähle für diese `SayPlay` Aktion `Say Text`.

![Screenshot eine Incoming Call Action mit Audiofile und text to speech](./assets/callin_example.png)

Wird diese Action ausgeführt, wartet "Veronica" darauf, dass sie von der Telefonnummer von "Player" angerufen wird.

Sobald "Player" anruft, wird der Anruf von Veronica angenommen. Direkt danach wird die Audiodatei "jingle.mp3" abgespielt. Anschließend wird der Text in `say` vom Twilio voice Service vorgelesen. `[[Player.name]]` wird zuvor durch die Variable "name" in "Player" ersetzt.

Ist der Text zu Ende vorgelesen, wird der Anruf beendet und der `next state` *NewJob* wird ausgelöst.

Sollte der Anruf zuvor unterbrochen werden wird der `next state` *Retry* ausgelöst.