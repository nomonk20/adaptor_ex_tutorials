Set Track Mix
=============

**Plugin**: [Ableton Live](../../plugins/ableton.md) | **Mode**: [Run](../../editor.md#run-actions)

Ändere die Mixer-Einstellungen eines Liveset-Tracks.

Settings
--------

Wähle die Einstellungen, die du im Mix ändern willst, über das `Settings` Menü im action editor aus.

![Track Mix Einstellungen hinzufügen](./assets/ableton_track_mix_settings.png)

![Die Track Mix Elemente in ableton live](./assets/ableton_track_mix.png)

### liveset

Das Liveset, an das die Set Track Mix message gesendet wird.

### track

Der Track in `liveset`, dessen Mix geändert werden soll.

### panning 

Ändere den Track im Stereo Mix. Ein Wert zwischen `-1` und `1`. `-1` für ganz links, 1 für ganz rechts, `0` für mittig.

### volume

Ändere die Lautstärke des Tracks im gesamten Mix. Ein Wert zwischen `0` und `1`.

### track_activator

Aktiviere oder deaktiviere den Track. `1` aktiviert den Track, `0` deaktiviert den Track.

### Sends

Ändere den Anteil des jeweiligen *sends*-Kanals. 

Füge weitere sends über `Settings` hinzu. 

![Weitere sends mit ADD hinzufügen](./assets/ableton_add_sends.png)

Gib als `Property Name` den send-Kanal in Großbuchstaben an (z.B. `C`) und füge ihn mit `Add` hinzu.
