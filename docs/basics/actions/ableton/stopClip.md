Stop Clip
=========

**Plugin**: [Ableton Live](../../plugins/ableton.md) | **Mode**: [Run](../../editor.md#run-actions)

Stoppe einen Clip im Liveset.

![Ein Clip im Liveset](./assets/ableton_clip_stop.png)