Play Scene
==========

**Plugin**: [Ableton Live](../../plugins/ableton.md) | **Mode**: [Run](../../editor.md#run-actions)

Starte alle Clips in einer Scene im Liveset.

![Scenes im ableton Liveset](./assets/ableton_scene_start.png)

Scenes können erst dann ausgewählt werden, wenn du ihnen einen eindeutigen Namen gibst:

![Rechts klick auf die Scene um sie umzubenennen](./assets/ableton_change_scene_name.png)
