Timeout
=======

**Plugin**: *Time* | **Mode**: [Listen](../../editor.md#listen-actions)

Activate the next state after a specified time has elapsed.

The `timeout` property determines how long it takes to go to the` next state`.

![Beispiel Timeout](./assets/timeout_example.png)
## time formatting

Times can be specified in seconds or - separated by `:`- in hours, minutes and seconds (hh:mm:ss).

Fractions of seconds can be specified using dot notation as the last digit.

### examples

`5` - triggers the `next state` in 5 seconds

`125` - 2 minutes and 5 seconds

`2:05` - also 2 minutes and 5 seconds (identical to `02:05`)

`01:30:00`- 1 hour and 30 minutes

`48:00:01`- 2 days and 1 second

`0.5`- 500 milliseconds

`3:33.33`- 3 minutes, 33 seconds and 330 milliseconds
