Schedule
========

**Plugin**: *Time* | **Mode**: [Listen](../../editor.md#listen-actions)

Löst den nächsten State an einem kommenden Wochentag, einem Tag des Monats, zu einer Minute der aktuellen Stunde oder beim nächsten Eintreten einer Uhrzeit aus.

Die Zeitplanung bezieht sich stets auf den nächstmöglichen Zeitpunkt ausgehend vom Zeitpunkt zu dem der State, der die *Schedule* action enthält, ausgelöst wurde.

Settings
--------

*Schedule* besitzt 4 Grundkonfigurationen die verschiedene Formen der Zeitangabe ermöglichen.

![Screenshot: Die Schedule Auswahlmöglichkeiten](./assets/schedule_settings_options.png)

`Clock Time` - Löst den `next state` beim nächsten Eintreffen einer Uhrzeit aus.

`Minute of Hour` - Löst den `next state` zu einer bestimmten Minute in der Stunde aus.

`Day of the Week` - Löst den `next state` an einem kommenden Wochentag zu einer bestimmten Uhrzeit aus.

`Day of the Month` - Löst den `next state` an einem kommenden Tag des Monats zu einer bestimmten Uhrzeit aus.

### Time

*Verfügbar in: `Clock Time` | `Day of the Week` | `Day of the Month`*

Die Uhrzeit am geplanten Tag, an dem der `next state` ausgelöst wird. Nutze `Pick Time` um eine Uhrzeit auszuwählen oder `Custom` um eine manuelle Angabe zu machen.

*Time* muss im folgenden Format angegeben werden: `HH:MM:SS`

Um etwa **Time** auf 16:30 Uhr und 30 Sekunden zu setzen gib folgendes an:

`16:30:30`

Du kannst Minuten und/oder Sekunden weglassen um die volle Stunde oder ganze Minute anzugeben.

### Minute of Hour

*Verfügbar in: `Minute of Hour`*

Die Minute der aktuellen oder kommenden Stunde zu der der `next state` ausgelöst wird.

Setze **Minute of Hour** etwa auf `0` um `next state` zur nächsten vollen Stunde auszulösen oder auf `30` um `next state` zur nächsten halben Stunde der Uhr auszulösen.

### Day of Week

*Verfügbar in: `Day of the Week`*

Ein Wochentag an dem der `next state` ausgelöst werden soll.

Nutze `Select` um den Wochentag auszuwählen oder `Custom` um den Wochentag manuell anzugeben. 

Der Tag kann als ganzer englischer Begriff (`Sunday`, `Monday` ...) , abgekürzt (`Su`, `Mo` ...) oder numerisch, beginnend mit `0` für Sonntag, angegeben werden.

Eingeplant wird das nächstmögliche Eintreffen der angegebenen Uhrzeit ([Time](#time)) am entsprechenden Wochentag. 

Wird der State, der die *Schedule* action enthält, am selben Wochentag zu einer späteren Uhrzeit ausgelöst, wird der kommende Wochentag als nächstmöglicher Zeitpunkt geplant.

Mit [Occurence](#occurrence-day-of-week) und/oder [Month](#month) kann ein später in der Zukunft liegender Wochentag geplant werden bzw. lässt sich so festlegen in welcher Woche oder welchem Monat sich der angegebene Wochentag befindet.

### Occurrence Day of Week

*optional*

*Verfügbar in: `Day of the Week`*

Das auftreten des angegebenen [Day of Week](#day-of-week) innerhalb des Monats.

Eine Zahl zwischen `1` und `6` die angibt, das wievielte erscheinen des entsprechenden Wochentages im aktuellen oder kommenden Monat geplant werden soll.

Um `next state` etwa am 2. Mittwoch im aktuellen oder kommenden Monat auszulösen gib `2` für **Occurrence Day of Week** an.

Ist **Occurrence Day of Week** größer als die Anzahl des Auftretens des angegebenen Wochentags, wird das letzte Eintreten des angegebenen Wochentags im aktuellen oder kommenden Monat geplant.

Gib eine negative Zahl oder `0` (`-6` bis `0`) an um vom letzten Auftreten des Wochentages innerhalb des Monats auszugehen. `0` entspricht dabei dem letzten Auftreten des Wochentags innerhalb des Monats.

### Day of Month

*Verfügbar in: `Day of the Month`*

Der Tag des Monats an dem `next state` ausgelöst werden soll.

Gib eine Zahl zwischen 1 und 31 an um das nächste Eintreffen eines Tags des Monats zu planen.

Eingeplant wird das nächstmögliche Eintreffen der angegebenen Uhrzeit ([Time](#time)) am nächstmöglichen Eintreffen des Monatstages. 

Wird der State, der die *Schedule* action enthält, am selben Tag des Monats zu einer späteren Uhrzeit ausgelöst, wird der entsprechende Tag im nächsten Monat als nächstmöglicher Zeitpunkt geplant.

Mit [Month](#month) kann der Monat im Jahr, auf den sich **Day of Month** bezieht optional festgelegt werden.

Ist **Day of Month** größer als die Anzahl Tage, die sich im aktuellen bzw. kommenden oder im festgelegten Monat befinden, wird der letzte Tag des Monats geplant.

Gib eine negative Zahl oder `0` (`-31` bis `0`) an, um vom Ende des Monats auszugehen. `0` entspricht dabei dem letzten Tag des Monats.

### Month

*optional*

*Verfügbar in: `Day of the Month` | `Day of the Week`*

Der Monat des aktuellen oder kommenden Jahres für den das Auslösen des `next state` geplant wird.

Nutze `Select` um den Monat auszuwählen oder `Custom` um den Monat manuell anzugeben.

Der Monat kann als ganzer englischer Begriff (`January`, `February` ...) , abgekürzt (`Jan`, `Feb` ...) oder numerisch, beginnend mit `1` für Januar, angegeben werden.

### next state

*Verfügbar in: `Clock Time` | `Minute of Hour` | `Day of the Week` | `Day of the Month`*

Der State, der ausgelöst wird, wenn der geplante Zeitpunkt eintrifft.

Beispiel
--------

Erstelle einen neuen State indem du die *Schedule* (ACTIONS > TIME) action auf die STAGE ziehst.

Benenne den State in `WaitForWeekend` um.

Wähle im oberen Auswahlmenü `Day of the Week` aus.

Wähle unter **Day of Week** den Wochentag aus auf den gewartet werden soll und klicke auf das Uhrsymbol unter `Time` um den Zeitpunkt am gewählten Wochentag auszuwählen, auf den gewartet werden soll.

Gib `Weekend` als `next state` an.

![Screenshot: Einen Wochentag Planen](./assets/schedule_example.png)

Erstelle einen weiteren State mit einer [Log Message](../control/log.md) action und einer weiteren *Schedule* action. Wähle hier einen anderen Wochentag aus und gib `WaitforWeekend` als `next state` an.

![Screenshot: Einen Wochenrhythmus erstellen](./assets/schedule_example2.png)

Wird der *WaitForWeekend* State ausgelöst, wird am nächsten Freitag um 17:00 in den *Weekend* State gewechselt.

Die *Schedule* action im *Weekend* State wird nun warten bis der nächste Montag eintritt und um 9:00 Uhr wiederum in den *WaitForWeekend* State wechseln.

![Screenshot: Der Wochenrhythmus](./assets/schedule_example3.png)