Date and Time
==============

**Plugin**: *Time* | **Mode**: [Listen](../../editor.md#listen-actions)

Triggers the next state on a specific day and time.

_Date and Time_ can also be used to trigger the next state at a specific time on the [current day](#current-day).

Settings
--------

### Date and Time

Enter the date and time when the `next state` is triggered.

Select `Pick Date` to set the date and time via a calendar widget.

#### Manual input

With `Custom Date` you can specify the date and time manually in [ISO 8601 time](https://en.wikipedia.org/wiki/ISO_8601) format. Of course, you also have the option of inserting a variable here.

An ISO 8601 time specification has the following format: `YYYY-MM-DDTHH:MM:SS`.

Year, month and day must be separated by a hyphen `-`. The time is separated by spaces or `T` from the date. Hour, minute and second are separated by a colon `:`.

For example, you would specify 06/28/2023 at 10:13 as follows:

`2023-06-28T10:13`

If you don't specify a time, it will `next state` trigger at 00:00 of the specified day.

If the specified time is in the past, it is `next state` triggered immediately.

#### Current-day

If you don't specify a date, the time refers to the day on which the state containing the _Date and Time_ action is triggered.

For `next state` example, to trigger the on the current day at 12:33 p.m., enter the following:

`12:33`

If the time on this day is in the past, it is `next state` triggered immediately.

>Instead it uses [Schedule](./schedule.md) Time action to trigger the `next state` when the time event occurs. So on this or the next day, depending on whether the time on the current day is in the future or in the past.

### next state

The State that is raised when the specified date and time occurs.

## example

Create a new state by dragging the _Date and Time_ action (ACTIONS > TIME) into the STAGE.

Select `Pick Date` and click the calendar icon below `Date and Time`. Now select the time to wait for.

![Screenshot: Einen Zeitpunkt in der date and time action auswählen](./assets/datetime_example.png)

Under `next state`, enter the follow state.

When _AwaitShow is_ triggered, the _Date and Time_ listener waits until 10/26/2022 at 8:00 p.m. and then triggers the _ShowTime_ State.
