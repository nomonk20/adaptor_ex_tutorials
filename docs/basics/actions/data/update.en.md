Update Query
============

**Plugin**: *Data* | **Mode**: [Run](../../editor.md#run-actions)

Performs a database update query to change items.

_Update Query_ allows you to perform change operations on items that cannot be done with simple _data_ actions.

Learn more about [Items](../../variables.md#collections-und-data-items).

## Settings

### Item

The data or plugin item to be modified.

**item** can be a level argument like `Player` or some other reference to an item.

**item** must be an item and cannot be a variable within an item. Variables are addressed in the **update query .**

### Update query

A query object in the form of a [MongoDB update query](https://docs.mongodb.com/manual/tutorial/update-documents/)

**update query** uses JSON syntax to specify what to change. The outermost _key_ is the [update operator](https://docs.mongodb.com/manual/reference/operator/update/).

To change variables in the item use i.e. the _$set_ operator:

`$set: {favorite_color:"purple"}`

To multiply a number value use the _$mul_ operator

`$mul: {score:5}`

> ATTENTION: If no _update operator_ is used and NeDB is used as the database for the adaptor:ex server (default), the entire item is replaced with the JS object specified in the **update query .**

You can use variables within the update query.

`$mul: {score:[[bonus]]}` Multiplies the `score` item's variable by the value in the local `bonus` variable.

## Example

Use the _$max_ update operator and set the `score` from `Player` to 20 if it's above 20.

![beispiel für eine update query](./assets/update_example.png)
