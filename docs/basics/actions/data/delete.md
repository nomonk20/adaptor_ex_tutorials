Delete Item or variable
=======================

**Plugin**: *Data* | **Mode**: [Run](../../editor.md#run-actions)

Entferne ein Item oder eine variable.

Settings
--------

### variable

Gib hier die Variable oder das Item an, das du entfernen willst. Du kannst eine lokale Variable, ein Level Attribute oder Argument, eine Variable in einem Item oder ein Item angeben.

Du kannst die Variable oder das Item per Hand formulieren oder aus der VARIABLES TOOLBAR herüberziehen.

Die Variable oder das Item kann mit oder ohne Umfassende eckige Klammern `[[` und `]]` geschrieben werden.

`Player.inventory` ist hier das selbe wie `[[Player.inventory]]`

Beispiel
--------

Entferne das gesamte Data Item, das durch das Level Argument `Player` referenziert wird

![Ein Item löschen](./assets/delete_item_example.png)

Entferne die Variable "inventory" im Level Argument `Player`

![Eine Variable entfernen](./assets/delete_variable_example.png)