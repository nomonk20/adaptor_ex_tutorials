Load Item
==========

**Plugin**: *Data* | **Mode**: [Run](../../editor.md#run-actions)

Erstellt eine neue oder überschreibt eine existierende Referenz auf ein Item.

Das Item kann anschließend mit dem neuen Referenz Namen innerhalb des Levels verwendet werden wie ein Level Argument.

Mehr über [Item Referenzen](../../variables.md#item-container-und-level-arguments) erfahren.

Settings
--------

### name

Der name der Referenz, unter der das Item innerhalb des Levels anschließend adressierbar ist.

**name** darf keine Leerzeichen enthalten.
### Collection

Die Collection, in der das zu referenzierende Item enthalten ist.
### find query

Formuliere eine Datenbank Suche im Stile einer [MongoDB find query](https://docs.mongodb.com/manual/reference/method/db.collection.find/) um das Item zu laden.

**find query** verwendet JSON syntax um festzustellen welches Item geladen werden soll.

Finde das Item mit dem Namen "Hans Hövel":

`{name: "Hans Hövel"}`

Finde ein Item, dessen `score` Variable größer ist als 30:

`{score: {$gt:30}}`

Finde ein Item, dessen `score` Variable größer ist als der Wert in der lokalen Variable `minimum`:

`{score: {$gt:[[minimum]]}}`

> Die äußeren geschweiften Klammern in **find query** kannst du weglassen, wenn du möchtest.

Beispiel
--------

Lade ein Item aus der "location" Collection in die lokale Item Referenz "Location". Suchkriterium für die Referenz ist, dass die `name` Variable "laboratory" ist.

![Ein Item als Referenz ins Level laden](./assets/load_example.png)

Anschließend kannst du das Item z.B. in der [Switch](../logic/switch.md) action verwenden.

![Die Referenz in der switch action verwenden](./assets/load_switch_example.png)