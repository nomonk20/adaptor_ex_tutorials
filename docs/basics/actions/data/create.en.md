Create Item
============

**Plugin**: *Data* | **Mode**: [Run](../../editor.md#run-actions)

Creates a new data item in one of your collections.

Learn more about [Items](../../variables.md#collections-und-data-items).

## Settings

### Collection

Select the collection in which the new data item should be created.

Under `Game-> settings-> COLLECTIONS` you can view your collections and create new collections.

### variables

Add variables to the item that it has from the start.

Every item you create needs a `name` variable from the start.

To add more variables use the `variables-> Settings` button. Enter a new variable in the input field and press `Add`.

![Eine Variable zu einem neuen Item hinzufügen](./assets/create_add_variable.png)

You can choose between the data types _string_,  _number_, _boolean_ and _array_ for each additional variable.

_string_ - A text or other variable

Use square brackets (`[[` and `]]`) to assign the value to another variable.

Use curly brackets (`{`and `}`) to assign a JS object.

_number_ - A number value. Use `.` to specify a floating point number.

_boolean_ - 1 of 0, true or false.

_array_ - A list of values

### reference

A name that refers to the data item within the level. As with a level argument, you can address the item with this name in subsequent actions.

## example

Create a new data item in the `Collection` "locations" with the initial values `name` "laboratory" and `condition` "locked" that will be `reference` available in the level as "Lab".

![beispiel ein neues Item erstellen](./assets/create_example.png)

You can then use the item in the [Switch](../logic/switch.md) action, for example, and check whether the `condition` variable is "locked" or "open".

![Das Item in der switch action verwenden](./assets/create_switch_example.png)
