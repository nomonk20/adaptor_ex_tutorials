Remove from list
================

**Plugin**: *Data* | **Mode**: [Run](../../editor.md#run-actions)

Removes one or more entries from a list (array).

## Settings

### Remove values

Enter the values to be removed from the list here.

The value in the list must exactly match the value specified here. The data type must also be identical.

Add more values with `Add Value`.

Choose to specify `string` text or another variable and `number` to specify a number.

Use square brackets (`[[` and `]]`) to assign the value to another variable.

### From list

The array variable from which to remove entries.

You can specify a local variable, a level attribute or argument, or a variable in an item.

You can formulate the variable manually or drag it over from the VARIABLES TOOLBAR.

The variable can be written with or without surrounding square brackets `[[` and `]]`.

`Player.friends` is the same as here `[[Player.friends]]`.

The variable must be an _array_ .

## example

Remove the entry "Monica McLain" from the `friends` list in the level argument `Player`.

![Beispiel einen Eintrag aus einer Liste zu entfernen](./assets/pull_example.png)
