Load Item
==========

**Plugin**: *Data* | **Mode**: [Run](../../editor.md#run-actions)

Creates a new or overwrites an existing reference to an item.

The item can then be used with the new reference name within the level like a level argument.

Learn more about [Item References](../../variables.md#item-container-und-level-arguments).

## Settings

### Name

The name of the reference by which the item can then be addressed within the level.

**name** cannot contain spaces.

### Collection

The collection that contains the item to be referenced.

### find query

Formulate a database search in the style of a [MongoDB find query](https://docs.mongodb.com/manual/reference/method/db.collection.find/) to load the item.

**find query** uses JSON syntax to determine which item to load.

Find the item named "Hans Hövel":

`{name: "Hans Hövel"}`

Find an item whose `score` variable is greater than 30:

`{score: {$gt:30}}`

Find an item whose `score` variable is greater than the value in the local variable `minimum`:

`{score: {$gt:[[minimum]]}}`

> You can omit the outer curly brackets in **find query** if you want.

## example

Load an item from the "location" collection into the local item reference "Location". Search criterion for the reference is that the `name` variable is "laboratory".

![Ein Item als Referenz ins Level laden](./assets/load_example.png)

You can then use the item in the [Switch](../logic/switch.md) action, for example.

![Die Referenz in der switch action verwenden](./assets/load_switch_example.png)
