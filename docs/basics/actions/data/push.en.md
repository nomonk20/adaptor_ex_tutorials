Add to list
===========

**Plugin**: *Data* | **Mode**: [Run](../../editor.md#run-actions)

Adds one or more items to a list (array).

If the variable doesn't already exist, it will be recreated as an _array_ variable.

## Settings

### Add values

Enter the values to be added to the list here.

Add more values with `Add Value`.

Choose to specify `string` text or another variable and `number` to specify a number.

Use square brackets (`[[` and `]]`) to assign the value to another variable.

Use curly brackets (`{`and `}`) to assign a JS object.

### To list

The array variable to add items to or to create with items specified in `add values`.

You can specify a local variable, a level attribute or argument, or a variable in an item.

You can formulate the variable manually or drag it over from the VARIABLES TOOLBAR.

The variable can be written with or without surrounding square brackets `[[` and `]]`.

`Player.friends` is the same as here `[[Player.friends]]`

The variable must be an _array_ if it already exists.

### Allow duplicates

By default, duplicate entries are overwritten and not added to the list as individual entries.

Activate `allow duplicates` to also add the same entries to the list.

## Example

Add "Hans Hövel" to the `friends` list in the level argument `Player`

![Einen Wert zu einer Liste hinzufügen](./assets/push_example.png)
