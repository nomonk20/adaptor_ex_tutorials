Set Variable
============

**Plugin**: *Data* | **Mode**: [Run](../../editor.md#run-actions)

Verändert den Wert einer Variable. 

Wenn die Variable nicht existiert, wird sie neu erstellt.

Mehr über [Variablen](../../variables.md) erfahren.

Settings
--------

### set variable

Gib hier die Variable an, die du ändern willst. Du kannst eine lokale Variable, ein Level Attribute oder Argument oder eine Variable in einem Item angeben.

Du kannst die Variable per Hand formulieren oder aus der VARIABLES TOOLBAR herüberziehen.

Die Variable kann mit oder ohne Umfassende eckige Klammern `[[` und `]]` geschrieben werden.

`Player.favorite_color` ist hier das selbe wie `[[Player.favorite_color]]`

Du kannst kein Item angeben ohne eine Variable in dem Item festzulegen.

Das Level Argument `Player` kann z.B. nicht mit einem einfachen `string` oder `number` Wert überschrieben werden.

### to value

Der Wert, in den die Variable geändert werden soll.

Wähle `string` um einen Text oder eine andere Variable und `number` um eine Zahl anzugeben.

Nutze eckige Klammern (`[[` und `]]`) um den Wert einer anderen Variable zuzuweisen.

Nutze geschwungene Klammern (`{` und `}`) um ein JS Object zuzuweisen.

Beispiel
--------

Ändere die variable `favorite_color` im Level Argument `Player` auf `purple`

![Eine Variable ändern mit set](./assets/set_example.png)