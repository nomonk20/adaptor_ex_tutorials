Update Query
============

**Plugin**: *Data* | **Mode**: [Run](../../editor.md#run-actions)

Führt eine Datenbank update Query durch um Items zu verändern.

*Update Query* erlaubt es Änderungsoperationen an Item vorzunehmen, die mit den einfachen *Data* actions nicht zu bewerkstelligen sind.

Mehr über [Items](../../variables.md#collections-und-data-items) erfahren.

Settings
---------

### item

Das Data oder Plugin Item, das geändert werden soll.

**item** kann ein Level Argument wie `Player` oder eine andere Referenz auf ein Item sein.

**item** muss ein Item und kann nicht eine Variable in einem Item sein. Variablen werden in der **update query** adressiert.

### update query

Ein query Objekt in Form einer [MongoDB update query](https://docs.mongodb.com/manual/tutorial/update-documents/)

**update query** verwendet JSON syntax um festzulegen was geändert werden soll. Äußerster *key* ist der [update operator](https://docs.mongodb.com/manual/reference/operator/update/).

Um Variablen im Item zu ändern nutze z.B. den *$set* operator:

`$set: {favorite_color:"purple"}`

Um einen Nummern wert zu multiplizieren nutze den *$mul* operator

`$mul: {score:5}`

> ACHTUNG: Wird kein *update operator* verwendet und NeDB wird als Datenbank für den adaptor:ex server eingesetzt (default), wird das gesamte Item mit dem in der **update query** angegebenen JS Object ersetzt.

Du kannst innerhalb der Update query variablen verwenden.

`$mul: {score:[[bonus]]}` Multipliziert die `score` variable des Item mit dem Wert, der in der lokalen `bonus` Variable steht.

Beispiel
---------

Nutze den *$max* update operator und setze den `score` von `Player` auf 20, wenn er über 20 liegt

![beispiel für eine update query](./assets/update_example.png)
