Add to list
===========

**Plugin**: *Data* | **Mode**: [Run](../../editor.md#run-actions)

Fügt einen oder mehrere Einträge zu einer Liste (einem Array) hinzu.

Wenn die Variable noch nicht existiert, wird sie als *array* Variable neu erstellt.

Settings
--------

### add values

Gib hier die Werte an die der Liste hinzugefügt werden sollen.

Füge weitere Werte mit `Add Value` hinzu.

Wähle `string` um einen Text oder eine andere Variable und `number` um eine Zahl anzugeben.

Nutze eckige Klammern (`[[` und `]]`) um den Wert einer anderen Variable zuzuweisen.

Nutze geschwungene Klammern (`{` und `}`) um ein JS Object zuzuweisen.

### to list

Die Array Variable, der die Einträge hinzugefügt werden oder die mit den in `add values` angegebenen Einträgen erstellt werden soll.

Du kannst eine lokale Variable, ein Level Attribute oder Argument oder eine Variable in einem Item angeben.

Du kannst die Variable per Hand formulieren oder aus der VARIABLES TOOLBAR herüberziehen.

Die Variable kann mit oder ohne Umfassende eckige Klammern `[[` und `]]` geschrieben werden.

`Player.friends` ist hier das selbe wie `[[Player.friends]]`

Es muss sich bei der Variable, wenn sie bereits existiert, um ein *array* Handeln.

### allow duplicates

Standardmäßig werden doppelte Einträge überschrieben und nicht als einzelne Einträge der Liste hinzugefügt.

Aktiviere `allow duplicates` um auch gleiche Einträge der Liste hinzuzufügen.

Beispiel
--------

Füge "Hans Hövel" zu der Liste `friends` im Level Argument `Player` hinzu

![Einen Wert zu einer Liste hinzufügen](./assets/push_example.png)