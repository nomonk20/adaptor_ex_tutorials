Delete Item or variable
=======================

**Plugin**: *Data* | **Mode**: [Run](../../editor.md#run-actions)

Remove an item or variable.

## Settings

### Variable

Enter the variable or item that you want to remove here. You can specify a local variable, a level attribute or argument, a variable in an item, or an item.

You can formulate the variable or item manually or drag it over from the VARIABLES TOOLBAR.

The variable or item can be written with or without surrounding square brackets `[[` and `]]`.

`Player.inventory` is the same as here `[[Player.inventory]]`

## Example

Remove the entire data item referenced by the level argument `Player`.

![Ein Item löschen](./assets/delete_item_example.png)

Remove the "inventory" variable in the level argument `Player`

![Eine Variable entfernen](./assets/delete_variable_example.png)
