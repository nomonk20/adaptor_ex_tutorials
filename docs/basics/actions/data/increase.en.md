Increase number
===============

**Plugin**: *Data* | **Mode**: [Run](../../editor.md#run-actions)

Increases or decreases a number variable by a specified value.

## Settings

### Increase variable

Enter the variable you want to increase here. You can specify a local variable, a level attribute or argument, or a variable in an item.

You can formulate the variable manually or drag it over from the VARIABLES TOOLBAR.

The variable can be written with or without surrounding square brackets `[[` and `]]`.

`Player.score` is the same as here `[[Player.score]]`

The variable, if it already exists, must be a _number_ or _integer_ value.

### by value

The value by which the variable will be incremented.

Prepend a `-` to decrease the value instead.

Increase the variable by 6:   `6`

Decrease the variable by 22.5:  `-22.5`

Use square brackets (`[[` and `]]`) to increment the variable by the value of another number variable.

## example

Increase the variable `score` in the level argument `Player` by 3

![Einen Variablen Wert erhöhen](./assets/increase_example.png)
