Play
====

**Plugin**: [Sound](../../plugins/sound.md) | **Mode**: [Listen](../../editor.md#listen-actions)

Spiele einen oder mehrere soundfiles hintereinander über deinen Rechner ab.

Füge Audiodateien zu deinen [Files](../../files.md) hinzu um sie abzuspielen.

Settings
--------

### tracks

Gib einen oder mehrere Soundfiles an, indem du sie aus der MEDIA TOOLBAR in das Textfeld ziehst oder aus dem select Dropdown auswählst.

Die Tracks werden nacheinander abgespielt. Wenn ein Track zu Ende abgespielt ist, beginnt der nächste.

### loop

Benutze `loop` wenn du die Wiedergabe der tracks wiederholen willst. Mit Ende des untersten Tracks wird mit dem ersten Track erneut gestartet. Gib an wie oft die Track Liste neu gestartet werden soll (1 - x).

Gib -1 an um die Track Liste unendlich zu wiederholen.

Der loop wird unterbrochen, wenn in einen anderen State im selben [Pfad](../../paths.md) gewechselt wird.

### next state

Wechsel in den `next state` wenn alle Tracks abgespielt wurden.

Wenn `loop` angegeben ist, wird erst in `next state` gewechselt, wenn die entsprechende Anzahl Wiederholungen abgespielt wurde.

Sounds stoppen
---------------

Die Wiedergabe von Sounds die mit **Play** abgespielt werden, wird unterbrochen sobald in einen anderen State im selben [Pfad](../../paths.md) gewechselt wird.

Wenn du etwa einen anderen [listener](../../editor.md#listen-actions) im selben State einsetzt wie die **Play** action, wird die Wiedergabe unterbrochen, wenn dieser listener ausgelöst wird bevor die Wiedergabe beendet ist.

Du kannst mehrere soundfiles parallel abspielen, indem du **Play** in unterschiedlichen Pfaden einsetzt.