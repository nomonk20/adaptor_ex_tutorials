Live Mode
==========

In live mode you can control the live situation of your game. You can observe what is happening and intervene in the process.

In the navigation bar, click on the live icon, the small triangle in a circle, in the top right corner. This will open live mode.

<img src="./assets/editor-04.jpg" alt="screenshot"/>

You can close the live mode at any time by clicking on the live icon.

It is particularly important that you can manage the game status of your levels in live mode. We call the currently played live versions of a level **Sessions**. Further down in the [Session](#session) chapter is explained in more detail what this is all about.

In live mode you have access to the same pages as in [Editor](editor.md) mode, but see additional information and have different interaction options.

Level Overview
--------------

In `GAME -> overview` in addition to your levels, the active [sessions](#session) of the respective levels are listed. In addition, the status of each session is displayed. From here you can switch directly to the respective session, end a session or start a new session.

<img src="./assets/live_overview.png" alt="screenshot"/>

Dashboard
---------

Under `GAME -> dashboard` you can track which [data items](variables.md) have recently been modified and see immediately when changes take place. For example, you can keep an eye on your players and see when a new player has been added.

![Screenshot: Game Menü Dashboard](./assets/live_menu_dashboard.png)

The `LATEST` view shows the items that were last modified at the top. Items that you mark as favorites (star symbol) are `FAVORITES` collected in the view.

![Screenshot: Die Dashboard Übersicht](./assets/live_dashboard.png)

Level Editor
------------

Under `LEVEL -> edit`in the [Level Editor](editor.md) you can monitor and control the [sessions](#session) of the corresponding level. You see all sessions that are running in the open LEVEL and can select them to see which STATES are currently active in the session and change the STATE if necessary.

You can find the active sessions of a level in the right sidebar.

If a session is in progress, or even if something is broken or stuck, you will see the session there.

You can END any session manually by clicking on the trash icon.

<img src="./assets/live_session_view.png" alt="screenshot"/>

There are two sessions from the level opened here. The upper session in the list is selected and we see that the STATE _HelpMe_ is active there.

If you cannot see a session at the moment, this is quite normal and may be due to the fact that no session has been started yet or the last session has already ended.

To start a session manually, click `Create Session` Below the list of active sessions.

![Eine Session starten](./assets/live_create_session.png)

You can name the new session or leave the field blank to use an automatically generated name.

Below the name input you can specify which [data items](variables.md) should be passed to this session as [level arguments](./variables.md#item-container-und-level-arguments). You can also leave this field blank if you don't use level arguments within your level.

When the session is created, it appears in the list and is automatically selected.

If a session is selected in the sidebar, you can see which STATES are currently active in this session or which STATES were last triggered within a path. Active states are highlighted in color.

click on a STATE in the selected session to switch to that STATE in the session.

**You cannot edit the level in live mode**. First switch back to the editor mode by clicking on the live button again if you want to change something in the level.

Session
-------

Each level that you created and edited in the level editor can be played many times at the same time.

These live versions of the level can start at different times and progress in different ways, be played by different players, etc.

So a **session** is a running live version of a level.

> It is not always necessary or useful to be particularly adept at handling sessions. For some projects it will be best to manually create exactly one session of each (or only) level and not end it again.

### Edit a session

You can edit the level on which the sessions are based at any time and, if necessary, also change how the current sessions continue.

Sessions always use the latest version of the level. With the following restrictions:

1) Active states within a session are not directly adjusted by level changes. For changes to an active state to take effect, it must be triggered again.

2) If you change, add or remove arguments in the level, the level arguments of running sessions don't change `Level -> config` . For changes to the level arguments to take effect, you must restart the sessions of the respective level.

### Start a session automatically

Sessions can be started manually or automatically and also ended manually or automatically.

For example, you can specify a level for [Telegram](./plugins/telegram.md) Clients, [Twilio](./plugins/twilio.md#default-level-angeben) Phones and [devices](./plugins/devices.md#devices) which will automatically start with a new session as soon as an unknown contact is reported.

Sessions can also be started within sessions. With the [Launch Session](./actions/control/launch.md) ACTION it is possible to specify within a level that a session, within the same level or in another level, should be started at this point.

This is a good way to automatically switch from one level to the next. But you can also use it to create a superordinate _meta_ level that, under certain conditions, starts sessions from other levels.

### Active states

Within a session there is usually 1 active _current_ STATE. This means that the session is in the status of the STATE that was last triggered. This STATE 's [RUN ACTIONS](./editor.md#run-actions) are those most recently triggered and its [LISTEN ACTIONS](./editor.md#listen-actions) are currently listening to something.

As soon as the STATE is changed, all LISTEN ACTIONS that were active in the previous STATE are terminated.

[Paths](./paths.md) are an exception to this rule. Paths make it possible for several STATES to be active within a session. In turn, only one STATE can be active in each path.

