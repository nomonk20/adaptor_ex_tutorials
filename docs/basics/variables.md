Variablen, Daten, Referenzen
============================

Variablen erlauben es dir, veränderbare Werte in deinen actions einzusetzen. Variablen sind Platzhalter für Texte, Zahlenwerte oder komplexere Daten, die erst in dem Moment einen festen Wert bekommen, in dem die action ausgeführt wird.

Variablen werden in adaptor:ex durch zwei umschließende eckige Klammern gekennzeichnet. Das sieht dann z.B. so aus:

`[[Player.name]]`

In dem Moment, in dem wir unser Level designen, ist noch offen, welche Spieler:in (`Player`) das Level später spielen wird - und somit ist auch der Name (`name`) noch *variabel*.

Wird dann eine Session des Levels im Live Modus gestartet und die action ausgeführt, wird `[[Player.name]]` durch den Namen derjenigen Spieler:in ersetzt, die das Level gerade spielt.

In dieser Twilio **smsout** action nutzen wir eine Variable, um die Ansprache zu personalisieren:

![Beispiel action twilio smsout](assets/smsout_example.png)

Wird das Level etwa von einer Spieler:in gespielt, für die an einer anderen Stelle der Name 'Ada Lovelace' gespeichert wurde, sieht die sms dann so aus: `Hello Ada Lovelace! How is it going?`

Variablen können in fast allen actions eingesetzt werden. Du kannst eine variable aus dem VARIABLES-Menü der [TOOLBAR](./editor.md#toolbar) in deine action ziehen oder sie selbst eintippen.

In der VARIABLES TOOLBAR findest du die Variablen, die adaptor:ex aus den bestehenden Daten gesammelt hat. Es kann aber auch Variablen geben die dort (noch) nicht aufgelistet sind und die Variablen in der TOOLBAR könnten auch in dem Moment wo sie aufgerufen werden ins Leere verweisen. 

Variablen, insbesondere [Items](#item-container-und-level-arguments), können andere Variablen enthalten. Deshalb ist die TOOLBAR ähnlich einem Dateiverzeichnis aufgebaut. Wenn du eine Variable herüberziehst wird der Pfad, der zur eigentlichen Variable führt ggf. ergänzt.

![Eine Variable aus der TOOLBAR in die Send Message action ziehen](./assets/variables_toolbar_drop.png)

Es gibt verschiedene Orte in adaptor:ex, an denen variable Daten gespeichert werden, und verschiedene Wege, diese Daten zu *referenzieren*, also mit Variablen auf diese Daten zu verweisen. Im folgenden findest du heraus, welche Daten und Formen von Referenzen es gibt.

Lokale Variablen
----------------

Lokale Variablen sind Daten, die nur für den Verlauf einer Level Session Bestand haben.

Mit der Data action [Set](../basics/actions/data/set.md) etwa kannst du eine lokale Variable erstellen und ihr einen Wert zuweisen, oder eine bereits existierende verändern.

So kannst du z.B. an einer Stelle des Levels die Information speichern, ob eine Spieler:in den linken oder den rechten Weg eingeschlagen hat, um am Ende des Spiels wieder darauf einzugehen.

![set local varible](assets/set_local_variable.png)

Später kannst du diese variable dann z.B. in einem Text verwenden oder in der logic action [Switch](../basics/actions/logic/switch.md) nutzen, um zu bestimmen, mit welchem state es weiter geht.

Level Attributes
----------------

In der `level -> config` kannst du `Attributes` für dein Level angeben. `Attributes` ermöglichen es Eigenschaften für dein Level festzulegen.

![Level Config Attributes](./assets/variables_level_attributes.png)

Addressiere `Attributes` im entsprechenden Level indem du der variable `level` voranstellst, z.B.:

`[[level.reward]]`

Jede Live Session des Levels greift hier auf die selbe variable zu. Wenn also ein Level Attribut in einer Session geändert wird, wird Sie für alle Sessions geändert.

Level `Attribute` sind gut geeignet um in der [Launch Session](./actions/control/launch.md) darauf zurückzugreifen. So kannst du das level, dass gestartet werden soll auf Grund seiner Attribute bestimmen.

Collections und Data Items
--------------------------

Jedes Data Item ist ein Datensatz, der verschiedene Eigenschaften haben kann und einer Sammlung von items (*collection*) zugeordnet ist. 

> Ein guter Einstieg in die Arbeit mit collections und Items ist das Tutorial zum [erstellen und verwenden eines Synonymwörterbuchs](../tutorials/messenger-thesaurus/).

Unter `Game -> settings -> COLLECTIONS` findest du die Data Items, die derzeit in deinem game existieren. Hier kannst du auch neue items hinzufügen, die Eigenschaften von bestehenden items ändern oder eine ganz neue collection erstellen.

![Die Collections Übersicht](./assets/variables_settings_collections.png)

Mit der [Create Item](./actions/data/create.md) action kannst du neue Data Items auch im Laufe des Levels in einer deiner Data Collections erstellen. 

Data Items können in jedem Level verwendet werden und bleiben bestehen, auch wenn die session eines Levels beendet wurde. Das ist z.B. wichtig, wenn dein game von verschiedenen Spieler:innen gespielt wird, die im Laufe des Spiels unterschiedliche Fortschritte machen, Punkte sammeln, ihren Namen anpassen und so fort.

Plugins wie [telegram](./plugins/telegram.md) und [twilio](./plugins/twilio.md) erstellen automatisch neue `player` items, z.B. wenn ein game von einer bisher unbekannten Telefonnummer kontaktiert wird. Deshalb hat jedes adaptor:ex game bereits von Beginn an eine `player` collection.

Es gibt verschiedene Möglichkeiten, Data Items in einem Level zu referenzieren:

### Name Reference

Data Items, die eine `name` Eigenschaft haben, die bereits bekannt ist, können darüber direkt referenziert werden. 

`[[player.Ada]]`

ist ein Verweis auf das Data Item mit der `name` Eigenschaft `Ada` in der `player` collection.

Um eine Eigenschaft des Data Item zu referenzieren, fügst du sie mit Punktnotation getrennt hinten an. Etwa:

`Hello Ada, your score is [[player.Ada.score]]`

Achtung: Namen, die so als Referenz verwendet werden, dürfen keine Leerzeichen enthalten!

### Inline Query

Der Datenbank-Typ, der adaptor:ex im Hintergrund mit Daten versorgt, erlaubt es, dass wir so genannte *find querys* verwenden können, um auf items zu verweisen. *find querys* können sehr komplex und beeindruckend sein. Meist benötigen wir aber nur sehr einfache, leicht verständliche querys, um an die richtigen Daten zu kommen.

Markiere inline querys, indem du sie mit geschweiften Klammern umrandest. Für gewöhnlich verwenden wir einen *key* und einen *value*, um eine Anfrage an die Datenbank zu stellen.

`[[player.{name:'Ada'}.score]]` liefert das gleiche Item und den gleichen Wert wie die obige variable, die mit einer Namensreferenz funktioniert. Wir können mit querys aber noch mehr tun.

Zum Beispiel können wir auch alle anderen Eigenschaften, die ein item im Laufe des game erhalten hat, nutzen, um es zu referenzieren.

`[[player.{location:'At the pub'}.score]]` gibt uns den `score` des `player`, der oder die sich gerade `At the pub` aufhält. Beachte, dass der *value*, solange es sich um einen *string* handelt, in Anführungszeichen gesetzt werden muss.

Querys bieten viele weitere Möglichkeiten, items zu referenzieren. Wenn du mehr darüber erfahren möchtest, schau dich einmal in der Dokumentation von [mongo DB](https://docs.mongodb.com/manual/reference/method/db.collection.find/) um - der Datenbank, mit der adaptor:ex arbeitet.

### Item Container und Level Arguments

Wenn eine session von einem level gestartet wird, ist es möglich, dem level items als *arguments* mitzugeben. Das heißt: Während wir das level designen, arbeiten wir mit Platzhaltern für items, die erst dann referenziert werden, wenn eine session im Live Modus gestartet wird.

Die arguments können in der `level config` bearbeitet, entfernt und hinzugefügt werden. Hier können wir auch bereits items referenzieren, die  in der level session eingesetzt werden, wenn beim Start der level session kein alternatives item für dieses argument angegeben wurde.

![create level arguments](assets/variables_level_arguments.png)

Jedes level hat zu Beginn bereits ein argument `Player` definiert. `Player` ist in diesem Fall der Name des arguments, wie es beim Leveldesign verwendet wird. Die weiteren Angaben sind optional. Mit *collection* kann festgelegt werden, aus welcher Datensammlung das item, das übergeben wird, stammen muss. *Default* ermöglicht es bereits festzulegen, welches item referenziert wird, wenn beim Start der level session kein item übergeben wird. 

![create level arguments with default value](assets/variables_level_arguments2.png)

Beim Start einer level session können auch arguments übergeben werden, die nicht in der `level config` definiert wurden.

Welches item in einer neu gestarteten level session übergeben wird, kann beim manuellen Starten festgelegt werden. Auch die control action [Launch Session](../basics/actions/control/launch.md) erlaubt es, level arguments anzugeben.

Um im Leveldesign auf das variable item zuzugreifen, verwenden wir den Namen des arguments. Haben wir ein argument `Player` und wollen die *telnumber*-Eigenschaft nutzen, nutzen wir, wie bei name- und query-Referenzen, Punktnotation.

Je nachdem, welcher player item beim level session start manuell oder automatisch übergeben wurde, bezieht sich z.B. `[[Player.telnumber]]` auf die Eigenschaft *telnumber* des jeweiligen player.

Mit der data action [Load Item](../basics/actions/data/load.md) kannst du auch im Laufe des Levels items mit einem solchen *container* referenzieren. Das ermöglicht es auch, level arguments im Laufe des Levels zu überschreiben.

Manche actions erlauben es, items, die mit der action entstehen, direkt in einen container zu laden. Das item wird unabhängig von der level session erstellt und ist auch zum Session-Ende noch vorhanden, es kann aber im level direkt mit dem angegebenen Referenznamen gelesen und geändert werden.

Plugins wie telegram und twilio nutzen Level-Argumente, wenn es eingehende Nachrichten von Kontakten gibt, für die bisher kein level läuft. Wird so ein *default level* gestartet, wird überprüft, ob ein player item mit entsprechenden Kontaktdaten (z.B. einer Telefonnummer) existiert, um dieses player item an die level session als 'Player' argument weiterzugeben.
Existiert kein passendes player item, wird ein neues erstellt und an die level session weitergegeben.

Action Data
------------

Einige actions speichern Daten ab, die während des Spiels anfallen, und ermöglichen es so, im Laufe des Levels darauf zuzugreifen.

Solche action Daten sind über das keyword `state`, den State-Namen, in dem die action sich befindet, und den Namen der action lesbar:

`[[state.MyState.smsin_1.text]]` enthält, sobald der state `MyState` getriggert wurde und eine SMS eingegangen ist, den Text dieser SMS.

Das `VARIABLES`-Menü in der Sidebar zeigt dir an, welche deiner actions im Laufe des Levels Daten enthalten können und wie sie adressiert werden.

Functions
----------

Es ist auch möglich die Palette der [functions](./functions.md), die sich einfach erweitern lässt, zu nutzen, um Texte variabel zu ergänzen, Bedingungen zu überprüfen oder items zu verändern.

Eine function variable wird, wenn die entsprechende action ausgeführt wird, mit dem jeweiligen Rückgabewert (*return value*) der Funktion ersetzt.

`Es ist jetzt [[function.getTime()]] Uhr` ergibt also z.B. `Es ist jetzt 16:00 Uhr`.

Um arguments an die Funktion zu übergeben, werden sie ohne Leerzeichen und durch ein Komma getrennt zwischen die runden Klammern gesetzt:

`[[function.myFunction(arg1,arg2)]]`

Du findest alle zur Verfügung stehenden functions in der VARIABLES TOOLBAR unter FUNCTIONS.

Im Kapitel [Javascript Funktionen](./functions.md) erfährst du wie du Funktionen erstellst und zu deinem adaptor:ex Game hinzufügst.

Fehlende und leere Variablen
----------------------------

Führt eine Variable zu keinem Wert oder keinem item ist sie **undefined**. Das kann der Fall sein, weil die referenzierte Eigenschaft oder das item (noch) nicht existiert, oder weil die variable einen (Tipp-)Fehler enthält.

In einem Text, der eine leere oder fehlende Variable enthält, wird an dieser stelle auch **undefined** ergänzt. Etwa: 'Hallo [[Player.name]]!' würde, wenn kein Player existiert oder `Player` keine `name` Eigenschaft hat, zu `Hallo undefined!`
