Javascript Funktionen
=====================

Javascript Funktionen erweitern die Möglichkeiten eines adaptor:ex games um die komplette Palette der Skript Programmiersprache Javascript. Einige Funktionen sind bereits in der adaptor:ex server Installation enthalten. Es ist zudem möglich eigene Funktionen zu erstellen und einzubinden.

Funktionen hinzufügen
---------------------

So wie [Dateien und Media Files](./files.md) werden Funktionen im adaptor:ex data Ordner in `games` im jeweiligen Game Ordner abgelegt.

Erstelle eine oder mehrere javascript (`.js`) Dateien im `functions` Ordner deines Games, die deine Funktionen enthalten.

```
data
├── games
|   ├── Playground
│   |   ├── files
│   |   ├── functions
│   |   |   ├── myfunctions.js
├── log
├── nedb
├── config.json
```
Die Dateien werden als [nodejs Module](https://www.w3schools.com/nodejs/nodejs_modules.asp) in dein Game geladen. Du musst deine Funktionen also mit `module.exports` für adaptor:ex verfügbar machen

``` js
async function playerCount(args, {session, game}) {
    all_player = await game.db.player.find({})

    return all_player.length
}

function reverse(args, {session, game}) {
    let reversed = args[0].split("").reverse().join("")
    return reversed
}

module.exports = {
  playerCount:playerCount,
  reverse:reverse
}
```

Wenn du diese Zeilen in eine neue Datei mit `.js` Endung kopierst und in den functions Ordner in deinem Game verschiebst, kannst du in deinem Game auf zwei neue Funktionen "reverse" und "playerCount" zugreifen.

Du kannst beliebig viele Funktionen in einer Datei hinzufügen oder die Funktionen auf mehrere Dateien aufteilen. Die Dateinamen sind beliebig und Spielen keine Rolle.

Funktionen ausführen
--------------------

Du kannst Funktionen sowohl in der eigenständigen [Function](./actions/logic/function.md) action, als auch in allen Formularfeldern die Variablen als Eingabe erlauben, einsetzen.

### Funktionen als Variablen

Gib den Funktionsnamen, eingefasst in zwei eckige Klammern (`[[` und `]]`), in einer action Einstellung an, die Variablen als Eingabe erlaubt.

Der Funktionsname wird durch je eine geöffnete und geschlossene Klammer `()` ergänzt. Hier können auch Argumente, getrennt durch ein Komma (`,`) angegeben werden, um sie der Funktion zu übergeben.

Zwischen den Argumenten dürfen keine Leerzeichen stehen und sie dürfen keine Leerzeichen enthalten. Zudem werden die einzelnen Argumente hier immer innerhalb eines *array* als *string* übergeben.

Beispiel für eine Funktion als Variable:

`[[test(hallo,welt)]]`

Innerhalb der Funktion werden "hallo" und "welt" als *array* übergeben:

``` js
function test(args, {session, game}) {
  session.log("1. " + args[0]) // 1. hallo
  session.log("2. " args[1]) // 2. welt
}
```

Der return Wert der Funktion wird an der Stelle Eingesetzt an der die Funktion eingesetzt ist.

Du findest alle Funktionen deines Games auch in der VARIABLES TOOLBAR und kannst sie von dort in die jeweilige action ziehen.

#### Beispiel

Verwende die oben erstellte Function "playerCount" in einer [Switch](./actions/logic/switch.md) action.

![Funktion als Variable in der switch action](./assets/function_as_variable_example.png)

Ist die Anzahl der Items in der player Collection größer als 10, geht es weiter im `next state` "Party". Sonst geht es weiter mit "NetflixAndChill"

### Die Function action

Hier findest du mehr über die action heraus: [Function](./actions/logic/function.md)

Funktionen schreiben
--------------------

> Dieses Kapitel ist bei weitem nicht vollständig. Melde dich bei uns wenn du Fragen hast unter [tech@machinaex.de](mailto:tech@machinaex.de) oder auf dem [machina commons discord server](https://discord.gg/quHbQAMvF6)

Du kannst alle Eigenschaften der [nodejs](https://nodejs.org/de/) Javascript Runtime nutzen. 

Um externe libraries einzubinden die nicht in adaptor:ex enthalten sind, empfehlen wir sie mit npm global zu installieren.

Alle Übergabe Parameter, die von der level session übergeben werden sind im ersten argument enthalten.

Das zweite argument enthält immer Variablen und Funktionen aus dem adaptor:ex game und session context.

``` js
async function playerCount(args, context) {
    all_player = await context.game.db.player.find({})

    return all_player.length
}

function reverse(args, {session, game}) {
    let reversed = args[0].split("").reverse().join("")
    return reversed
}

module.exports = {
  playerCount:playerCount,
  reverse:reverse
}
```

`args` enthält die im Level angegebenen Übergabeparameter. Eine Mehrzahl an Parametern muss als *array* übergeben werden.

> Beachte, dass "args" immer ein Array ist, wenn die Funktion [als Variable aufgerufen](#funktionen-als-variablen) wird. Deshalb ist es empfehlenswert "args" stets als array zu behandeln. Auf diese Weise kannst du die Funktion in jedem Kontext einsetzen.

`context` ist ein Object, das `session` und `game` enthält.

`context.session` enthält Funktionen und Variablen, die sich auf die Session beziehen, die die Funktion aufgerufen het

`context.game` enthält Funktionen und Variablen, die sich auf das Game beziehen in dem die Session läuft, die die Funktion aufgerufen hat

Zudem hast du Zugriff auf die globale Variable `adaptor`, die Funktionen und Variablen enthält, die sich auf den adaptor:ex server beziehen, der das Game mit der entsprechenden Session hosted.

Wir werden versuchen die verschiedenen Funktionen und Variablen so bald wie möglich an dieser Stelle zu erläutern.
