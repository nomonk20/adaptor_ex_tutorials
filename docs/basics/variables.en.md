# Variables, data, references
============================

Variables allow you to use changeable values in your actions. Variables are placeholders for text, numerical values or more complex data that only get a fixed value when the action is executed.

Variables are identified in adaptor:ex by enclosing two square brackets. It then looks like this, for example:

`[[Player.name]]`

At the moment when we design our level, it is still unclear which players: in ( `Player`) will play the level later - and therefore the name (`name`) is still _variable_ .

If a session of the level is then started in live mode and the action is executed, it is `[[Player.name]]` replaced by the name of the player who is currently playing the level.

In this Twilio **smsout** action we use a variable to personalize the address:

![Beispiel action twilio smsout](assets/smsout_example.png)

If the level is played by a player for whom the name 'Ada Lovelace' was saved somewhere else, the SMS will look like this: `Hello Ada Lovelace! How is it going?`

Variables can be used in almost any action. You can drag a variable from the [TOOLBAR](./editor.md#toolbar) 's VARIABLES MENU into your action or type it in yourself.

In the VARIABLES TOOLBAR you will find the variables that adaptor:ex has collected from the existing data. However, there can also be variables that are not (yet) listed there, and the variables in the TOOLBAR could also refer to nothing at the moment they are called.

Variables, especially [Items](#item-container-and-level-arguments), can contain other variables. Therefore, the TOOLBAR is structured similar to a file directory. If you drag a variable over, the path that leads to the actual variable may be added.

![Eine Variable aus der TOOLBAR in die Send Message action ziehen](./assets/variables_toolbar_drop.png)

There are different places in adaptor:ex where variable data is stored and different ways of referencing that data, i.e. _using_ variables to refer to that data. In the following, you will find out what data and forms of references there are.

## Local Variables

Local variables are data that only last for the course of a level session.

For example, with the [Set](../basics/actions/data/set.md) Data action, you can create a local variable and assign it a value, or modify an existing one.

For example, you can save the information at one point in the level as to whether a player has taken the left or the right path, so that you can go back to it at the end of the game.

![set local varible](assets/set_local_variable.png)

You can later use this variable in a text or use it in the [Switch](../basics/actions/logic/switch.md) Logic action to determine which state to continue with.

## Level Attributes

In the `level -> config` you can specify `Attributes`  for your level. `Attributes` allow you to set properties for your level.

![Level Config Attributes](./assets/variables_level_attributes.png)

Address `Attributes` at the appropriate level by prefixing the variable `level`, i.e. :

`[[level.reward]]`

Each live session of the level accesses the same variable here. So if a level attribute is changed in one session, it will be changed for all sessions.

Levels `Attribute` can be used well in the [launch session](./actions/control/launch.md). Then you can determine the level that should be started based on its attributes.

Collections and Data Items
--------------------------

Each data item is a data set that can have various properties and is associated with a collection of items ( _collection_ ).

> A good introduction to working with collections and items is the tutorial on [creating and using a synonym dictionary](../tutorials/messenger-thesaurus/).

Under `Game->settings->COLLECTIONS` you will find the data items that currently exist in your game. Here you can also add new items, change the properties of existing items or create a completely new collection.

![Die Collections Übersicht](./assets/variables_settings_collections.png)
With the [Create Item](./actions/data/create.md) action you can also create new data items in one of your data collections during the course of the level.

Data items can be used in any level and persist even after a level's session has ended. This is important, for example, if your game is played by different players who will progress differently throughout the game, collect points, change their name and so on.

Plugins like [telegram](./plugins/telegram.md) and [twilio](./plugins/twilio.md) automatically create new `player` items, for example when a game is contacted by a previously unknown phone number. That's why every adaptor:ex game has a `player` collection right from the start.

There are different ways to reference data items in a level:

### Name Reference

Data items that have a `name` property that is already known can be referenced directly.

`[[player.Ada]]`

is a reference to the data item with the `name` property `Ada` in the `player` collection.

To reference a property of the data item, add it to the end, separated with dot notation. About:

`Hello Ada, your score is [[player.Ada.score]]`

Attention: Names that are used as a reference in this way must not contain spaces!

### Inline Query

The database type that feeds adaptor:ex behind the scenes allows us to use _find queries_ to refer to items. _find queries_ can be very complex and impressive. Most of the time, however, we only need very simple, easy-to-understand queries to get the right data.

Mark inline queries by enclosing them in curly brackets. Usually, we use a _key_ and a _value_ to make a query to the database.

`[[player.{name:'Ada'}.score]]` returns the same item and value as the variable above, which works with a name reference. But we can do even more with queries.

For example, we can also use any other properties that an item has acquired throughout the game to reference it.

`[[player.{location:'At the pub'}.score]]` gives us the `score` of the `player` who is `At the pub`  at the moment. Note that as long as the _value is a_ _string_, it must be enclosed in quotes.

Queries offer many other ways of referencing items. If you want to learn more about it, take a look at the documentation of [mongo DB](https://docs.mongodb.com/manual/reference/method/db.collection.find/) - the database that adaptor:ex works with.

### Item-Container-and-Level-Arguments

When a session is started from a level, it is possible to pass items to the level as _arguments_. That means: While designing the level, we work with placeholders for items that are only referenced when a session is started in live mode.

The arguments can be edited, removed and added in the  `level config`. Here we can already reference items that are used in the level session if no alternative item was specified for this argument when the level session was started.

![create level arguments](assets/variables_level_arguments.png)

Each level already has an argument `Player` defined at the beginning. `Player` in this case is the name of the argument as used in level design. The other information is optional. Collection can be used to determine from which data _collection_ the item that is passed must come from. _Default_ already makes it possible to specify which item is referenced if no item is passed at the start of the level session.

![create level arguments with default value](assets/variables_level_arguments2.png)

When starting a level session, arguments that were not defined in the  `level config` 
can also be passed.

Which item is passed in a newly started level session can be specified when starting it manually. The control action [Launch Session](../basics/actions/control/launch.md) also allows level arguments to be specified.

To access the variable item in the level design, we use the name of the argument. If we have an argument `Player` and want to use the _telnumber_ property, we use dot notation, as with name and query references.

Depending on which player item was handed over manually or automatically at the start of the level session, it refers, for example
`[[Player.telnumber]]`, to the _telnumber_ property of the respective player.

With the data action [Load Item](../basics/actions/data/load.md) you can also reference items with such a _container during the level._ This also allows level arguments to be overridden throughout the level.

Some actions allow items created with the action to be loaded directly into a container. The item is created independently of the level session and is still available at the end of the session, but it can be read and changed directly in the level with the specified reference name.

Plugins like Telegram and Twilio use level arguments when there are incoming messages from contacts for whom no level is running yet. If such a _default level_ is started, it is checked whether a player item with corresponding contact data (i.e. a telephone number) exists in order to pass this player item into the level session as a 'Player' argument. If no suitable player item exists, a new one is created and passed into the level session.

Action Data
------------

Some actions save data that occurs during the game and thus make it possible to access it throughout the level.

Such action data can  be read using the keyword `state`, the state name in which the action is located, and the name of the action:

`[[state.MyState.smsin_1.text]]` contains the text of this SMS as soon as the state `MyState` has been triggered and an SMS has been received.

The `VARIABLES` menu in the sidebar shows you which of your actions in the course of the level can contain data and how they are addressed.

Functions
----------

It is also possible to use the range of [functions](./functions.md), which can be easily expanded, to add text variably, to check conditions or to change items.

A function variable is replaced with the respective _return value_ of the function when the corresponding action is executed.

`It is [[function.getTime()]] now` results in `It is 16:00 now`.

To pass arguments to the function, put them between the parentheses without spaces and separated by a comma:

`[[function.myFunction(arg1,arg2)]]`

You can find all available functions in the VARIABLES TOOLBAR under FUNCTIONS.

In the [Javascript functions](functions.md) chapter you will learn how to create functions and add them to your adaptor:ex game.

## Missing and empty variables

If a variable does not result in a value or item, it is **undefined**. This can be the case because the referenced property or item does not (yet) exist, or because the variable contains a (type) error.

In a text that contains an empty or missing variable, **undefined** is also added at this point. Like:  `Hello [[Player.name]]!`,  if no player exists or has `Player` no `name`property, to `Hello undefined!`
