Getting Started
=========================

![adaptor:ex](./assets/header.png)

adaptor:ex hilft dir Interaktive Performances, Theater-Games, Installationen, Chatbot-Adventure oder Escape Rooms zu erstellen. Die grafische Programmieroberfläche ist darauf optimiert Dramaturgie und Game Mechanik Zusammenzubringen und übersichtlich anzuordnen. 

Du kannst schnelle Anpassungen während der Proben vornehmen und den Live Ablauf während der Tests und Aufführungen überwachen und kontrollieren.

Der adaptor:ex Editor wird über den Browser aufgerufen, sodass ihr mit mehreren Leuten das selbe Game editieren und kontrollieren könnt.

![Screenshot: Der adaptor:ex editor](./assets/editor_example.png)

Hier findest du mehr darüber hinaus, wie du mit adaptor:ex deine Storyline, Game Mechanik, Software und Hardware miteinander verdrahtest und unter Kontrolle bringst.

Hole und installiere dir adaptor:ex auf deinem lokalen Rechner oder auf einem Server. Es gibt verschiedene Wege für die [Installation](#installation).

Beginne mit einem [Tutorial](./tutorials/arduino-serial/index.md), wenn du eine der verschiedenen Möglichkeiten kennenlernen willst auf die du adaptor:ex zu nutzen kannst.

Im [Glossar](./basics/editor.md) sind die Grundbausteine, Actions und Plugins genauer beschrieben.

Installation
------------

Um adaptor:ex zu installieren musst du derzeit die Kommandozeile deines Betriebssystems nutzen. Unter macOS suche das Programm "Terminal". Unter Linux findest du das Terminal unter der Tastenkombination [ctrl] + [alt] + [T]. Unter windows suche die Anwendung "Eingabeaufforderung" bzw. "Commandline".

Es stehen drei Installationsmöglichkeiten zur Auswahl: [NPM](#installieren-mit-npm), [SOURCE](#installieren-von-source-mit-npm) und [DOCKER](#installieren-mit-docker). 

Wenn du die Anschlüsse an deinem Rechner nutzen willst, z.B. den Seriellen Anschluss per USB oder eine Soundkarte, installiere adaptor:ex mit [NPM](#installieren-mit-npm) oder via [SOURCE](#installieren-von-source-mit-npm).

Im besonderen für eine Server Installation empfehlen wir die Installation in einem [Docker Container](#installieren-mit-docker). 

adaptor:ex ist gerade erst an den Start gegangen. Wir werden in den kommenden Monaten häufig Fehler beheben, Dinge (hoffentlich) verbessern und neue Features hinzufügen. Führe hin und wieder ein Update via [NPM](#updates-mit-npm) oder [Docker](updates-mit-docker) durch, um auf dem neuesten Stand zu bleiben.

Melde dich bei uns, wenn du Schwierigkeiten hast, adaptor:ex zu installieren. Schreib uns an [tech@machinaex.de](mailto:tech@machinaex.de) oder schau auf unserem [Discord](https://discord.gg/quHbQAMvF6) vorbei.

### Installieren mit NPM

Bevor Du mit der Installation von adaptor:ex fortfahren kannst, brauchst Du die aktuelle Version von NodeJS. Die nötigen Installatiosdateien für dein Betriebssystem findest Du [hier](https://nodejs.org).

Sobald Du NodeJS installiert hast, öffne deine Konsole bzw. das Terminal und führe in der Kommandozeile die Installation mit npm aus

Auf macOS und Linux

```console
sudo npm install -g adaptorex --unsafe-perm
```
Auf Windows
```console
npm install -g adaptorex --unsafe-perm
```

Wenn die Installation erfolgreich war, starte adaptor:ex mit dem Kommando

```console
adaptorex
```

Möglichkeiten adaptor:ex zu Konfigurieren findest du in der [adaptor:ex server readme](https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server#install)

Um den adaptor:ex server wieder zu beenden benutze die Tastenkombination [Strg] + [C] (windows/linux) bzw. [Cmd] + [C] (MacOS), oder gib `quit` in die Konsole ein.

### Updates mit NPM

Lade dir die neueste Version von adaptor:ex herunter indem du die obige Installation wiederholst.

Mit `npm outdated -g` kannst du überprüfen ob eine neue Version zur Verfügung steht.

### Installieren von Source mit NPM

Bevor Du mit der Installation von adaptor:ex fortfahren kannst, brauchst Du die aktuelle Version von NodeJS. Die nötigen Installatiosdateien für dein Betriebssystem findest Du [hier](https://nodejs.org).

Hole dir adaptor:ex Server und Client:

**Server**

Klone oder lade dir das aktuelle GitLab-Repository des adaptor:ex Servers hier herunter: [https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server](https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server)

**Client**

Klone oder lade dir das aktuelle GitLab-Repository des adaptor:ex Clients hier herunter: [https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_client](https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_client)

Jetzt kannst du den Server und den Client mit [NPM](https://www.npmjs.com/) lokal installieren.

Wechsel im Terminal (der Konsole) in das Verzeichnis in das du den Server oder Client geladen oder geklont hast.

```console
cd adaptor_ex_server/
```

bzw.

```console
cd adaptor_ex_client/
```

Wenn du in dem richtigen Verzeichnis bist führe den folgenden Befehl aus:

```console
npm install
```

Wenn die Installation erfolgreich war, starte den adaptor:ex Server im entsprechenden Verzeichnis mit dem Kommando

```console
node index.js
```

Starte anschließend, in einer separaten Commando Zeile, den adaptor:ex Client im entsprechenden Verzeichnis mit dem Kommando

```console
npm run serve
```

### Updates mit git

Lade dir die neueste Version von adaptor:ex server und client herunter indem du im jeweiligen Verzeichnis das Git Repository mit folgendem Befehl updates

```console
git pull
```

Starte den adaptor server und client anschließend neu.

### Telegram Erweiterung installieren

Um das Telegram Plugin in adaptor:ex nutzen zu können musst du zusätzlich [python](https://python.org) und einige python Bibliotheken installieren. Folge den Anweisungen der adaptor:ex [Telegram Plugin Readme](https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server/-/blob/main/plugins/telegram/README.md).

### Installieren mit Docker

Lade Docker für dein Betriebsystem herunter und folge den Installationsanweisungen auf [https://docs.docker.com/get-docker/](https://docs.docker.com/get-docker/)

Erstelle einen neuen Ordner in dem du adaptor:ex installieren willst.
 
Öffne deine Kommandozeile, wechsle mit dem `cd` command in das adaptor:ex verzeichnis und lade die [adaptor:ex docker-compose](https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server/-/blob/main/docker-compose.yml) Datei herunter:

```console
curl -o docker-compose.yml https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server/-/raw/main/docker-compose.yml
```

Führe die Docker Compose Datei aus:

```console
docker-compose up -d
```

adaptor:ex Server, Client, Datenbank und Plugin Erweiterungen werden heruntergeladen und installiert.

In der [adaptor:ex server readme](https://gitlab.com/machina_ex/adaptor_ex/adaptor_ex_server#install) kannst du mehr über die Konfiguration von adaptor:ex erfahren.

### Updates mit Docker

Um die adaptor:ex Docker installation zu aktualisieren wechsle in das adaptor:ex Verzeichnis. Dort führe folgende Kommandos aus:

```console
docker-compose pull
```

und anschließend

```console
docker-compose up -d
```

Erste Schritte
---------------

Gebe die URL, also die webadresse, deines adaptor:ex setups im Web Browser ein. 

> Wir empfehlen die aktuellste Version von Google Chrome oder Firefox zu verwenden.

Wenn du adaptor:ex mit den Standardeinstellungen auf deinem Rechner oder Laptop installiert hast, kannst du auch auf einen dieser links klicken: [http://localhost:8081](http://localhost:8081) (Installation per NPM) oder [http://localhost:8080](http://localhost:8080) (Source Installation). 

Hast du adaptor:ex auf einem Server oder einem anderen Rechner installiert musst du die URL durch die IP oder den Domainnamen deines Servers ersetzen.

### Ein Game erstellen

![Die adaptor:ex Startseite](./assets/no_games.png)

Beginne damit, ein neues Game Projekt anzulegen. Klicke auf `New Game` und gib deinem ersten Game einen Namen.

![Ein neues Game erstellen](./assets/new_game.png)

### Level hinzufügen
Wenn du das neu erstellte Game Projekt anklickst, landest du in der Übersicht, die alle existierenden level anzeigt. Jedes Game kann aus mehreren Leveln bestehen, die auf verschiedenste Art miteinander verknüpft sind.

Erstelle dein erstes Level mit `Add new`.

> Games und Level bekommen eine eigene Web Adresse, über die sie im Web Browser erreichbar sind. Darum darf der Name keine Leerzeichen oder Sonderzeichen enthalten.

Gib dem Level einen Namen und klicke auf `Add new Level`.

![Ein Level hinzufügen](./assets/new_level.png)

### Level bearbeiten

Öffne das level anschließend, indem du auf den Namen klickst. Jetzt befindest du dich im level Editor.

Start (*START*) und Endpunkt (*QUIT*) deines Levels existieren bereits.

![Der Level Editor](./assets/empty_level.png)

Suche aus der linken Seitenleiste, den [actions](./basics/editor#action), die control action [Log Message](./basics/actions/control/log.md) heraus und ziehe sie per Drag and Drop auf die Mitte der Seite.

Es entsteht ein neues [State](./basics/editor#state) Element, dass eine [Log Message](./basics/actions/control/log.md) action enthält (*log_1*). 

![Eine Action hinzufügen](./assets/log_action.png)

Klicke auf die neue action, um sie zu bearbeiten. Schreibe einen kurzen Text in das `message` Eingabefeld und wähle im `level` Dropdown *info* aus.

![Eine Action bearbeiten](./assets/edit_log_action.png)

Um dein Level verständlicher zu gestalten, ist es gut allen States nachvollziehbare Namen zu geben.

> Auch State Namen dürfen keine Leerzeichen beinhalten, da sie in [Variablen](./basics/variables.md) zum Einsatz kommen können

Klicke in die Titelzeile deines Log Message State und benenne ihn um. 

![States umbenennen](./assets/rename_state.png)

Bearbeite auch die [Next](./basics/actions/control/next.md) action *next_1* im *START* State indem du sie anklickst. Wähle dann den neuen State, hier *HelloWorld*, aus dem `next state` Dropdown aus um ihn mit dem *START* State zu verbinden.

![2 States verbinden](./assets/connect_log_action.png)

### Level starten

Wechsel vom Bearbeitungs in den [Live Modus](./basics/live.md), indem du auf den Play button in der oberen rechten Ecke klickst 

![Live Modus Toggle](./assets/live_toggle.png)

Die linke Seitenleiste mit den actions schließt sich und ein Menü öffnet sich an der rechten Seitenleiste.

Klicke in der rechten Seitenleiste auf `Create Session` und dann auf `Start Session` um eine [Session](./basics/live#session) von deinem Level zu erstellen.

![Eine Session starten](./assets/start_session.png)

Die neu gestartete Session ist in der rechten Seitenleiste ausgewählt.

![Eine session auswählen](./assets/select_session.png)

Der *HelloWorld* State ist bereits hervorgehoben.

Wenn wir eine neue Session von einem Level erstellen, wird automatisch der *START* State ausgelöst. Die *next_1* action hat direkt danach zu unserem State *HelloWorld* mit der Log Message weitergeführt.

Öffne die Log Konsole, indem du auf den cursor icon klickst.

![Log Konsole anzeigen](./assets/console_toggle.png)

Die neueste Log Nachricht sollte 'Hello World!' sein.

Indem du auf den *HelloWorld* State klickst, kannst du ihn manuell erneut auslösen. Beobachte die Log Konsole ob die Nachricht erscheint.

### Level automatisieren

Wechsel zurück in den Bearbeitungsmodus, indem du erneut auf den Live Play Button klickst.

Suche die [Timeout](./basics/actions/time/timeout.md) action in der rechten Seitenleiste heraus und ziehe sie auf den *START* State. 

Wenn du die action darüber ziehst wird der State hervorgehoben und zeigt an, dass die action diesem State hinzugefügt wird, anstatt einen neuen State zu erstellen.

> Wenn wir einem State eine action hinzufügen, die auf etwas wartet, wie ein timer der abläuft, ersetzt sie automatisch eine etwaige [Next](./basics/actions/control/next.md) action.

Bearbeite den timeout. Gib eine kleine Zahl Sekunden im `timeout` feld an und wähle den *HelloWorld* State als `next state` aus.

![Einen timeout erstellen](./assets/timeout.png)

Füge auch dem *HelloWorld* State eine Timeout action hinzu. Bearbeite den Timeout und wähle diesmal *Quit* als `next state` aus.

Wenn du nun in den Live Modus wechselst und eine neue Session startest werden die States in festen Zeitabständen nacheinander ausgelöst.

![Mehrere Sessions gleichzeitig](./assets/automated_session.png)

Sobald der *QUIT* State erreicht ist, wird die session beendet und verschwindet aus der rechten Seitenleiste.

Deine erste Session wird sich nicht automatisch beenden. Um sie zu beenden kannst du sie in der rechten Seitenleiste schließen (x) oder den *QUIT* State auslösen wenn du die Session ausgewählt hast.

> Jede Session basiert immer auf der aktuellsten Version des Levels. Wenn jedoch der State, in dem sich eine Session befindet, verändert, muss er erneut ausgelöst werden.

Nächste Schritte
----------------

In der **Referenz** findest du mehr über die verschiedenen Grundbausteine von adaptor:ex heraus.

**Tutorials** leiten dich Schritt für Schritt durch Projekte die du mit adaptor:ex umsetzen kannst.

Lerne zum Beispiel, wie du das Telegram plugin einsetzt, um ein messenger Adventure zu entwickeln: [Storytelling mit Telegram und adaptor eX](./tutorials/telegram-basics/index.md)

Oder beginne damit, einen Arduino mit adaptor:ex anzusteuern: [Arduino Serial](./tutorials/arduino-serial/index.md)

Autoren und Unterstützer
-------------------------

Dies ist ein [machina commons](https://www.machinacommons.world) Projekt von [machina eX](https://machinaex.com).  

gefördert durch

<img src="https://machinacommons.world/img/Logo_Senat_Berlin.png" alt="Logo Senatsverwaltung für Kultur und Europa Berlin" width="50%"/>

Senatsverwaltung für Kultur und Europa Berlin  
