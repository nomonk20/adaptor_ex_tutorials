# Telegram Einrichten: Setup / Settings / Add Client

Bevor wir mit adaptor:ex den Messenger Service Telegram für benutzen können, müssen wir adaptor:ex dafür konfigurieren.

Das funktioniert für Telegram sowohl im Server Modus als auch im Lokalen Modus, aber auf einem Server kläre bitte immer vorher ab, ob du die Erlaubnis das Telegram Plugin zu benutzen. 

## Vorbereitung

<span style="color:red">**ACHTUNG:**</span> *Sei dir im Klaren darüber, dass andere mit Zugriff auf den adaptor:ex server, deine Telefonnummer und Telegram API credentials (dazu kommen wir weiter unten) lesen können! Gehe verantwortlich mit diesem Tool um. Telegram blockt User Accounts wenn diese als Spam auffallen oder markiert werden. Außerdem musst du sicherstellen dass alle Daten von Menschen die mit eurer adaptor:ex instanz via telegram kommunizieren, geschützt sind und alle Datenschutzrichtlinien die für euch zutreffen eingehalten werden. Benutze auf keinen Fall deine private Nummer! Deine Freunde wissen ja nicht dass du mit Telegram Apps spielst und ihre Daten verarbeitet werden!*

Wenn du die Erlaubnis hast und Datenschutz mäßig sicher bist, dass alles in Ordnung ist, brauchst du folgendes:

- Einen Telegram Account mit dazugehöriger Telefonnummer (sei dir im Klaren darüber dass adaptor:ex über das Plugin vollen Zugriff auf den telegram account bekommt)
- Zugriff auf adaptor:ex
- ein bereits angelegtes Game in adaptor:ex

## Setup Telegram

### Account bei Telegramm anlegen

1. Nun gehen wir in das Game, indem wir Telegram benutzen wollen und wählen dort in der oberen Leiste `Game > Settings` aus.
2. Dort fügen wir aus der Liste der inaktiven Plugins das Plugin `Telegram` hinzu und öffnen es.
<img src='./assets/4.png'/>
Wir sehen 3 Felder: `api_id`,`api_hash` und `port`. Die 3 Felder wollen ausgefüllt werden. `port` muss eine Port Nummer bekommen, die noch nicht auf unserer adaptor:ex instanz benutzt wird (Im Zweifelsfall weiss das die Admin). Wenn noch keine anderen Plugins ports belegen, können wir es einfach beim Default Wert (hier: `5000`) belassen.

3. Um `api_id` und `api_hash` ausfüllen zu können, müssen wir uns einen account bei <a href='https://my.telegram.org'>`https://my.telegram.org`</a> anlegen:
<img src='./assets/1.png'/>

4. Dort loggen wir uns dann ein und erstellen unter `API developer tools` die credentials für eine client app. Mehr Infos dazu unter: [https://core.telegram.org/api/obtaining_api_id](https://core.telegram.org/api/obtaining_api_id) 
<img src='./assets/2.png'/>

5. Nachdem wir das getan haben sollten wir unter `API developer tools` unsere persönliche `api_id` und `api_hash` bekommen haben und können diese nun in die entsprechenden Felder in unseren adaptor:ex Game Settings eintragen:
<img src='./assets/3.png'/>
<img src='./assets/4.png'/>

6. Anschließend klicken wir oben rechts in unserem Telegram Plugin Settings auf den Reload Plugin Button (der Zirkel Pfeil) um unser Game als Telegram Client zu aktivieren.
<img src='./assets/5.png'/>
    
Der rote Indikator sollte nun grün werden. Ab sofort ist unsere adaptor:ex instanz eine Telegram Client. Also in etwa so wie die Telegram App auf unserem Smartphone.

### Telefonnummer des Accounts im Telegram-Plugin anmelden

7. Als nächstes müssen wir unsere Telefonnummer in unserer Telegram Plugin App anmelden. Dazu fügen wir einen Telegram Client im Plugin hinzu (der blaue Plus Button unten unter `ITEMS`/`CLIENT`).
<img src='./assets/6.png'/>
Dem Client geben wir einen guten Namen (bei machina eX Games nehmen wir normalerweise den Namen der Figur, dessen Telegram Account das in unserer Spielwelt sein soll, hier z.B. `Alice`)

8. Dann klicken wir auf den Tab Settings in unserem Client, und geben die Telefonnummer ein mit dem wir uns einloggen wollen. 

<span style="color:red">**WICHTIG**</span>: Diese Telefonnummer sollte schon bei Telegram angemeldet sein und wir brauchen zugriff auf Nachrichten von Telegram im nächsten Schritt. Stelle also sicher, dass du a) angemeldet bist und b) Zugriff mit einer Telegram App (z.b. auf einem Smartphone) hast und Nachrichten von Telegram an diese Nummer empfangen kannst!

<img src='./assets/7.png'/>

9. Nachdem wir unsere Telefonnummer eingetragen haben, klicken wir erst auf SAVE, dann auf den Connect Button (das Stecker Symbol oben rechts in unserem Telegram Client Settings Feld).
<img src='./assets/8.png'/>

10. Das sollte nun unsere Telefonnummer/unseren Telegram Account durch unsere Plugin Telegram App bei Telegram anmelden. Wir sehen daher (2tes Bild) einen Prompt in adaptor:ex der uns nach einem Login Code fragt und (1tes Bild) sollten in Telegram von Telegram eine Nachricht an die gerade registrierte Telefonnummer bekommen die eben diesen Login Code beinhaltet. Das funktioniert genauso als würden wir uns zum ersten Mal mit einer neuen App/einem neuen Gerät in einen Telegram Account anmelden.
<img src='./assets/9.png'/>
<img src='./assets/10.png'/>

Wir tragen also den Login Code in den Prompt unter `code` ein und klicken `send`.

11. Wenn alles geklappt hat, sollten wir nun eine zweite Nachricht von Telegram bekommen, die ungefähr so aussieht wie in diesem Bild:
<img src='./assets/11.png'/>

Das ist der Telegram Service, der nochmal sicherstellt dass wir auch wissen dass nun eine neue App Zugriff auf das Telegram Konto mit unserer Telefonnummer hat.

12. Nun klicken wir noch einmal auf den Reload Button unseres Clients um alles ans Laufen zu bringen und in den Settings weiterzu machen.
<img src='./assets/12.png'/>

13. adaptor:ex und der Telegram Plugin empfangen jetzt alle Nachrichten für diese Telefonnummer, genauso wie die anderen Telegram Apps mit denen diese Nummer angemeldet ist.

### Telegram Plugin mit LEVEL verbinden
Aber wir müssen adaptor:ex nun noch sagen, in welches Level es die eingehenden Nachrichten leiten soll: Dazu gehen wir in unseren Telegram Plugin Client `Alice` und klicken auf den `Settings` Button. Dort wählen wir die Option `default level` aus und tragen dort ein Level ein, in dem alle eingehenden Telegram Nachrichten geleitet werden sollen, die von Nummern kommen, die gerade in keinem anderen Level aktiv sind.
    
<img src='./assets/13.png'/>

In unserem Beispiel ist das hier das Level `Send_Telegram`

<img src='./assets/14.png'/>


Immer wenn nun eine Telegram Nachricht an unseren Client `Alice` geschickt wird, schaut der adaptor:ex Telegram Plugin nach, ob die schreibende Telefonnummer schon in einem Gespräch in einem unserer Levels ist. Wenn dem nicht so ist, erstellt sie automatisch eine `Player Variable` mit den Telegram Daten der schreibenden Person und startet eine neue `Session` des `Default Levels`. 

Dort können wir dann auf Nachrichten der `Player` warten, oder ihnen als `Alice` Telegram Nachrichten schicken, oder sie in andere Levels weiterleiten und und und....

Damit ist das Setup unserer Telegram Plugin App als auch unseres ersten Clients `Alice` abgeschlossen. 

Wir können natürlich mehrere Clients innerhalb unserer Plugin App anmelden. Dafür brauchen wir aber weitere bei Telegram bereits registrierte Telefonnummern.

### Loslegen
Wenn alles eigerichtet ist, geht es hier zum ersten Tutorial für ein Telegram-Adventure-Game: [Create a simple telegram adventure with adaptor:ex](../telegram-basics/index.md){target=_blank}