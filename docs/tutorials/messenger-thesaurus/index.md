Ein Synonymwörterbuch erstellen und verwenden
==============================================

Wenn wir eingehende Nachrichten in einem Messenger abfragen, kann es sein, dass wir häufig auf ähnliche Wörter oder Phrasen mit der selben Antwort reagieren wollen.

> Dieses Tutorial setzt voraus, dass du schon einmal ein Game erstellt, die ersten States in einem Level verknüpft und eine Session von deinem Level erstellt hast.

Mithilfe von [Collections und Data Items](../../basics/variables.md#collections-und-data-items) können wir ein Thesaurus oder Synonymwörterbuch erstellen, dass in jedem Level und jeder Session zur Verfügung steht. Mehrere verwandte Begriffe sind dann unter einem Sammelbegriff zusammengefasst und wir können auf verschiedene ähnliche eingehende Nachrichten mit einer einzigen Abfrage reagieren.

Ein Wörterbuch erstellen
-------------------------

Unter `GAME > settings > COLLECTIONS` finden wir alle global verfügbaren Daten unseres Games. Die Daten die hier zu sehen sind, sind in allen Levels und deren Sessions verfügbar und können auch von jeder [Session](../../basics/live.md#session) erstellt, geändert und gelöscht werden.

Jedes Game besitzt bereits eine "player" Collection in der zum Beispiel neue Einträge (*Items*) entstehen, wenn einer deiner Messenger Clients von einem unbekannten Account kontaktiert wird.

Erstelle für das Synonym Wörterbuch eine neue eigene Collection indem du auf `Create Collection` klickst. 

![Screenshot: Die collection Übersicht](./assets/create_collection.png)

Nenne die collection z.B. "thesaurus" und klicke auf `Create`.

![Screenshot: Eine neue Collection erstellen](./assets/collection_name.png)

Die "thesaurus" Collection ist nun verfügbar.

Jede Collection kann mehrere Data Items enthalten welche wiederum mehrere Werte, Texte, Zahlen oder Listen enthalten können. Für ein einfaches Synonymwörterbuch brauchen wir nur 1 Item.

Klicke auf den `Add Item` Button in deiner "thesaurus" Collection. 

![Screenshot: Die collection Übersicht mit thesaurus collection](./assets/add_item.png)

Es öffnet sich ein Fenster mit einem Text Editor. Soweit ist unser Dokument leer und es enthält lediglich eine öffnende und eine schließende geschweifte Klammer.

`{}`

adaptor:ex Items werden in [JSON](https://www.json.org/json-de.html) beschrieben. Jedes JSON Document in unserer Collection beginnt und endet mit einer geschweiften Klammer. 

Beginnen wir damit unserem Wörterbuch einen Namen zu geben, um es später in unserem Level einfach adressieren zu können. Wir fügen dem leeren Document eine Variable "name" hinzu und weisen ihr den Wert "words" hinzu. 

Kopiere den folgenden Abschnitt und ersetze damit den Inhalt im `Create Item` Textfeld.

```json
{
    "name":"words"
}
```

Klicke anschließend auf `Create`.

![Screenshot: Ein neues Item hinzufügen](./assets/create_item.png)

Wenn du nun auf den Titel der "thesaurus" Collection klickst, ist dort dein neu erstelltes Dokument zu sehen. Neben der `name` Eigenschaft sind noch einige Meta Informationen automatisch hinzugefügt worden.

Synonyme hinzufügen und bearbeiten
-----------------------------------

Wir fügen nun für jede Sammlung synonymer Begriffe eine Liste in unserem "words" Item an.

Klicke auf die "thesaurus" collection und anschließend auf den Bearbeiten Button (Stift Symbol) deines "words" Item.

![Screenshot: Ein collection Item bearbeiten](./assets/edit_item.png)

Das Textfeld zum Bearbeiten des Item öffnet sich wie beim Erstellen des Items.

Für jede Synonym Sammlung ordnen wir nun eine Liste von Begriffen einem Sammelbegriff zu. Wenn wir ähnliche Begriffe für "ja" und für "nein" sammeln wollen sieht unser Item etwa so aus:

```json
{
    "name":"words",
    "ja":["ja","jo","yes","jup","jep","okay","klar","einverstanden"],
    "nein":["nein","nö","no","nicht","nie"]
}
```

Ersetze hiermit den Text im Textfeld oder schreibe deine eigenen Synonymlisten um sie dem "words" Item hinzuzufügen.

![Screenshot: Synonym Listen zu einem collection Item hinzufügen](./assets/edit_item_modal.png)

Beachte die besondere JSON Syntax. Sie muss akkurat eingehalten werden, ansonsten wirst du eine Fehlermeldung bekommen und kannst das Dokument erst updaten wenn alle Syntax Fehler Behoben sind. 

- Alle Wörter sind in Anführungszeichen (`"`) gefasst. 
- Zwischen dem Sammelbegriff und der Liste ist ein Doppelpunkt (`:`). 
- Zwischen allen Begriffen in der Synonymliste ist ein Komma (`,`) sowie zwischen den Eigenschaften ("name","ja","nein") deines Item. 
- Eine Liste (*array*) ist in eckige Klammern eingefasst (`[]`).

Online findest du viele Tutorials und Hilfen zum erstellen von [JSON](https://www.json.org/json-de.html) und welche Möglichkeiten es dir bietet Daten zu strukturieren.

Das Synonymwörterbuch im Level Editor einsetzen
----------------------------------------------

Wenn wir ein Level bearbeiten setzen wir unsere Synonym Listen als [variablen](../../basics/variables.md) ein. Auf das Synonymwörterbuch können wir über seine `name` Eigenschaft "words" zuzugreifen.

Adressiere per Punkt (`.`) Notation die collection, das Item und zuletzt den Sammelbegriff:

Z.B.: `[[thesaurus.words.ja]]` oder `[[thesaurus.words.nein]]`

> Du kannst auch auf die einzelnen Wörter in der Liste Zugreifen indem du zusätzlich ihre numerische Stelle, beginnend mit `0`, in der Liste angibst: `[[thesaurus.words.ja.3]]` ergibt in unserem Falle "jup"

### Synonyme in der Log Konsole anzeigen

Probiere aus, wie du auf die Synonymliste zugreifen kannst indem du die variable in einer Control [Log](../../basics/actions/control/log.md) action verwendest.

Zieh eine [Log](../../basics/actions/control/log.md) action aus der TOOLBAR auf die STAGE und gib deine "ja" Synonym variable in `message` an.

![Screenshot: Synonym variable in der log action im LogYes state](./assets/log_yes.png)

Starte eine Session und wechsle in den "LogYes" State. Wirf einen Blick in die Log Konsole indem du auf das Konsolen Icon `>_` oben rechts Klickst. Du solltest die Liste aller Begriffe die unter "ja" gesammelt sind angezeigt bekommen.

![Screenshot: Die Synonyme für ja in der Log Konsole](./assets/log_console_yes.png)

### Eingehende Nachrichten auf Synonyme abfragen

Wir nutzen das Synonymwörterbuch für eine einfache Abfrage im Telegram Messenger.

> Falls du das Telegram Plugin bisher noch nicht im Einsatz hattest schau dir zunächst das[ Tutorial zur Einrichtung des Telegram Plugin](../telegram-settings/index.md) an.

Lass uns zunächst eine Nachricht mit einer "ja/nein" Frage mit der [Send Message](../../basics/actions/telegram/sendMessage.md) Action an den Player schicken.

Ziehe die [Send Message](../../basics/actions/telegram/sendMessage.md) Action auf die STAGE und gib unter `message` eine Frage an, die mit "ja" oder "nein" beantwortet werden kann.

![Screenshot: Send Message Action mit Frage](./assets/send_message.png)

Füge nun eine [Dialog](../../basics/actions/telegram/dialog.md) action zum "YesOrNo" State hinzu. Für unsere Abfrage brauchen wir 2 `if` Bedingungen. Eine, die die eingehende Nachricht auf unsere "ja" Synonymliste abfragt und eine, die auf unsere "nein" Synonymliste abfragt.

Füge über `Settings` die `if` Option hinzu und klicke auf `Add Condition`.

![Screenshot: Eine if Bedingung zum Dialog hinzufügen](./assets/dialog_if.png)

Wir machen eine `contains` Abfrage und geben im `contains` Feld unsere thesaurus variable an: `[[thesaurus.words.ja]]`

![Screenshot: Dialog mit thesaurus in contains abfrage](./assets/dialog_contains.png)

Füge eine weitere Bedingung mit `Add condition` hinzu und gibt dieses mal `[[thesaurus.words.nein]]` für `contains` an.

Erstelle im [Live Modus](../../basics/live.md) eine Session von diesem Level und gib deinen Telegram Account als *Player* Argument an oder verknüpfe das Level als `default level` mit dem Telegram client, den du im telegram Plugin eingerichtet hast.

Die entsprechende Reaktion wird nun ausgelöst, wenn die eingehende Nachricht einen der in der Thesaurus Collection angegebenen Begriffe enthält.

![Screenshot: Der Editor im live modus und der Telegram Chat](./assets/example.png)

Da wir eine `contains` Abfrage verwenden, wird überprüft ob einer der Begriffe in unserer Synonymliste in der Nachricht **enthalten** ist. 

Wenn wir `contains` nutzen können, reicht es die Grundformen der Synonyme im Synonymwörterbuch einzutragen. Der Eintrag "ja" in der Wortliste wird sowohl eine Eingehende Nachricht "ja gerne" als auch "jawohl" matchen.

> Mit einer `equals` Abfrage, würden wir feststellen ob die gesamte Nachricht einem der Begriffe in der Synonymliste **genau entspricht**. In diesem Falle muss auch die Synonym Liste genauen Begriffe enthalten.

### Das Synonymwörterbuch als Level Argument angeben

Wenn du häufig Synonymwörterbücher verwendest, kann es hilfreich sein, das Wörterbuch Item als [*Level Argument*](../../basics/variables.md#item-container-und-level-arguments) für dein Level einzurichten.

Dies ermöglicht es dir die Synonyme beim editieren des Levels einfacher zu adressieren. Zudem findest du deine *Level Argumente* in der *Varaibles* Toolbar im Level Editor und kannst die Synonyme dann per drag'n'drop in deine Actions ziehen. Wenn du mit mehreren Wörterbüchern arbeitest bist du auf diese Weise zudem flexibler wenn du Wörterbücher für deine Level tauschen möchtest und kannst Wörterbücher z.B. in der [Launch Session](../../basics/actions/control/launch.md) dynamisch zuweisen.

Wechsle in die `LEVEL > config`.

![Screenshot: Level Config Menü Auswahl](./assets/level_config.png)

Klicke auf `Add Argument `um ein neues *Level Argument* hinzuzufügen.

![Screenshot: Add Level Argument Button](./assets/add_argument.png)

trage für das neue *Level Argument* die folgenden Werte ein

![Screenshot: Angaben für das level Argument](./assets/thesaurus_argument.png)

#### 1. NAME:  ```Words```

Unter diesem Begriff kannst du dein Synonymwörterbuch anschließend im Level adressieren.

#### 2. COLLECTION: ```thesaurus```

Der Name der Collection die wir für unsere Wörterbücher angelegt haben.

#### 3. DEFAULT QUERY: ```{name:"words"}```

Das Item, dass als `Words` in unser Level eingebunden werden soll, wenn eine neue [Session](../../basics/live#session) von deinem Level gestartet wird und wenn beim Start der Session kein anderes `Words` Item übergeben werden soll.

> Eine Query ist eine Suchanfrage in einer Collection. Wir geben hier an, dass ein Item in der `thesaurus` Collection mit `name` `"words"` als `Words` im Level verfügbar sein soll.

#### 4.

Bestätige die Angaben mit klick auf das Kontrollkästchen

Wenn du nun eine Synonymliste im Level Editor verwenden willst gib als Variable das *Level Argument* `Words` und durch einen Punkt `.` getrennt, den Sammelbegriff an, z.B.:

`[[Words.ja]]`

Du findest die Synonymlisten, die du im "words" Thesaurus angelegt hast jetzt auch in der VARIABLES TOOLBAR.