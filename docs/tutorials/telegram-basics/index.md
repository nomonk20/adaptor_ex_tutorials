# Storytelling mit Telegram und adaptor eX

adaptor:ex ist Teil des Projekts MACHINA COMMONS und wird im Programm "Digitale Entwicklung im Kulturbereich" der Berliner Senatsverwaltung für Kultur und Europa gefördert.

adaptor:ex ist eine open-source Software-Engine, die es ermöglicht, interaktive Theatergames und -installationen umzusetzen. Ein Teilbereich davon sind partizipative Performances und multimediale Chat-Adventure innerhalb des Messaging-Service "Telegram".

## Vorab:

Wir gehen davon aus, dass du oder eine Techniker:in bereits einen Telegram Client mit default level eingerichtet hat. Wenn nicht, findest du hier das [Tutorial](../telegram-settings/index.md){target=_blank}. 

adaptor:ex kann zu großen Teilen mit einem Browser (wir empfehlen Chromium/Chrome oder Firefox) bedient werden.
Um dein Level zu testen, brauchst du natürlich den Messenger Telegram - entweder als App auf deinem Smartphone oder auf deinem Computer.

<div class="page"/>

## Schritt für Schritt

### Grundlagen

1. Wenn du dich im GAME OVERVIEW befindest, klicke auf das Level (lila Button), das du bearbeiten möchtest …     

    <img src="./assets/levelview2.png" alt="screenshot"/>

2. … um in den Level Editor zu gelangen:

    <img src="./assets/leveleditor-1-1.png" alt="screenshot leveleditor"/>

3. Um die Grundlagen des LEVEL EDITOR kennenzulernen, solltest du dir zunächst die folgende Erklärung ansehen:

    [Einführung Editor](../../basics/editor.md){target=_blank} 

    Es bietet sich an, die Editor-Grundlagen auch später geöffnet zu lassen, falls du während dieses Tutorials nochmal etwas nachschauen möchtest.

### Los geht's

1. Wir fangen mit etwas Einfachem an: Scroll in der Toolbar unter ACTIONS runter, bis du unter TELEGRAM **sendMessage** siehst. Dann zieh mit der Maus die SEND MESSAGE ACTION auf einen freien Bereich der Stage.
     
     <img src="./assets/actions3-2.png" alt="screenshot"/>

    Jetzt kannst du den neu entstandenen STATE umbenennen. Wir nennen ihn mal "WILLKOMMEN":
     
     <img src="./assets/actions4-3.png" width="350px" alt="screenshot"/>

    STATES lassen sich auf der Stage via drag and drop bewegen: Einfach mit dem Mauszeiger über der Kopfzeile (!) hovern, klicken und dann bewegen.

2. Jetzt wollen wir daran arbeiten, dass in unserem Level auch was passiert. Dazu müssen wir den START STATE mit unserem neuen STATE WILLKOMMEN verbinden. 

    Dazu öffnen wir die NEXT ACTION, die im START STATE schon bereit liegt, indem wir auf die Kopfzeile der NEXT ACTION klicken. Klicke in das Textfeld unter "next STATE" - du kannst hier entweder den Namen des STATES schreiben, der auf den START STATE folgen soll, oder ihn via Autocomplete auswählen (dazu musst du einmal in das Textfeld klicken) – in unserem Fall also "WILLKOMMEN". Danach klicken wir einmal irgendwo auf die Stage – und fertig. Nun geht es für die Spieler:innen vom START sofort in unseren neuen STATE WILLKOMMEN:

     <img src="./assets/actions6-3.png" alt="screenshot"/><br>
     <img src="./assets/actions7-2.png" alt="screenshot"/>

3. In WILLKOMMEN wollen wir jetzt eine Nachricht an die Spieler:in schicken, die unser Level gestartet hat. 
    Dazu öffnen wir die **sendMessage** ACTION und füllen das Formular wie folgt aus:

     <img src="./assets/actions9-2.png" width=50% alt="screenshot"/>

    Der TELEGRAM CLIENT ist gewissermaßen unsere Figur, der Absender unserer Nachrichten. In unserem Workshop-Level gibt es nur eine Figur, aber in komplexeren Games könnten wir auch mehrere Charaktere haben, die mit unseren Spieler:innen kommunizieren.
    
    Unter "**to**" wählen wir "**Player**" aus. So entscheiden wir, an wen die Telegram-Nachricht gesendet wird. Auch hier haben wir aktuell nur eine Spieler:in zur Auswahl. Es könnten aber auch mehrere sein, z.B. eine Gruppe.

    Unter "**text**" können wir einen kleinen Text schreiben (den Inhalt der Nachricht), in dem wir die Spieler:in z.B. begrüßen und nach ihrem Namen fragen.

4. Jetzt ziehen wir eine **next** ACTION in unseren WILLKOMMEN STATE, öffnen diese und tragen "QUIT" unter "**next state**" ein.

    <img src="./assets/actions8-3.png" alt="screenshot"/>

    <br><br>
    <img src="./assets/actions10-2.png" width=30% alt="screenshot"/>

    Nun sollte unser Level auch schon funktionieren.
     
    So sollte das jetzt ungefähr aussehen:

    <img src="./assets/actions11-1.png" alt="screenshot"/>

    <div class="page"/>

5. Jetzt sollten wir mal testen, ob bis hierhin alles funktioniert. Dazu schicken wir den Namen unseres Levels als Telegram-Nachricht an unseren Client. In diesem Tutorial ist das Thekla, unsere Testfigur. Dazu solltest du Thekla als Kontakt anlegen. Die Nummer von Thekla findest du unter Game > Settings > Telegram > Client. 


     <img src="./assets/telegram1.png" width="300px" alt="screenshot"/>

    Wenn alles geklappt hat, sollte Thekla auf Telegram mit *Hallo, wie heißt du?* (oder mit der Nachricht, die du geschrieben hast) antworten.
    (Die Weiterleitung ist für unser Tutorial in einem übergeordneten Menu-Level vorbereitet, deshalb bekommst du noch eine weitere Antwort zurück - die kannst du erstmal ignorieren.)
    Hat es funktioniert? Dann **Herzlichen Glückwunsch!**


### User-Eingaben verwerten

Nun passiert erstmal nichts weiter in unserem Level. Es startet, die Spieler:in bekommt unsere Nachricht und dann endet es auch schon wieder. 
Um das Ganze etwas interaktiver zu gestalten, benutzen wir eine neue ACTION in einem neuen STATE:

Wir ziehen aus den TELEGRAM ACTIONS in der Toolbar eine DIALOG ACTION auf einen leeren Teil der Stage.

Wir wollen nun den Inhalt der Nachricht prüfen, die die Spieler:in an Thekla sendet. Die ACTION **dialog** macht genau das: Das System wartet auf eine Eingabe der Spieler:in und überprüft dann nach von uns zu definierenden Regeln, wie es mit der Antwort der Spieler:in umgehen soll.

Wir benutzen im folgenden eine if-/else-Abfrage von Bedingungen. Wenn du mehr über die Grundprinzipen davon erfahren möchtest, findest du hier eine Erläuterung zu [Bedingungen Abfragen mit Conditions](../../basics/conditions.md){target=_blank} .

1. Benenne den neuen STATE, wie du willst, zum Beispiel "REPLY" (so nennen wir ihn in diesem Tutorial ab jetzt).

2. Verknüpfe die STATES miteinander, sodass dein neuer REPLY STATE auf den WILLKOMMEN STATE folgt.

3. Klicke in der DIALOG ACTION auf **Settings** und wähle **else* aus. Dann klicke zum Schließen wieder auf **Settings**. ("if" kommt später, keine Sorge!)

    <img src="./assets/dialog1-1.png" alt="screenshot" width="350px"/><br>

4. Wir sehen nun, dass ein "else"-Block in der ACTION hinzugekommen ist. Dazu kommen wir gleich.

    Wenn wir jetzt in der Toolbar auf VARIABLES klicken, sehen wir außerdem, dass dort neue Variablen aufgetaucht sind: Es gibt eine ganze Reihe von Boxen, deren übergeordnete Box genau wie dein DIALOG STATE benannt ist. In unserem Tutorial heißt sie "REPLY", darunter steht "dialog-1". **Eventuell musst du die Seite neu laden, um die neuen Variablen zu sehen.**

    <img src="./assets/dialog2-1.png" alt="screenshot" width="250px"/>
    <br>

    Hier finden wir ab sofort die Daten (z.B. die Eingaben der Spieler:innen), die uns aus dem STATE "REPLY" zur Verfügung stehen (natürlich immer erst dann, wenn die Spieler:innen während des Games am STATE "REPLY" angekommen sind).
    
    <img src="./assets/dialog6-2.png" alt="screenshot"/>
    
5.  Wir haben die Spieler:in nach ihrem Namen gefragt und wollen sie ab sofort auch korrekt ansprechen, also benutzen wir den Antworttext, den die Spieler:in an unseren Client Thekla gesendet hat: Dazu brauchen wir die Antwort, die nach Theklas erster Nachricht eingegangen ist. Da adaptor direkt nach "WILLKOMMEN" in den STATE "REPLY" springt, kommt die nächste Nachricht der Spieler:in auch in diesem STATE an. adaptor speichert die eingehende Nachricht als VARIABLE "text" des dialog_1. Wir ziehen mit der Maus also die VARIABLE **text** aus dem "dialog_1" in das Textfeld **respond** des else-Blocks (wie im oberen Bild zu sehen). Damit verwenden wir die Eingabe der Spieler:in in unserer nächsten Nachricht. Wenn die Spieler:in die Frage sinnvoll beantwortet hat, sollte es sich bei der Eingabe um ihren Namen handeln.

    Im Textfeld taucht jetzt **[[state.REPLY.dialog_1.text]]** in eckigen Klammern auf. Die eckigen Klammern markieren Variablen und ähnliche Daten. (Wir könnten natürlich auch einfach selbst die Variable in das Textfeld tippen, aber drag & drop bewahrt uns vor Tippfehlern.)

<div class="page"/>

6. Schreibe nun einen Grußtext - zum Beispiel "Hallo" - vor und/oder hinter die Variable.

    <img src="./assets/dialog3-2.png" alt="screenshot" width="500px" />

    Alles innerhalb der eckigen Klammern wird im Spiel z.B. durch die eingegebenen Daten ersetzt. Alles ohne eckige Klammern wird genau so als Antwort (**respond**) geschickt, wie es im Textfeld steht.

7. Trage nun unter **else** > **next state** "QUIT" ein, damit das Level auch automatisch beendet wird.
    Du kannst jetzt auch endlich wieder testen. Schicke dazu erneut den Namen deines Levels an Thekla. Jetzt sollte sie wie folgt antworten:

    <img src="./assets/telegram2.png" alt="screenshot" width=50%/>    

    Wie du siehst, speichert das Level deine Eingabe, wenn du am STATE "REPLY" ankommst, und kombiniert sie dann mit dem Begrüßungstext im respond. Yay!

8. Um nur auf bestimmte Eingaben und nicht einfach auf jede denkbare Eingabe zu reagieren, brauchen wir neben der "else"-Bedingung noch eine "if"-Bedingung: Öffne die **dialog_1 ACTION** innerhalb des STATES "REPLY". Nun klicke wieder auf **Settings** vom **Telegram Client** und füge **if** hinzu:

    <img src="./assets/dialog5-1.png" alt="screenshot" width="300px" />

9. Jetzt taucht innerhalb der ACTION oberhalb des else-Blocks ein if-Block auf.
    Füge zuerst mit **add condition** eine neue Bedingung hinzu. Wähle im oberen Dropdown-Menü **contains**, wenn es nicht schon ausgewählt ist. Nun kannst du darunter im contains-Block im Textfeld eingeben, was das magische Zauberwort sein soll, um Thekla zum Schweigen zu bringen. Zum Beispiel "ruhe".
    Und unter **next state** trägst du ein, welcher STATE eintreten soll, wenn die eingehende Nachricht die Buchstabenfolge "ruhe" beinhaltet - uum Beispiel "QUIT".

    Trag unter **respond** den Text ein, mit dem Thekla antworten soll, bevor der nächste STATE eintritt, zum Beispiel "ok. ich gebe Ruhe!". 

    <img src="./assets/dialog4-1.png" alt="screenshot" width=40%/>

    Jetzt hast du einen Filter mit if/else im dialog gebaut:
    Wenn du Thekla nun irgendeinen Text schickst, der die Buchstabenfolge "ruhe" enthält (*if*), wird sie antworten: "ok. ich gebe Ruhe!". Auf alle anderen eingehenden Nachrichten (*else*) wird Thekla antworten wie schon zuvor.
    Du kannst so viele if-conditions hinzufügen, wie du willst, und auf diese Weise riesige interaktive Erzählungen bauen.

### Weitere Telegram Spezialfunktionen

Jetzt hast du die Grundfunktionen und das Interface von adaptor:ex kennengelernt und dein erstes Telegram-Level mit den ACTIONS "sendMessage" und "dialog" gebaut.
In der Toolbar (links) unter ACTIONS finden sich aber noch weitere TELEGRAM ACTIONS mit Spezialfunktionen. Wie **sendFile** & **sendGeo** funktionieren, lernst du in [diesem Tutorial](../telegram-send-media/index.md){target=_blank} .

Es gibt noch viele weitere ACTIONS und Spezialfunktionen. Zum Beispiel kannst du auch deine eigenen Variablen definieren und später mit [[meineVariable]] in eckigen Klammern abfragen - und und und ...

### Testing mit Session Ansicht

Wenn es Probleme gibt, kannst du [hier](../../basics/live.md){target=_blank} nachlesen, wie du mit der Session-Ansicht troubleshooten kannst.

<div class="page"/>

------------

## Weiterführende Infos

Mehr Infos erhältst du unter:

[https://www.machinacommons.world](https://www.machinacommons.world)
oder unter

[info@machinaex.de](mailto:info@machinaex.de)
[www.machinaex.de](https://www.machinaex.de)


Da adaptor:ex bis Februar 2022 noch in Entwicklung ist, ändern sich Dinge noch ein wenig. Die Grundfunktionen und die Bedienung werden aber so bleiben und sich nur noch erweitern. Und da es ein Open-Source-Projekt ist, mit dem wir in Zukunft auch weiter arbeiten werden, wird das auch so bleiben. 

adaptor:ex kann außer Telegram auch noch viel mehr: Bühnenlicht kontrollieren, interaktive Requisiten ansteuern, mit anderer Software (Lichtprogrammen, Soundprogrammen, etc.) kommunizieren uvm. Wenn du mehr darüber erfahren möchtest, schau dich einfach ein bisschen auf den anderen Seiten um!
